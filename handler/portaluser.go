package handler

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/nexlife/chopchop-backend/model"
	service "bitbucket.org/nexlife/chopchop-backend/service"

	config "bitbucket.org/nexlife/chopchop-backend/config"
	logic "bitbucket.org/nexlife/chopchop-backend/logic"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"
)

type UserToken struct {
	Token string `json:"token" form:"token" bson:"token"`
}

func (h *Handler) PortalLogout(c echo.Context) (err error) {
	request := new(UserToken)
	_ = c.Bind(request)

	db := h.DB.Clone()
	defer db.Close()

	err = db.DB(config.DBName).C(config.TokenUserBranch).Remove(bson.M{"token": request.Token})
	if err == nil {
		return
	}

	apiAuthToken := c.Request().Header.Get("token")
	userId, _, _, _ := service.ExtractClaims(apiAuthToken)

	if err := db.DB(config.DBName).C(config.PortalUser).Update(
		bson.M{"_id": bson.ObjectIdHex(userId)},
		bson.M{"$set": bson.M(map[string]interface{}{
			"loggedIn":  false,
			"lastLogin": 0,
		})},
	); err != nil {
		//return err
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, c)
}

type PortalResetPasswordRequest struct {
	Email               string `json:"email" form:"email" bson:"email"`
	Password            string `json:"password" form:"password" bson:"password"`
	ForgotPasswordToken string `json:"forgotPasswordToken" form:"forgotPasswordToken" bson:"forgotPasswordToken"`
}

type UserReqardRequest struct {
	Email      string `json:"email" form:"email" bson:"email"`
	MerchantID string `json:"merchantId" form:"merchantId" bson:"merchantId"`
}

func (h *Handler) PortalGetCustomer(c echo.Context) (err error) {
	u := new(UserReqardRequest)
	if err = c.Bind(u); err != nil {
		return
	}
	db := h.DB.Clone()
	defer db.Close()
	acc := new(model.User)
	usd := new(model.UserStampDetails)
	if err = db.DB(config.DBName).C(config.UserBranch).Find(bson.M{"email": u.Email}).One(acc); err != nil {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid email"}
	}
	_ = db.DB(config.DBName).C(config.UserRewardDetails).Find(bson.M{"userid": u.Email, "merchantId": u.MerchantID}).One(usd) //; err != nil {
	// 	return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid email"}
	// }

	return c.JSON(http.StatusOK, struct {
		Name    string
		Email   string
		Stamps  int
		Rewards int
	}{acc.Name, acc.Email, usd.Stamp, usd.Reward})
}

func (h *Handler) PortalResetPassword(c echo.Context) (err error) {
	u := new(model.PortalUser)
	if err = c.Bind(u); err != nil {
		return err
	}
	email := u.Email
	db := h.DB.Clone()
	defer db.Close()
	fmt.Println(email)
	if err = db.DB(config.DBName).C(config.PortalUser).
		Find(bson.M{"email": email}).One(&u); err != nil {
		if err == mgo.ErrNotFound {
			return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid email"}
		}
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "Unknown Exception Please try again later"}
	}
	token := logic.GenerateRandomStringURLSafe(4)
	if err = db.DB(config.DBName).C(config.PortalUser).
		UpdateId(u.ID, bson.M{"$set": bson.M{"forgotPasswordToken": token, "updatedAt": int(time.Now().Unix())}}); err != nil {
		if err == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}
	service.PortalSendPasswordReset(email, token)
	return c.JSON(http.StatusOK, bson.M{"email": email, "token": token})
}

func (h *Handler) PortalForgot(c echo.Context) (err error) {
	u := new(model.PortalUser)

	resetObject := new(PortalResetPasswordRequest)
	if err = c.Bind(resetObject); err != nil {
		return err
	}

	email := resetObject.Email
	newPassword := resetObject.Password
	forgotPasswordToken := resetObject.ForgotPasswordToken

	if !logic.IsValidPassword(newPassword) {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid new password"}
	}
	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.PortalUser).Find(bson.M{"email": email, "forgotPasswordToken": forgotPasswordToken}).One(u); err != nil {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid email or token"}
	}

	if err = db.DB(config.DBName).C(config.PortalUser).
		UpdateId(u.ID, bson.M{"$set": bson.M{"forgotPasswordToken": "", "password": logic.HashPassword(newPassword), "loggedIn": false, "lastLogin": 0, "updatedAt": int(time.Now().Unix())}}); err != nil {
		if err == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}
	return c.JSON(http.StatusOK, "password updated")
}

func (h *Handler) PortalSetPassword(c echo.Context) (err error) {
	UserID := c.QueryParam("id")
	requestObject := new(UpdatePasswordStruct)
	if err = c.Bind(requestObject); err != nil {
		return err
	}
	newPassword := requestObject.NewPassword
	oldPassword := requestObject.OldPassword

	user := model.PortalUser{}
	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.PortalUser).FindId(bson.ObjectIdHex(UserID)).One(&user); err != nil {
		return err
	}

	if !logic.ComparePasswords(user.Password, oldPassword) {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "wrong old password"}
	}
	if !logic.IsValidPassword(newPassword) {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid new password"}
	}

	if err = db.DB(config.DBName).C(config.PortalUser).
		UpdateId(bson.ObjectIdHex(UserID), bson.M{"$set": bson.M{"password": logic.HashPassword(newPassword), "newlogin": "", "updatedAt": int(time.Now().Unix())}}); err != nil {
		if err == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}
	return c.JSON(http.StatusOK, bson.M{"status": "Success"})
}

func (h *Handler) PortalSignup(c echo.Context) (err error) {
	// Bind
	u := &model.PortalUser{ID: bson.NewObjectId()}
	if err = c.Bind(u); err != nil {
		return
	}

	db := h.DB.Clone()
	defer db.Close()

	numRows, err := db.DB(config.DBName).C(config.PortalUser).Find(bson.M{"email": u.Email}).Count()

	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Sorry, please try later"}
	}
	if numRows > 0 {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Email already taken, please choose another email"}
	}

	u.Password = logic.HashPassword(u.Password)
	u.NewLogin = "yes"
	u.VerifyKey = logic.GenerateRandomStringURLSafe(32)

	if err = db.DB(config.DBName).C(config.PortalUser).Insert(u); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Sorry, please try later"}
	}
	u.Password = ""
	if u.UserType != "merchant" {
		service.SendVerificationEmail_Merchant(u.Email, u.VerifyKey, "portal/userverification")
	}
	return c.JSON(http.StatusCreated, u)
}

func (h *Handler) UserVefification(c echo.Context) (err error) {
	e := c.QueryParam("e")
	k := c.QueryParam("k")
	db := h.DB.Clone()
	defer db.Close()
	var acc *model.PortalUser
	if err = db.DB(config.DBName).C(config.PortalUser).Find(bson.M{
		"email":     e,
		"verifyKey": k,
	}).One(&acc); err != nil {
		return c.HTML(http.StatusOK, "<h1>invalid link or token already expired</h1>")
	}
	if err := db.DB(config.DBName).C(config.PortalUser).
		UpdateId(acc.ID, bson.M{"$set": bson.M{"verifyKey": "", "updatedAt": int(time.Now().Unix())}}); err != nil {
		c.HTML(http.StatusOK, "<h1>Server Error, Please try again later</h1>")
	}
	model.AccountVerified(e)
	return c.HTML(http.StatusOK, "<script>alert('Your Email Id:"+e+" is verified');window.close();</script>")
}

//LoginStruct is
type PortalLoginStruct struct {
	Email    string `json:"email" form:"email" bson:"email"`
	Password string `json:"password" form:"password" bson:"password"`
	IDFA     string `json:"idfa" form:"idfa" bson:"idfa"`
	Platform string `json:"platform" form:"platform" bson:"platform"`
}

func (h *Handler) PortalLogin(c echo.Context) (err error) {
	// Bind
	u := new(model.PortalUser)
	request := new(PortalLoginStruct)
	if err = c.Bind(request); err != nil {
		return err
	}

	email := request.Email
	password := request.Password

	// Find user
	db := h.DB.Clone()
	defer db.Close()
	if err = db.DB(config.DBName).C(config.PortalUser).
		Find(bson.M{"email": email}).One(&u); err != nil {
		if err == mgo.ErrNotFound {
			return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid email"}
		}
		return
	}

	if !logic.ComparePasswords(u.Password, password) {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid password"}
	}

	if u.VerifyKey != "" {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "Please verify your email"}
	}

	if u.LastLogin != 0 || u.LoggedIn {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "you are logged in on other device"}
	}

	token := logic.GenerateToken(u.ID.Hex())
	if _, erro := db.DB(config.DBName).C(config.TokenUserBranch).Upsert(bson.M{"token": token}, bson.M{"$set": bson.M{"userId": u.ID.Hex(), "updatedAt": int(time.Now().Unix())}}); erro != nil {
		if erro == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}
	if err != nil {
		return err
	}
	u.Password = "" // Don't send password

	lastLogin := uint64(time.Now().Unix())
	tok, err1 := service.GenerateJWT(u.Name, u.ID.Hex(), "merchant", lastLogin)
	if err1 != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Failed to generate token"}
	}

	if err := db.DB(config.DBName).C(config.PortalUser).Update(
		bson.M{"_id": bson.ObjectIdHex(u.ID.Hex())},
		bson.M{"$set": bson.M(map[string]interface{}{
			"loggedIn":  true,
			"lastLogin": lastLogin,
		})},
	); err != nil {
		return err
	}

	u.Token = tok

	return c.JSON(http.StatusOK, u)

}
func (h *Handler) PortalVerify(c echo.Context) (err error) {
	// Bind
	u := new(model.User)
	if err = c.Bind(u); err != nil {
		return
	}

	email := c.QueryParam("e")
	verifyKey := c.QueryParam("k")

	// Find user
	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.PortalUser).Find(bson.M{"email": email, "verifyKey": verifyKey}).One(u); err != nil {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid link or token already expired"}
	}

	if err = db.DB(config.DBName).C(config.PortalUser).
		UpdateId(u.ID, bson.M{"$set": bson.M{"verifyKey": "", "updatedAt": int(time.Now().Unix())}}); err != nil {
		if err == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}

	return c.JSON(http.StatusOK, "ok")
}

// UpdatePassword to update profile of the user
func (h *Handler) PortalUpdatePassword(c echo.Context) (err error) {
	userID := userIDFromToken(c)
	requestObject := new(UpdatePasswordStruct)
	if err = c.Bind(requestObject); err != nil {
		return err
	}
	newPassword := requestObject.NewPassword
	oldPassword := requestObject.OldPassword

	user := model.User{}
	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.PortalUser).FindId(bson.ObjectIdHex(userID)).One(&user); err != nil {
		return err
	}

	if !logic.ComparePasswords(user.Password, oldPassword) {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "wrong old password"}
	}

	if !logic.IsValidPassword(newPassword) {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid new password"}
	}

	if err = db.DB(config.DBName).C(config.PortalUser).
		UpdateId(bson.ObjectIdHex(userID), bson.M{"$set": bson.M{"password": logic.HashPassword(newPassword), "updatedAt": int(time.Now().Unix())}}); err != nil {
		if err == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}

	return c.JSON(http.StatusOK, "ok")

}
