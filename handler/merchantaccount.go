package handler

import (
	"errors"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/nexlife/chopchop-backend/dbservices/merchant"
	"bitbucket.org/nexlife/chopchop-backend/dbservices/outlet"
	"bitbucket.org/nexlife/chopchop-backend/logic"
	"bitbucket.org/nexlife/chopchop-backend/model"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"

	service "bitbucket.org/nexlife/chopchop-backend/service"
)

func createMerchantAccount(acc *model.MerchantAccount) error {
	if !logic.IsValidEmail(acc.Email) {
		return errors.New("invalid email")
	}
	_, err := merchant.GetByEmail(acc.Email)
	if err == nil {
		return errors.New("Email already exists, please choose another email")
	} else if err != mgo.ErrNotFound {
		return errors.New("Sorry, please try later")
	}

	acc.AccStatus = model.StatusInProcess
	acc.ApprovalStatus = model.StatusInProcess
	acc.ApprovalReqSent = true
	// if acc.ApprovalRequired == false {
	// 	acc.ApprovalStatus = model.StatusApproved
	// }

	acc.VerifyKey = logic.GenerateRandomStringURLSafe(32)
	// acc.Password = logic.HashPassword(acc.Password)
	acc.CreatedAt = int(time.Now().Unix())
	acc.UpdatedAt = int(time.Now().Unix())

	acc.Isnew = true

	merchID, err := merchant.Create(acc)
	if err != nil {
		return errors.New("Sorry, please try later")
	}
	service.SendVerificationEmail_Merchant(acc.Email, acc.VerifyKey, "merchants/verification")
	approver := model.GetApprover()
	if approver.UserID != "" {
		service.SendApprovalRequest(approver.UserID, merchID)
	}
	return nil
}

func (h *Handler) CreateMerchantAccount(c echo.Context) (err error) {
	acc := &model.MerchantAccount{ID: bson.NewObjectId()}
	if err = c.Bind(acc); err != nil {
		return err
	}

	if err = createMerchantAccount(acc); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	// fmt.Println(acc.ID.Hex())
	brandApproval(acc.ID.Hex())
	return c.JSON(http.StatusCreated, struct {
		MerchantId bson.ObjectId `json:"merchantId"`
		Email      string        `json:"email"`
	}{acc.ID, acc.Email})
}

func ListMerchantAccounts(c echo.Context) (err error) {
	v := new(verify)
	if err = c.Bind(v); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	if v.Email != "" {
		accs, err := merchant.GetByEmail(v.Email)
		if err != nil {
			return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
		}
		return c.JSON(http.StatusCreated, struct {
			MerchantId bson.ObjectId `json:"merchantId"`
			Email      string        `json:"email"`
		}{accs.ID, accs.Email})
	}
	accs, err := merchant.GetAll(0)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	return c.JSON(http.StatusOK, accs)
}

type verify struct {
	Email string `json:"email"`
}

func (h *Handler) MerchantAccVerification(c echo.Context) (err error) {
	e := c.QueryParam("e")
	k := c.QueryParam("k")
	db := h.DB.Clone()
	defer db.Close()

	var acc *model.MerchantAccount
	if acc, err = merchant.Find(map[string]interface{}{
		"email":     e,
		"verifyKey": k,
	}); err != nil {
		return c.HTML(http.StatusOK, "<h1>invalid link or token already expired</h1>")
	}

	if err = merchant.Patch(acc.ID.Hex(), map[string]interface{}{
		"verifyKey": "",
		"accStatus": "confirmed",
	}); err != nil {
		if err == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}
	model.AccountVerified(e)
	return c.HTML(http.StatusOK, "<script>alert('Your Email Id:"+e+" is verified');window.close();</script>")
}

type approvalReq struct {
	verify
	Status bool `json:"status" bson:"status"`
}

func brandApproval(v string) (err error) {
	var acc *model.MerchantAccount
	if acc, err = merchant.GetById(v); err != nil {
		return errors.New("invalid email or account")
	}

	var approvalStatus string
	// if v.Status == true {
	approvalStatus = model.StatusApproved
	// } else {
	// 	approvalStatus = model.StatusRejected
	// }

	if err = merchant.Patch(acc.ID.Hex(), map[string]interface{}{
		"approvalStatus": approvalStatus,
	}); err != nil {
		if err == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}

	outlets := []*model.Outlet{}
	accId := acc.ID.String()
	accId = accId[strings.Index(acc.ID.String(), "\"")+1 : strings.LastIndex(acc.ID.String(), "\"")]
	if outlets, err = outlet.FindAll(map[string]interface{}{"merchantId": accId}); err != nil {
		return errors.New("Error listing outlets")
	}

	for _, o := range outlets {
		if o.ApprovalStatus != model.StatusApproved {
			_ = outlet.ApproveOutlet(o.ID.Hex())
		}
	}

	return nil
}

func (h *Handler) BrandApproval(c echo.Context) (err error) {
	merchant_ID := c.QueryParam("merchant")
	if err = brandApproval(merchant_ID); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}

	return c.JSON(http.StatusOK, "success")
}

type logInRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func MerchantLogIn(c echo.Context) error {
	req := new(logInRequest)
	if err := c.Bind(req); err != nil {
		return err
	}

	acc, err := merchant.GetByEmail(req.Email)
	if err != nil {
		if err == mgo.ErrNotFound {
			return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid email"}
		}
		return err
	}
	if !logic.ComparePasswords(acc.Password, req.Password) {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid password"}
	}

	lastLogin := uint64(time.Now().Unix())
	tok, err1 := service.GenerateJWT(acc.ContactName, acc.ID.Hex(), "merchant", lastLogin) 
	if err1 != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Failed to generate token"}
	}

	//if acc.LoggedIn == true {
	//	return &echo.HTTPError{Code: http.StatusForbidden, Message: "Duplicate user session"}
	//}

	if err := merchant.Patch(acc.ID.Hex(), map[string]interface{}{
		"loggedIn": true,
		"lastLogin": lastLogin,
	}); err != nil {
		return err
	}

	return c.JSON(http.StatusOK, bson.M{"userId":  acc.ID.Hex(), "token": tok})
}

type logOutRequest struct {
	Id string `json:"id"`
}

func MerchantLogOut(c echo.Context) error {
	req := new(logOutRequest)
	if err := c.Bind(req); err != nil {
		return err
	}

	if err := merchant.Patch(req.Id, map[string]interface{}{
		"lastLogin": 0,
		"loggedIn": false,
	}); err != nil {
		return err
	}

	return c.JSON(http.StatusOK, "success")
}

type updatePasswordReq struct {
	MerchantId string `json:"id"`
	OldPasswd  string `json:"oldPassword"`
	NewPasswd  string `json:"newPassword"`
}

func UpdateMerchantPassword(c echo.Context) (err error) {
	req := new(updatePasswordReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	acc, err := merchant.GetById(req.MerchantId)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusNotFound, Message: "account not found"}
	}

	if !logic.ComparePasswords(acc.Password, req.OldPasswd) {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "wrong old password"}
	}

	if !logic.IsValidPassword(req.NewPasswd) {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid new password"}
	}

	if err = merchant.Patch(req.MerchantId, map[string]interface{}{
		"password": logic.HashPassword(req.NewPasswd),
	}); err != nil {
		if err == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: err.Error()}
	}

	return c.JSON(http.StatusOK, "ok")
}
