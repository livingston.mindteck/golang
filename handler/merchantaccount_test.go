package handler

import (
	"errors"
	"fmt"
	"strings"
	"testing"

	"bitbucket.org/nexlife/chopchop-backend/logic"
	"bitbucket.org/nexlife/chopchop-backend/model"
	"github.com/globalsign/mgo/bson"
	"github.com/stretchr/testify/assert"
)

func TestCreateMerchantAccount(t *testing.T) {
	id := bson.NewObjectId()
	email := string(logic.GenerateRandomString(8)) + "@test.com"
	fmt.Println("email:", email)
	tests := []struct {
		acc    *model.MerchantAccount
		expErr error
	}{
		{
			acc: &model.MerchantAccount{
				ID:    id,
				Email: "",
			},
			expErr: errors.New("invalid email"),
		},
		{
			acc: &model.MerchantAccount{
				ID:       id,
				Email:    email,
				Password: "",
			},
			expErr: errors.New("invalid password"),
		},
		{
			acc: &model.MerchantAccount{
				ID:       id,
				Email:    email,
				Password: "secret",
			},
			expErr: nil,
		},
		{
			acc: &model.MerchantAccount{
				ID:       id,
				Email:    email,
				Password: "secret",
			},
			expErr: errors.New("Email already exists, please choose another email"),
		},
	}

	for i, _ := range tests {
		err := createMerchantAccount(tests[i].acc)
		assert.Equal(t, tests[i].expErr, err)
	}
}

func TestBrandApproval(t *testing.T) {
	acc := &model.MerchantAccount{
		ID:               bson.NewObjectId(),
		Email:            "testapproval@test.com",
		Password:         "secret",
		ApprovalRequired: false,
	}

	if err := createMerchantAccount(acc); err != nil {
		return
	}

	if err := createOutlets(acc.ID.String()[strings.Index(acc.ID.String(), "\"")+1 : strings.LastIndex(acc.ID.String(), "\"")]); err != nil {
		return
	}

	tests := []struct {
		req    *approvalReq
		expErr error
	}{
		{
			req: &approvalReq{
				verify: verify{Email: ""},
				Status: true,
			},
			expErr: errors.New("invalid email or account"),
		},
		{
			req: &approvalReq{
				verify: verify{Email: "testapproval@test.com"},
				Status: true,
			},
			expErr: nil,
		},
		{
			req: &approvalReq{
				verify: verify{Email: "testapproval@test.com"},
				Status: false,
			},
			expErr: nil,
		},
	}

	for i, _ := range tests {
		err := brandApproval(tests[i].req)
		assert.Equal(t, tests[i].expErr, err)
	}
}

func createOutlets(merchantId string) error {
	outlets := []*model.Outlet{
		{
			ID:         bson.NewObjectId(),
			Name:       "outlet1",
			MerchantId: merchantId,
		},
		{
			ID:         bson.NewObjectId(),
			Name:       "outlet2",
			MerchantId: merchantId,
		},
	}

	for _, o := range outlets {
		if err := createOutlet(o); err != nil {
			return err
		}
	}
	return nil
}
