package handler

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	config "bitbucket.org/nexlife/chopchop-backend/config"
	merchant "bitbucket.org/nexlife/chopchop-backend/dbservices/merchant"
	outlet "bitbucket.org/nexlife/chopchop-backend/dbservices/outlet"
	model "bitbucket.org/nexlife/chopchop-backend/model"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"
)

type StaffTransaction struct {
	UserID     string
	StaffID    string
	OutletID   string
	Name       string
	NoOfStamp  int
	NoOfReward int
}

func (h *Handler) GetBillLogCount(c echo.Context) (err error) {
	merchantid := c.QueryParam("merchantid")
	db := model.DB.Clone()
	defer db.Close()
	numRows, err := db.DB(config.DBName).C(config.BillingLog).Find(bson.M{"merchantid": merchantid}).Count()
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	return c.JSON(http.StatusOK, struct {
		Count int `json:"count" bson:"count"`
	}{Count: numRows})
}

func (h *Handler) GetTransactionsCount(c echo.Context) (err error) {
	merchantid := c.QueryParam("merchantid")
	db := model.DB.Clone()
	defer db.Close()
	numRows, err := db.DB(config.DBName).C("mer" + merchantid).Find(nil).Count()
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	return c.JSON(http.StatusOK, struct {
		Count int `json:"count" bson:"count"`
	}{Count: numRows})
}

func (h *Handler) GetBillLog(c echo.Context) (err error) {
	merchantid := c.QueryParam("merchantid")
	pagenumber, _ := strconv.Atoi(c.QueryParam("page"))
	pagesize, er1 := strconv.Atoi(c.QueryParam("size"))
	if pagesize == 0 || er1 != nil {
		pagesize = config.PaginationSize
	}

	// userID := userIDFromToken(c)
	// user, _ := model.GetUser(userID)
	db := model.DB.Clone()
	defer db.Close()
	results := []model.Billing{}
	err = db.DB(config.DBName).C(config.BillingLog).Find(bson.M{"merchantid": merchantid}).Sort("-$natural").Skip(pagenumber * pagesize).Limit(pagesize).All(&results)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	return c.JSON(http.StatusOK, results)
}

func (h *Handler) GetTransactions(c echo.Context) (err error) {
	outletid := c.QueryParam("outletid")
	if outletid == "" {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Empty email id"}
	}
	transactions, err := model.GetTransactionByOutlet(outletid)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	return c.JSON(http.StatusOK, struct {
		Transactions []model.TransactionDetails `json:"transactions"`
	}{Transactions: transactions})
}
func (h *Handler) StampsByUser(c echo.Context) (err error) {
	merchantid := c.QueryParam("merchantid")
	results, err := model.GetUserTransactionsByStamps(merchantid)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	rwds, err := model.GetRewards(merchantid)
	return c.JSON(http.StatusOK, struct {
		Transactions []bson.M    `json:"transactions"`
		Rewards      interface{} `json:"rewards"`
	}{Transactions: results, Rewards: rwds[0]["count"]})
}
func (h *Handler) StampedRedeemed(c echo.Context) (err error) {
	merchantid := c.QueryParam("merchantid")
	stamps, redeemed, err := model.GetStampedRedeemed(merchantid)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	return c.JSON(http.StatusOK, struct {
		Stamps   int
		Redeemed int
	}{Stamps: stamps, Redeemed: redeemed})
}
func (h *Handler) GetTransactionsByMerchant(c echo.Context) (err error) {
	merchantid := c.QueryParam("merchantid")
	pagenumber, _ := strconv.Atoi(c.QueryParam("page"))
	pagesize, er1 := strconv.Atoi(c.QueryParam("size"))
	if pagesize == 0 || er1 != nil {
		pagesize = config.PaginationSize
	}
	if merchantid == "" {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Empty merchant id"}
	}
	transactions, err := model.GetTransactionsByMerchant(merchantid, pagenumber, pagesize)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	return c.JSON(http.StatusOK, struct {
		Transactions []model.TransactionDetails `json:"transactions"`
	}{Transactions: transactions})
}

// db.mer5ca94522502bfe0d71f2e138.aggregate([{$match:{type:"stamp"}},{$group:{_id:"$userid",count:{$sum:"$noofsr"}}}])
// { "_id" : "three@four.com", "count" : 10 }
// { "_id" : "one@two.com", "count" : 12 }

func (h *Handler) DoTransaction(c echo.Context) (err error) {
	req := new(StaffTransaction)
	if err = c.Bind(req); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	trn := &model.TransactionDetails{ID: bson.NewObjectId()}
	chop := 0
	if req.NoOfReward == 0 {
		trn.NoofSr = req.NoOfStamp
		trn.Type = "stamp"
		chop = req.NoOfStamp
	} else {
		trn.NoofSr = req.NoOfReward
		trn.Type = "reward"
		chop = req.NoOfReward
	}
	ouTlet, err := outlet.GetById(req.OutletID)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	} else if ouTlet.Manager == req.StaffID {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Manager Cannot Chop"}
	}

	meRchant, _ := merchant.GetById(ouTlet.MerchantId)

	if err = outlet.UpdateChop(req.StaffID, req.NoOfStamp > 0, chop, ouTlet); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	userChop, err := model.GetUserStamp(ouTlet.MerchantId, req.UserID)

	userChop.MerchantID = ouTlet.MerchantId

	if err != nil {
		if req.NoOfReward > 0 {
			return &echo.HTTPError{Code: http.StatusBadRequest, Message: req.UserID + " do not have sufficient Stamps"}
		}
		fmt.Println(req.NoOfStamp)
		fmt.Println(meRchant.NumberOfStamp)
		userChop.UserID = req.UserID
		waitStamp := req.NoOfStamp / meRchant.NumberOfStamp
		if req.NoOfStamp == 0 {
			userChop.Stamp = 0
		} else {
			userChop.Stamp = req.NoOfStamp % meRchant.NumberOfStamp
		}
		userChop.Reward = 0
		userChop.WaitingForReward = waitStamp
		model.InsertUserStamp(userChop)
	} else {
		fmt.Println(req.NoOfStamp)
		userChop.Stamp += req.NoOfStamp
		fmt.Println(userChop.Stamp)
		waitStamp := userChop.Stamp / meRchant.NumberOfStamp
		userChop.Stamp = userChop.Stamp % meRchant.NumberOfStamp
		fmt.Println(waitStamp)
		userChop.WaitingForReward += waitStamp

		fmt.Println(userChop.WaitingForReward)
		if req.NoOfReward > 0 &&
			(userChop.WaitingForReward < req.NoOfReward) {
			return &echo.HTTPError{Code: http.StatusBadRequest, Message: req.UserID + " do not have sufficient Stamps"}
		}
		userChop.WaitingForReward -= req.NoOfReward
		userChop.Reward += req.NoOfReward
		model.UpdateUserStamp(userChop)
	}

	trn.OutletID = bson.ObjectIdHex(req.OutletID)
	trn.UserID = req.UserID
	trn.StaffID = req.StaffID
	trn.Name = req.Name

	if err := model.ChopTransaction(ouTlet.MerchantId, trn); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	return c.JSON(http.StatusOK, "Success")
}

func TransactionLog(c echo.Context) (err error) {
	roomID := c.QueryParam("room_id")
	userID := userIDFromToken(c)
	user, _ := model.GetUser(userID)
	results := []model.LoyaltyTransactionLog{}
	if user.Status == "root" || (user.Rooms != nil && user.Rooms[roomID] != nil && user.Rooms[roomID].(bson.M)["role"] == "admin") {
		results = model.TransactionLog(roomID, "")
	} else {
		results = model.TransactionLog(roomID, userID)
	}
	return c.JSON(http.StatusOK, results)
}

type HistoryResponse struct {
	Data []*bson.M `json:"data" form:"data" bson:"data"`
}

//History is to give points to specific user in specific room
func (h *Handler) History(c echo.Context) (err error) {
	response := new(HistoryResponse)
	roomID := c.QueryParam("room_id")
	requestFromTimestamp := c.QueryParam("from_timestamp")
	requestToTimestamp := c.QueryParam("to_timestamp")

	result := []*bson.M{}
	fromTimestamp := bson.NewObjectIdWithTime(time.Unix(int64(0), 0))
	toTimestamp := bson.NewObjectIdWithTime(time.Unix(int64(0), 0))
	filter := bson.M{"point": bson.M{"$ne": 0}}

	if requestFromTimestamp != "" {
		tmp, _ := strconv.Atoi(requestFromTimestamp)
		time := time.Unix(int64(tmp), 0)
		fromTimestamp = bson.NewObjectIdWithTime(time)
		filter["_id"] = bson.M{"$gte": fromTimestamp}
	}
	if requestToTimestamp != "" {
		tmp, _ := strconv.Atoi(requestToTimestamp)
		time := time.Unix(int64(tmp), 0)
		toTimestamp = bson.NewObjectIdWithTime(time)
		filter["_id"] = bson.M{"$lte": toTimestamp}
	}

	if requestFromTimestamp != "" && requestToTimestamp != "" {
		filter["_id"] = bson.M{"$gte": fromTimestamp, "$lte": toTimestamp}
	}
	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.TransactionBranch + "." + roomID).Find(filter).
		All(&result); err != nil {
		if err == mgo.ErrNotFound {
			fmt.Println(err)
			return echo.ErrNotFound
		}
	}

	response.Data = result
	return c.JSON(http.StatusOK, response)
}
