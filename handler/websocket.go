package handler

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
)

var (
	upgrader = websocket.Upgrader{}
	WS       *websocket.Conn
	// Request  *http.Request
	// Response *http.Response
)

func rescue() {
	r := recover() // We stopped here the failing process!!!
	if r != nil {
		fmt.Println("Panic has been recover")
	}
}

// ListenWebsocket is function when client start to listen
func ListenWebsocket(c echo.Context) (err error) {
	defer rescue()
	req := c.Request()
	res := c.Response()
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	WS, err = upgrader.Upgrade(res, req, nil)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer WS.Close()

	for {

		_, msg, err := WS.ReadMessage()
		if err != nil {
			fmt.Println(err)
			// c.Logger().Error(err)
			return err
		}
		fmt.Printf("%s\n", msg)
		fmt.Println(time.Now().Format(time.RFC850))

	}
}

//NotifyPleaseRefreshLeaderBoard notify client to refresh the list
func NotifyPleaseRefreshLeaderBoard() {
	defer rescue()
	err := WS.WriteMessage(websocket.TextMessage, []byte("refreshLeaderboard"))
	fmt.Println(err)
}
