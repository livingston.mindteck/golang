package handler

import (
	"net/http"
	"time"

	"bitbucket.org/nexlife/chopchop-backend/config"
	"bitbucket.org/nexlife/chopchop-backend/model"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"
)

func CreateOutletStatus(c echo.Context) (err error) {
	status := &model.OutletStatus{ID: bson.NewObjectId()}
	if err = c.Bind(status); err != nil {
		return err
	}

	status.CreatedAt = int(time.Now().Unix())
	status.ModifiedAt = status.CreatedAt
	if err = model.CreateOutletStatus(status); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Sorry, please try later"}
	}

	return c.JSON(http.StatusCreated, status)
}

func GetOutletStatus(c echo.Context) error {
	key := c.QueryParam("statusid")
	s, err := model.QuerryOutletStatus(key)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	return c.JSON(http.StatusOK, s)
}

type DeleteOutletStatusReq struct {
	StatusId string `json:"statusid" form:"statusid" bson:"statusid"`
}

func (h *Handler) DeleteOutletStatus(c echo.Context) (err error) {
	req := &DeleteOutletStatusReq{}
	if err = c.Bind(req); err != nil {
		return err
	}
	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.OutletStatusBranch).RemoveId(bson.ObjectIdHex(req.StatusId)); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Sorry, please try later"}
	}

	return c.JSON(http.StatusOK, "success")
}
