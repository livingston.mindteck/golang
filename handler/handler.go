package handler

import (
	"github.com/globalsign/mgo"
)

type (
	// Handler - Handle with DB connection
	Handler struct {
		DB *mgo.Session
	}
)
