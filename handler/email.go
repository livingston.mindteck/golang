package handler

import (
	config "bitbucket.org/nexlife/chopchop-backend/config"
	"bitbucket.org/nexlife/chopchop-backend/dbservices/templates"
	email "bitbucket.org/nexlife/chopchop-backend/service"

	"github.com/labstack/echo/v4"
)

func (h *Handler) SendEmail(c echo.Context) (err error) {
	var toIds = []string{"raviteja.karri6@gmail.com"}
	email.SendEmail("Testing", toIds, "Its working")
	return nil
}

/*
func SendEmail1() (err error) {
	var toIds = []string{"raviteja.karri6@gmail.com"}
	email.SendEmail("Testing", toIds, "Its working")
	return nil
}
*/

func (h *Handler) SendTestEmail(c echo.Context) (err error) {
	var toIds = []string{"livingston.mindteck@gmail.com"}
	// email.SendEmail("Testing", toIds, "Its working")
	go email.SendMail("Test Email", config.EmailSender, toIds, "This is sample test to check with the ")
	return
}
func (h *Handler) UpdateEmail(c echo.Context) (err error) {
	_ = templates.AddDefaultTemplates()
	return
}
