package handler

import (
	"database/sql"
	"net/http"

	"bitbucket.org/nexlife/chopchop-backend/model"

	logic "bitbucket.org/nexlife/chopchop-backend/logic"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"

	_ "github.com/mattn/go-sqlite3"
)

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func BackupToSQLite(c echo.Context) (err error) {
	eventID := "tmone"

	rooms := []*model.Room{}
	arrRoomID := []string{}
	rooms, _ = model.GetRoomBasedOnEventID(eventID)
	for _, room := range rooms {
		arrRoomID = append(arrRoomID, room.ID.Hex())
	}

	db, err := sql.Open("sqlite3", "./sqlite.db")

	// stmt, err := db.Prepare("CREATE TABLE `user` (`uid` VARCHAR(64),`rid` VARCHAR(64),`point` INTEGER)")
	// checkErr(err)
	// _, err = stmt.Exec()
	checkErr(err)

	// stmt, err = db.Prepare("INSERT INTO userinfo(username, departname, created) values(?,?,?)")
	// checkErr(err)
	stmt, err := db.Prepare("INSERT INTO user(uid, rid, point) values(?,?,?)")
	checkErr(err)

	for _, user := range model.AllUesrs() {
		for key, value := range user.Rooms {
			for _, roomID := range arrRoomID {
				if key == roomID {
					if value.(bson.M)["loyalty_point"] != nil {
						// fmt.Println(user.ID.Hex() + " - " + roomID + " - " + strconv.Itoa(value.(bson.M)["loyalty_point"].(int)))
						_, err = stmt.Exec(user.ID.Hex(), roomID, value.(bson.M)["loyalty_point"].(int))
						checkErr(err)
					}
				}
			}
		}
	}

	return c.JSON(http.StatusOK, "success")
}

func RestoreFromScreenshot() {
	// func RestoreFromScreenshot(c echo.Context) (err error) {
	// roomID := "5c3c14807d7501141c00f08d"
	// email := ""
	// point := 0
	// user, err := model.GetUserFromEmail("norratulazirah.shaharudin@tm.com.my")
	// if err != nil {
	// 	// 	log.Fatal(err)
	// }
	// email := ""
	// point := 0
	// model.SavePointForUser(user.ID.Hex(), roomID, 148)

	aa("norratulazirah.shaharudin@tm.com.my", 148)

	aa("hanely7916@gmail.com", 144)
	aa("mzuraida@gmail.com", 105)
	// user, err = model.GetUserFromEmail("hanely7916@gmail.com")
	// if err != nil {
	// 	log.Fatal(" not found - " + user.Email)
	// }
	// model.SavePointForUser(user.ID.Hex(), roomID, 144)

	// user, err = model.GetUserFromEmail("mzuraida@gmail.com")
	// if err != nil {
	// 	log.Fatal(" not found - " + user.Email)
	// }
	// model.SavePointForUser(user.ID.Hex(), roomID, 105)

	// user, err = model.GetUserFromEmail("rashid.hamdan@tm.com.my")
	// if err != nil {
	// 	log.Fatal(" not found - " + user.Email)
	// }
	// model.SavePointForUser(user.ID.Hex(), roomID, 102)
	aa("rashid.hamdan@tm.com.my", 102)

	// user, err = model.GetUserFromEmail("soraya.ahmadnasir@tm.com.my")
	// if err != nil {
	// 	log.Fatal(" not found - " + user.Email)
	// }
	// model.SavePointForUser(user.ID.Hex(), roomID, 92)
	aa("soraya.ahmadnasir@tm.com.my", 92)

	// email := "Asmadi.saleh@tm.com.my"
	// user, err = model.GetUserFromEmail(email)
	// if err != nil {
	// 	log.Fatal(" not found - " + email)
	// }
	// model.SavePointForUser(user.ID.Hex(), roomID, 90)
	aa("Asmadi.saleh@tm.com.my", 90)

	// user, err = model.GetUserFromEmail("mazlita.shamsuddin@tm.com.my")
	// if err != nil {
	// 	log.Fatal(" not found - " + user.Email)
	// }
	// model.SavePointForUser(user.ID.Hex(), roomID, 75)
	aa("mazlita.shamsuddin@tm.com.my", 75)

	// email = "nasmie7383@gmail.com"
	// user, err = model.GetUserFromEmail(email)
	// if err != nil {
	// 	log.Fatal(" not found - " + email)
	// }
	// model.SavePointForUser(user.ID.Hex(), roomID, 73)

	aa("nasmie7383@gmail.com", 73)

	// email = "azlin.baharudin@tm.com.my"
	// user, err = model.GetUserFromEmail(email)
	// if err != nil {
	// 	log.Fatal(" not found - " + email)
	// }
	// model.SavePointForUser(user.ID.Hex(), roomID, 72)
	aa("azlin.baharudin@tm.com.my", 72)

	// email = "malissa.mustafah@tm.com.my"
	// point = 65
	// user, err = model.GetUserFromEmail(email)
	// if err != nil {
	// 	log.Fatal(" not found - " + email)
	// }
	// model.SavePointForUser(user.ID.Hex(), roomID, point)
	aa("malissa.mustafah@tm.com.my", 65)

	// email = "norshama@tm.com.ny"
	// point = 65
	// user, err = model.GetUserFromEmail(email)
	// if err != nil {
	// 	log.Fatal(" not found - " + email)
	// }
	// model.SavePointForUser(user.ID.Hex(), roomID, point)
	aa("norshama@tm.com.ny", 65)

	// email = "ismayati@tm.com.my"
	// point = 63
	// user, err = model.GetUserFromEmail(email)
	// if err != nil {
	// 	log.Fatal(" not found - " + email)
	// }
	// model.SavePointForUser(user.ID.Hex(), roomID, point)

	aa("ismayati@tm.com.my", 63)

	// email = ""
	// point =
	// user, err = model.GetUserFromEmail(email)
	// if err != nil {
	// 	log.Fatal(" not found - " + email)
	// }
	// model.SavePointForUser(user.ID.Hex(), roomID, point)

	aa("idi.ismail@tm.com.my", 62)
	aa("alanshah124@gmail.com", 61)
	aa("v01310@tm.com.my", 60)
	aa("sitirohayu.mohammadfauzi@tm.com.my", 56)
	aa("fauzan.fazil@tm.com.my", 52)
	aa("irmawani@tm.com.my", 49)
	aa("azila7@gmail.com", 48)
	aa("mfadhil.abdrauf@tm.com.my", 48)
	aa("mfaizal.marzuki@tm.com.my", 46)
	aa("hjam@gitn.com.my", 46)
	aa("amywym@gmail.com", 45)
	aa("mfaizam@tm.com.my", 45)
	aa("n.musaddik@gmail.com", 44)
	aa("enemef971@yahoo.com", 42)
	aa("nieta2010@gmail.com", 40)
	aa("azizhadi.sk@gmail.com", 40)
	aa("kayrahim85@gmail.com", 39)
	aa("nuzaimah.hanifah@tm.com.my", 38)
	aa("zzaridah@tm.com.my", 38)
	aa("samsus@tm.com.my", 36)
	aa("sitinurbainah@tm.com.my", 36)
	aa("masah@tm.com.my", 35)
	aa("nal07@yahoo.com", 35)
	aa("rafi816@gmail.com", 34)
	aa("syazana.azman@tm.com.my", 31)
	aa("nadiah.anuar@tm.com.my", 31)
	aa("m_nasir@tm.com.my", 31)
	aa("kkaran97@yahoo.com", 28)
	aa("fauziandanila@gmail.com", 26)
	aa("irdayaww@gmail.com", 26)
	aa("maridzar@tm.com.my", 25)
	aa("nazhar@tm.com.my", 25)
	aa("ahzul@tm.com.my", 24)
	aa("suhaimiz@tm.com.my", 24)
	aa("firdaussoder@gmail.com", 23)
	aa("iq_1@yahoo.com", 23)
	aa("lowhongni@tm.com.my", 22)
	aa("fairuzi@tm.com.my", 21)
	aa("sitiayshah.latip@tm.com.my", 21)
	aa("azlieza.hamzah@tm.com.my", 20)
	aa("zulhairy@gmail.com", 20)
	aa("azmi.ramli@tm.com.my", 20)
	aa("nizam.arshad@gmail.com", 20)
	aa("wanariff@yahoo.com", 20)
	aa("azrah@tm.com.my", 20)
	aa("msabrid@tm.com.my", 19)
	aa("syamim.haziq@tm.com.my", 19)
	aa("shahulhamid@tm.com.my", 18)
	aa("gdyong@tm.com.my", 18)
	aa("muzamer@gmail.com", 18)
	aa("kalsom.mdisa@tm.com.my", 18)
	aa("nelfi@tm.com.my", 17)
	aa("nikrus.nikazizan@tm.com.my", 17)
	aa("halim_wd12@yahoo.com", 16)
	aa("zawahir@tm.com.my", 16)
	aa("salmahmn@tm.com.my", 16)
	aa("badrulh@tm.com.my", 16)
	aa("peijet@tm.com.my", 16)
	aa("hany.hud@tm.com.my", 15)
	aa("ihassan1962@gmail.com", 15)
	aa("asmiyah.abdullah@tm.com.my", 15)
	aa("atiqah.yusoff@tm.com.my", 15)
	aa("siti.liyana@tm.com.my", 15)
	aa("nsuhaimi181@yahoo.com", 15)
	aa("badruz@tm.com.my", 15)
	aa("kamariah.kahar@tm.com.my", 14)
	aa("syazwanee.kamarudzaman@tm.com.my", 14)
	aa("suzee009@gmail.com", 14)
	aa("kkyrei@gmail.com", 14)
	aa("xs3code@gmail.com", 13)
	aa("meddy@tm.com.my", 13)
	aa("johancarlosa@gmail.com", 13)
	aa("nurulaziemah.chenazir@tm.com.my", 13)
	aa("fmwong@tm.com.my", 13)
	aa("fareezakadri@gmail.com", 12)
	aa("mizan@tm.com.my", 12)
	aa("magupai.maralin@tm.com.my", 12)
	aa("rhh@tm.com.my", 11)
	aa("ztuah99@gmail.com", 11)
	aa("nurbaya@tm.com.my", 11)
	aa("farah_zinal@yahoo.com", 11)
	aa("hazelin@tm.com.my", 11)
	aa("basker.balasundram@tm.com.my", 11)
	aa("Epoy82@yahoo.com", 11)
	aa("safsk@yahoo.com", 11)
	aa("angela.peter@tm.com.my", 11)
	aa("ishaqs@tm.com.my", 11)
	aa("faisal@tm.com.my", 11)

	aa("dzaryana@gmail.com", 10)
	aa("tmchemat@gmail.com", 10)
	aa("ismawati.shamsudin@tm.com.my", 10)
	aa("mdfaiz@tm.com.my", 10)
	aa("haslyn.nasuha@tm.com.my", 10)
	aa("nasrulrizal@gmail.com", 10)
	aa("samniah@tm.com.my", 10)
	aa("judefontaine989@gmail.com", 10)
	aa("rohayu.mdnoh@tm.com.my", 10)
	aa("hefkadri@tm.com.my", 10)
	aa("mhamdan@tm.com.my", 10)
	aa("miss_ijah85@yahoo.co.uk", 10)
	aa("salman.othman@tm.com.my", 10)
	aa("nurjannah.razif@tm.com.my", 10)
	aa("fiqshim@gmail.com", 10)
	aa("sharizal.mohamed@tm.com.my", 10)
	aa("khafizatun@tm.com.my", 10)
	aa("kleetc@yahoo.com", 10)
	aa("ziham.razak@gmail.com", 10)
	aa("iswandi.dorani@tm.com.my", 10)
	aa("suhaimi.alias@tm.com.my", 9)

	aa("hamidzan@tm.com.my", 1)
	aa("mokhtark@tm.com.my", 1)
	aa("saiful.ibrahim@tm.com.my", 1)
	aa("nuraini.yahya@tm.com.my", 1)
	aa("hmaliki@tm.com.my", 1)
	aa("azlan.abdulali@tm.com.my", 1)
	aa("azrina.ibrahimsha@tm.com.my", 1)
	aa("shu@tm.com.my", 1)
	aa("dami.kenny@gmail.com", 1)
	aa("raslanr@tm.com.my", 1)
	aa("dinesh.rama@tm.com", 1)
	aa("chang.wen@gmail.com", 1)
	aa("amen.amsler@gmail.com", 1)
	aa("nizhashim@yahoo.com", 1)
	aa("hamim.karim@tm.com.my", 1)
	aa("nuraisyah.johari@tm.com.my", 1)
	aa("muradnk20@gmail.com", 1)
	aa("norhisham.anuar@tm.com.my", 1)
	aa("zariman.shafie@tm.com.my", 1)
	aa("sulistyo@vads.co.id", 1)
	aa("ayewlee@gmail.com", 1)
	aa("mnornajamudin@tm.com.my", 1)
	aa("chinsuilee@yahoo.com", 1)
	aa("andrew_aj@yahoo.com", 1)
	aa("indraputra.ismail@tm.com.my", 1)
	aa("jafer@tm.com.my", 1)
	aa("zammil1985@gmail.com", 1)
	aa("sofia.samsuddin@webe.com.my", 1)
	aa("norhidayah.omar@tm.com.my", 2)
	aa("weihawch@hotmail.com", 2)
	aa("sen1605@gmail.com", 2)
	aa("fakhrurazi.noordin@tm.com.my", 2)
	aa("zainol.azman@gmail.com", 2)
	aa("aris_is@tm.com.my", 2)
	aa("ho.waikeen@tm.com.my", 2)
	aa("arif.abdulrashid@tm.com.my", 2)
	aa("azlee@tm.com.my", 2)
	aa("aizat.shafie@tm.com.my", 2)
	aa("saad@tm.com.my", 2)
	aa("hamlun@tm.com.my", 2)
	aa("darrylteo1987@gmail.com", 2)
	aa("chen@tm.com.my", 2)
	aa("1mandychin1@gmail.com", 2)
	aa("khairilhaniff@gmail.com", 2)
	aa("sue@webe.com.my", 2)
	aa("laihoong.chan@gmail.com", 2)
	aa("asihikin.ahmad@tm.com.my", 3)
	aa("azliza@tm.com.my", 3)
	aa("schu3883@gmail.com", 3)
	aa("wanirsalina.khalid@tm.my", 3)
	aa("ramzulmazwan.ramli@tm.com.my", 3)
	aa("taufik.zaidon@tm.com.my", 3)
	aa("mohdkamal.jamaludin@tm.com.my", 3)
	aa("roshadah@tm.com.my", 3)
	aa("linawati@tm.com.my", 3)
	aa("dian.andriani@vads.co.id", 3)
	aa("ezza.ali@tm.com.my", 3)
	aa("norizan.hashim@tm.com.my", 3)
	aa("ainols@tm.com.my", 3)
	aa("nhana.pilus@tm.com.my", 3)
	aa("norsyuhaila.sobri@tm.com.my", 3)
	aa("killymoorty@gmail.com", 3)
	aa("aizat.sayutgi@tm.com.my", 3)
	aa("shahrizan.yusof@tm.com.my", 3)
	aa("hermants@tm.com.my", 3)
	aa("aidil.ikhsan@gitn.com.my", 3)
	aa("suhaili.baba@tm.com.my", 3)
	aa("syahril.ali@tm.com.my", 3)
	aa("tini@tm.com.my", 3)
	aa("nuraida.jasmin@tm.com.my", 3)
	aa("noraz@tm.com.my", 3)
	aa("asapian@tm.com.my", 3)
	aa("kamal.zahari@gmail.com", 3)
	aa("sitirohaya.mohdnor@tm.com.my", 8)
	aa("amran@tm.com.my", 8)
	aa("zarena@tm.com.my", 8)
	aa("kamal2103@gmail.com", 8)
	aa("irwanshah.zainudin@tm.com.my", 8)
	aa("nahar.razak@webe.com.my", 8)
	aa("prestigemann@gmail.com", 8)
	aa("kaydeeannmobile@gmail.com", 8)
	aa("ahlee@tm.com.my", 8)
	aa("azharul.rumai@tm.com.my", 8)
	aa("norishfairuz@gmail.com", 8)
	aa("azarina@tm.com.my", 8)
	aa("azmir.ahmad@tm.com.my", 8)
	aa("zakwanjamil@gmail.com", 8)
	aa("jayanthi.andiappan@tm.com.my", 8)
	aa("nizam.a.khan@gmail.com", 9)
	aa("mawaddah.alias@tm.com.my", 9)
	aa("matriza@tm.com.my", 9)
	aa("hartini.hamid@tm.com.my", 9)
	aa("haniszafirah@tm.com.my", 9)
	aa("wnazura@tm.com.my", 9)

	aa("noorsyazana3@gmail.com", 6)
	aa("s12092@tm.com.my", 6)
	aa("azlan.anuar@tm.com.my", 6)
	aa("noorbaya@yahoo.com", 6)
	aa("mohdikhwanamin@tm.com.my", 6)
	aa("azbiz09@yahoo.com", 6)
	aa("sitianonm@tm.com.my", 6)
	aa("amanan85@gmail.com", 7)
	aa("nhaizura@tm.com.my", 7)
	aa("herwan@tm.com.my", 7)
	aa("basit.sohak@tm.com.my", 7)
	aa("joyanne.ng@gmail.com", 7)
	aa("mzs11927@gmail.com", 7)
	aa("rageeta.kaur@tm.com.my", 7)
	aa("romdzi@gmail.com", 7)
	aa("yitloo@hotmail.com", 7)
	aa("sitihawa@tm.com.my", 7)
	aa("wanmuhammad.wanjusoh@tm.com.my", 7)
	aa("rozainee@tm.com.my", 7)
	aa("shell.tanggaraju@tm.com.my", 8)
	aa("saifudinbahrudin@tm.com.my", 8)

	aa("mazura@user.com", 4)
	aa("adialaron@gmail.com", 4)
	aa("snowdrop186@gmail.com", 4)
	aa("shaharina.yaacob@gmail.com", 4)
	aa("rozal@tm.com.my", 4)
	aa("ashram.husainizakwan@yahoo.com", 4)
	aa("azlan.halib@tm.com.my", 4)
	aa("liyanaheyholetsgo@gmail.com", 4)
	aa("nadinegan86@gmail.com", 4)
	aa("azman.a@tm.com.my", 4)
	aa("jotheeswaran.s@webe.com.my", 4)
	aa("azaitulrimi@tm.com.my", 4)
	aa("asmara@tm.com.my", 4)
	aa("muhammadfitri.aziz@tm.com.my", 6)
	aa("mrozaimi@tm.com.my", 6)
	aa("axlstradlin82@gmail.com", 6)
	aa("agjamal@tm.com.my", 5)
	aa("badrulhs@tm.com.my", 5)
	aa("ananorhana@yahoo.com", 5)
	aa("rohaida.mdrashid@tm.com.my", 5)
	aa("kamal.zahari@gmail.com", 5)

	aa("idi.ismail@tm.com.my", 62)
	aa("mursyid.mohamed@tm.com.my", 6)
	aa("szainib@tm.com.my", 6)
	aa("dzhasrin@tm.com.my", 6)
	aa("kgwong1999@gmail.com", 6)
	aa("nurshalindda@tm.com.my", 6)
	aa("noly@tm.com.my", 6)
	aa("alamin.nordin@gitn.com.my", 6)
	aa("norhidayu.daud@tm.com.my", 6)
	aa("kenari6767@gmail.com", 6)
	aa("umaraizat@gmail.com", 6)
	aa("darina.zainal@tm.com.my", 6)
	aa("rajanoorizdihar@tm.com.my", 6)
	aa("ainafida@tm.com.my", 6)
	aa("rohani.lahap@tm.com.my", 6)
	aa("tunmarini@gmail.com", 6)
	aa("farina@tm.com.my", 5)
	aa("zakarea@tm.com.my", 5)
	aa("azlind.zawawi@tm.com.my", 5)
	aa("annanaz75@gmail.com", 5)
	aa("sylviajoe.ojihi@gmail.com", 5)
	aa("nazri.ambi@tm.com.my", 5)
	aa("mickylpc@gmail.com", 5)
	aa("muz.abd@gmail.com", 5)
	aa("ahidayat@tm.com.my", 5)
	aa("nuruladha215@gmail.com", 5)
	aa("haryani@gitn.com.my", 5)
	aa("ta469ch@gmail.com", 5)
	aa("cbollboy_85@yahoo.com", 5)
	aa("rodiahsabri@tm.com.my", 5)
	aa("ariatis@tm.com.my", 5)
	aa("norhan.wahab@tm.com.my", 5)
	aa("katherine.tan@tm.com.my", 5)
	aa("roxalliniataullah.mazlan@tm.com.my", 5)
	aa("ishak.k@tm.com.my", 5)
	aa("nazrirazali@tm.com.my", 5)
	aa("huzairi.g@gmail.com", 5)
	aa("umarabdulaziz@tm.com.my", 5)
	aa("fizaromlee78@gmail.com", 5)
	aa("ktng@mmu.edu.my", 5)
	aa("anaf@tm.com.my", 5)
	aa("asnawati.alatif@tm.com.my", 5)
	aa("gfazli@gmail.com", 5)
	aa("fauzil.wahab01@gmail.com", 5)
	aa("nurulizzatighazali@gmail.com", 5)
	aa("ritaneti1974@gmail.com", 5)
	aa("rajaazlini.rajaazman@tm.com.my", 5)
	aa("halim.muslim@yahoo.com", 5)
	aa("zamzuri1972@gmail.com", 5)
	aa("izzahmeor@yahoo.com", 5)
	aa("anna.yee@tm.com.my", 5)
	aa("anas@tm.com.my", 5)
	aa("joeanah@tm.com.my", 5)
	aa("mahadirnarifin@tm.com.my", 5)
	aa("ezafaizura@gmailcom", 5)
	aa("ain.mohtar@gmail.com", 5)
	aa("iirwanz@gmail.com", 5)
	aa("raimi.ishak77@gmail.com", 5)
	aa("charles.jackson@webe.com.my", 5)
	aa("asilah.kamil@tm.com.my", 5)
	aa("syahrul.liza@tm.com.my", 5)
	aa("hanis.hanisab@tm.com.my", 5)
	aa("jenny.soong@tm.com.my", 5)
	aa("rubiah@tm.com.my", 5)

	// return c.JSON(http.StatusOK, "success restore")

}

func aa(email string, point int) {
	user, err := model.GetUserFromEmail(email)
	if err != nil {
		id := model.CreateUser(email, logic.HashPassword("abc123"), email)
		model.SavePointForUser(id, "5c3c14807d7501141c00f08d", point)
	} else {
		model.SavePointForUser(user.ID.Hex(), "5c3c14807d7501141c00f08d", point)
	}
}
