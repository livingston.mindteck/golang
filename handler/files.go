package handler

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"

	"bitbucket.org/nexlife/chopchop-backend/model"

	config "bitbucket.org/nexlife/chopchop-backend/config"
	logic "bitbucket.org/nexlife/chopchop-backend/logic"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"
)

//UploadImageRoom is
func (h *Handler) UploadImageRoom(c echo.Context) (err error) {
	roomID := c.FormValue("room_id")
	fileCategory := c.FormValue("file_category")
	room := model.Room{}
	imagesPath := []string{}

	if roomID == "" {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "room_id parameter not supplied"}
	}

	if fileCategory == "" {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "file_category parameter not supplied"}
	} else if fileCategory == "icon" {
		db := h.DB.Clone()
		defer db.Close()
		if err = db.DB(config.DBName).C(config.RoomBranch).FindId(bson.ObjectIdHex(roomID)).One(&room); err != nil {
			return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Room with ID " + roomID + " not exist"}
		}
		if room.IconPath != "" {
			_ = os.Remove(room.IconPath)
		}
	}
	form, _ := c.MultipartForm()

	files := form.File["files"]

	for index, file := range files {
		// Source
		src, err := file.Open()
		if err != nil {
			return err
		}
		defer src.Close()

		fmt.Println("Need to validate image here if image is real image")
		// buff := make([]byte, 512) // why 512 bytes ? see http://golang.org/pkg/net/http/#DetectContentType
		// if _, err = file.Read(buff); err != nil {
		// 	fmt.Println(err) // do something with that error
		// 	return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Image not valid"}
		// }

		// Destination
		path := config.RoomUploadDir + "/" + roomID
		if _, err := os.Stat(path); os.IsNotExist(err) {
			os.MkdirAll(path, os.ModePerm)
		}
		imageName := fileCategory + "_" + strconv.Itoa(index) + "(" + logic.GenerateRandomStringURLSafe(10) + ")" + "_" + (time.Now()).Format("2006_01_02T15_04_05") + ".jpg"

		pathToFile := path + "/" + imageName
		imagesPath = append(imagesPath, pathToFile)

		// fmt.Println(pathToFile)
		dst, err := os.Create("./" + pathToFile) // file.Filename)
		if err != nil {
			return err
		}
		defer dst.Close()

		// Copy
		if _, err = io.Copy(dst, src); err != nil {
			return err
		}

		if fileCategory == "icon" {
			db := h.DB.Clone()
			defer db.Close()

			if err = db.DB(config.DBName).C(config.RoomBranch).Update(
				bson.M{"_id": bson.ObjectIdHex(roomID)},
				bson.M{"$set": bson.M{"icon_path": pathToFile}},
			); err != nil {
				return err
			}
		}
	}

	return c.JSON(http.StatusOK, imagesPath)
}

//RoomPublicFile is
func (h *Handler) RoomPublicFile(c echo.Context) (err error) {
	item := c.QueryParam("item")
	fmt.Println(item)
	return c.File(item)
}

type DeleteRoomPublicFileRequest struct {
	FileCategory string `json:"file_category" form:"file_category" bson:"file_category"`
	RoomID       string `json:"room_id" form:"room_id" bson:"room_id"`
	Path         string `json:"path" form:"path" bson:"path"`
}

func (h *Handler) DeleteRoomPublicFile(c echo.Context) (err error) {
	request := new(DeleteRoomPublicFileRequest)
	room := model.Room{}

	_ = c.Bind(request)
	if request.Path == "" {
		if request.FileCategory == "" {
			return &echo.HTTPError{Code: http.StatusBadRequest, Message: "file_category parameter not supplied"}
		}
		if request.RoomID == "" {
			return &echo.HTTPError{Code: http.StatusBadRequest, Message: "room_id parameter not supplied"}
		}
	} else {
		_ = os.Remove(request.Path)
		return c.JSON(http.StatusOK, "success")
	}

	db := h.DB.Clone()
	defer db.Close()

	if request.FileCategory == "icon" {

		if err = db.DB(config.DBName).C(config.RoomBranch).FindId(bson.ObjectIdHex(request.RoomID)).One(&room); err != nil {
			return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Room with ID " + request.RoomID + " not exist"}
		}
		_ = os.Remove(room.IconPath)

		if err = db.DB(config.DBName).C(config.RoomBranch).Update(
			bson.M{"_id": bson.ObjectIdHex(request.RoomID)},
			bson.M{"$unset": bson.M{"icon_path": 1}},
		); err != nil {
			fmt.Println(err)
			return err
		}

		// if err != nil {
		// 	return &echo.HTTPError{Code: http.StatusBadRequest, Message: "file " + request.Item + " not exist"}
		// }
	}

	return c.JSON(http.StatusOK, "success")
}
