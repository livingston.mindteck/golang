package handler

import (
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/nexlife/chopchop-backend/model"

	service "bitbucket.org/nexlife/chopchop-backend/service"

	config "bitbucket.org/nexlife/chopchop-backend/config"
	logic "bitbucket.org/nexlife/chopchop-backend/logic"

	"github.com/dgrijalva/jwt-go"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"
	"github.com/tealeg/xlsx"
)

//LogoutStruct is
type LogoutStruct struct {
	IDFA        string `json:"idfa" form:"idfa" bson:"idfa"`
	OneSignalID string `json:"onesignal_id" form:"onesignal_id" bson:"onesignal_id"`
}

//Logout to remove user from push notification list
func Logout(c echo.Context) (err error) {
	// userID := userIDFromToken(c)
	request := new(LogoutStruct)
	_ = c.Bind(request)
	// model.ClearUserIDFA(request.IDFA)
	model.ClearUserOnesignal(request.OneSignalID)

	return c.JSON(http.StatusOK, "success")
}

//ProfileIDFA is when user send idfa (without token), will return detail user if exist
func (h *Handler) ProfileIDFA(c echo.Context) (err error) {
	idfa := c.QueryParam("idfa")
	idfaUser := model.IDFAUser{}
	user := model.User{}

	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.IDFAUserBranch).Find(bson.M{"idfa": idfa}).One(&idfaUser); err != nil {
		fmt.Println(err)
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: idfa + " not exist in or record"}
	}

	if err = db.DB(config.DBName).C(config.UserBranch).FindId(idfaUser.UserID).One(&user); err != nil {
		fmt.Println(err)
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "user with idfa " + idfa + " not exist in or record"}
	}

	user.Password = ""
	user.VerifyKey = ""
	user.ForgotPasswordToken = ""

	return c.JSON(http.StatusOK, user)
}

func (h *Handler) Signup(c echo.Context) (err error) {
	// Bind
	u := &model.User{ID: bson.NewObjectId()}
	if err = c.Bind(u); err != nil {
		return
	}

	// Validate
	if !logic.IsValidEmail(u.Email) {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "invalid email"}
	}
	if !logic.IsValidPassword(u.Password) {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "invalid password"}
	}

	db := h.DB.Clone()
	defer db.Close()

	numRows, err := db.DB(config.DBName).C(config.UserBranch).Find(bson.M{"email": u.Email}).Count()

	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Sorry, please try later"}
	}
	if numRows > 0 {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Email already taken, please choose another email"}
	}

	u.Status = "Unverified"
	if u.VerifyKey == "IwantToBeRootUser" {
		u.Status = "root"
	}
	u.VerifyKey = logic.GenerateRandomStringURLSafe(32)
	u.Password = logic.HashPassword(u.Password)
	u.UpdatedAt = int(time.Now().Unix())
	u.Token = logic.GenerateToken(u.ID.Hex())

	if err = db.DB(config.DBName).C(config.UserBranch).Insert(u); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Sorry, please try later"}
	}

	//disable email confirmation for a while
	// service.SendVerificationEmail(u.Email, u.VerifyKey)

	u.Password = ""
	u.VerifyKey = ""

	return c.JSON(http.StatusCreated, u)
}

//LoginStruct is
type LoginStruct struct {
	Email       string `json:"email" form:"email" bson:"email"`
	Password    string `json:"password" form:"password" bson:"password"`
	IDFA        string `json:"idfa" form:"idfa" bson:"idfa"`
	OneSignalID string `json:"onesignal_id" form:"onesignal_id" bson:"onesignal_id"`
	BundleID    string `json:"bundle_id" form:"bundle_id" bson:"bundle_id"`
	Platform    string `json:"platform" form:"platform" bson:"platform"`
}

func (h *Handler) SaveUser(c echo.Context) (err error) {
	u := &model.User{ID: bson.NewObjectId()}
	if err = c.Bind(u); err != nil {
		return
	}
	u.Password = "dummy"
	db := h.DB.Clone()
	defer db.Close()

	acc := new(model.User)
	if err = db.DB(config.DBName).C(config.UserBranch).Find(bson.M{"email": u.Email}).One(acc); err != nil {
		u.Status = "Unverified"
		if u.VerifyKey == "IwantToBeRootUser" {
			u.Status = "root"
		}
		u.VerifyKey = logic.GenerateRandomStringURLSafe(32)
		u.Password = logic.HashPassword(u.Password)
		u.UpdatedAt = int(time.Now().Unix())
		u.Token = logic.GenerateToken(u.ID.Hex())
		if err = db.DB(config.DBName).C(config.UserBranch).Insert(u); err != nil {
			return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Sorry, please try later"}
		}
		return c.JSON(http.StatusCreated, struct{ Status string }{"User details Updated"})
	}
	return c.JSON(http.StatusOK, struct{ Message string }{"User already exists"})
}

//Login is request token function
func (h *Handler) Login(c echo.Context) (err error) {
	// Bind
	u := new(model.User)
	request := new(LoginStruct)
	if err = c.Bind(request); err != nil {
		return err
	}

	email := request.Email
	password := request.Password
	idfa := request.IDFA
	oneSignalID := request.OneSignalID

	// Find user
	db := h.DB.Clone()
	defer db.Close()
	if err = db.DB(config.DBName).C(config.UserBranch).
		Find(bson.M{"email": email}).One(&u); err != nil {
		if err == mgo.ErrNotFound {
			return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid email"}
		}
		return
	}

	if !logic.ComparePasswords(u.Password, password) {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid password"}
	}

	//-----
	// JWT
	//-----

	// Create token
	// token := jwt.New(jwt.SigningMethodHS256)

	// // Set claims
	// claims := token.Claims.(jwt.MapClaims)
	// claims["id"] = u.ID
	// //sorry, lazy to make refresh token, haha, so set expiry too long. please dont try this at home
	// claims["exp"] = time.Now().Add(time.Hour * 99999).Unix()

	// // Generate encoded token and send it as response
	// u.Token, err = token.SignedString([]byte(Key))
	u.Token = logic.GenerateToken(u.ID.Hex())

	if _, erro := db.DB(config.DBName).C(config.IDFAUserBranch).Upsert(bson.M{"idfa": idfa}, bson.M{"$set": bson.M{"userId": u.ID.Hex(), "updatedAt": int(time.Now().Unix())}}); erro != nil {
		if erro == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}
	if err != nil {
		return err
	}

	if oneSignalID != "" {
		if _, err = db.DB(config.DBName).C(config.OnesignalUserBranch).Upsert(
			bson.M{"onesignal_id": oneSignalID},
			bson.M{"$set": bson.M{
				"user_id":   u.ID.Hex(),
				"bundle_id": request.BundleID,
				"platform":  request.Platform,
				"updatedAt": int(time.Now().Unix()),
			}},
		); err != nil {
			fmt.Println(err)
			return err
		}
	}

	u.Password = "" // Don't send password
	return c.JSON(http.StatusOK, u)
}

//Verify to verify user (clicked by user in email)
func (h *Handler) Verify(c echo.Context) (err error) {
	// Bind
	u := new(model.User)
	if err = c.Bind(u); err != nil {
		return
	}

	email := c.QueryParam("e")
	verifyKey := c.QueryParam("k")

	// Find user
	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.UserBranch).Find(bson.M{"email": email, "verifyKey": verifyKey}).One(u); err != nil {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid link or token already expired"}
	}

	if err = db.DB(config.DBName).C(config.UserBranch).
		UpdateId(u.ID, bson.M{"$set": bson.M{"verifyKey": "", "status": "Verified", "updatedAt": int(time.Now().Unix())}}); err != nil {
		if err == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}

	return c.JSON(http.StatusOK, "ok")
}

type ResetPasswordRequest struct {
	Email               string `json:"email" form:"email" bson:"email"`
	Password            string `json:"password" form:"password" bson:"password"`
	ForgotPasswordToken string `json:"forgotPasswordToken" form:"forgotPasswordToken" bson:"forgotPasswordToken"`
}

//ResetPassword is
func (h *Handler) ResetPassword(c echo.Context) (err error) {
	u := new(model.User)

	resetObject := new(ResetPasswordRequest)
	if err = c.Bind(resetObject); err != nil {
		return err
	}

	email := resetObject.Email
	newPassword := resetObject.Password
	forgotPasswordToken := resetObject.ForgotPasswordToken

	fmt.Println(email)
	fmt.Println(newPassword)
	fmt.Println(forgotPasswordToken)

	if !logic.IsValidPassword(newPassword) {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid new password"}
	}
	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.UserBranch).Find(bson.M{"email": email, "forgotPasswordToken": forgotPasswordToken}).One(u); err != nil {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid email or token"}
	}

	if err = db.DB(config.DBName).C(config.UserBranch).
		UpdateId(u.ID, bson.M{"$set": bson.M{"forgotPasswordToken": "", "password": logic.HashPassword(newPassword), "updatedAt": int(time.Now().Unix())}}); err != nil {
		if err == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}
	return c.JSON(http.StatusOK, "password updated")
}

//ResetPasswordPage is not used
func ResetPasswordPage(c echo.Context) (err error) {
	s := strings.Split(c.Request().RequestURI, "?")
	if len(s) == 2 && s[1] != "" {
		return c.Redirect(302, config.Domain+"public/html/reset-password?"+s[1])
		// return c.Redirect(302, "https://fattahmuhyiddeen.github.io/reset-password?"+s[1])
	}
	return err
}

//RequestChangePassword is requesting reset link to be sent to user email
func (h *Handler) RequestChangePassword(c echo.Context) (err error) {
	u := new(model.User)
	if err = c.Bind(u); err != nil {
		return
	}
	email := c.QueryParam("email")

	// Find user
	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.UserBranch).Find(bson.M{"email": email}).One(u); err != nil {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid email"}
	}
	forgotPasswordToken := logic.GenerateRandomString(46)
	if err = db.DB(config.DBName).C(config.UserBranch).
		UpdateId(u.ID, bson.M{"$set": bson.M{"forgotPasswordToken": forgotPasswordToken, "updatedAt": int(time.Now().Unix())}}); err != nil {
		if err == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}
	user, _ := model.GetUserFromEmail(email)
	go service.SendRequestResetPasswordEmail(email, user)

	return c.JSON(http.StatusOK, "a secret link has been sent to "+email)
}

// GetProfile to profile of the user by the user it self. it contain private data of the user
func (h *Handler) GetProfile(c echo.Context) (err error) {
	userID := userIDFromToken(c)

	user := model.User{}
	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.UserBranch).FindId(bson.ObjectIdHex(userID)).One(&user); err != nil {
		fmt.Println(err)
		return err
	}

	user.Password = ""
	user.VerifyKey = ""
	user.ForgotPasswordToken = ""

	return c.JSON(http.StatusOK, user)
}

// GetBiodata in to get personal detail of a user by other user. therefore, private fields will not be return in this function
func (h *Handler) GetBiodata(c echo.Context) (err error) {
	user := model.User{}

	userID := c.QueryParam("user_id")
	roomID := c.QueryParam("room_id")

	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.UserBranch).FindId(bson.ObjectIdHex(userID)).One(&user); err != nil {
		fmt.Println(err)
		return err
	}

	user.Password = ""
	if roomID != "" {
		user.Rooms = bson.M{roomID: user.Rooms[roomID]}
	} else {
		user.Rooms = bson.M{}
	}
	user.VerifyKey = ""
	user.ForgotPasswordToken = ""

	return c.JSON(http.StatusOK, user)
}

//SearchUsersRequest is
type SearchUsersRequest struct {
	Keyword string `json:"keyword" form:"keyword" bson:"keyword"`
}

func SearchUsers(c echo.Context) (err error) {
	request := new(SearchUsersRequest)
	c.Bind(request)
	users := model.SearchUser(request.Keyword)
	return c.JSON(http.StatusOK, users)
}

//UpdateProfileStruct is parameters for UpdateProfile api
type UpdateProfileStruct struct {
	Name string `json:"name" form:"name" bson:"name"`
}

// UpdateProfile to update profile of the user
func (h *Handler) UpdateProfile(c echo.Context) (err error) {
	userID := userIDFromToken(c)

	requestObject := new(UpdateProfileStruct)
	if err = c.Bind(requestObject); err != nil {
		return err
	}

	name := requestObject.Name

	db := h.DB.Clone()
	defer db.Close()
	if err = db.DB(config.DBName).C(config.UserBranch).
		UpdateId(bson.ObjectIdHex(userID), bson.M{"$set": bson.M{"name": name, "updatedAt": int(time.Now().Unix())}}); err != nil {
		if err == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}

	return c.JSON(http.StatusOK, "ok")
}

//UpdatePasswordStruct is
type UpdatePasswordStruct struct {
	NewPassword string `json:"newPassword" form:"newPassword" bson:"newPassword"`
	OldPassword string `json:"oldPassword" form:"oldPassword" bson:"oldPassword"`
}

// UpdatePassword to update profile of the user
func (h *Handler) UpdatePassword(c echo.Context) (err error) {
	userID := userIDFromToken(c)
	requestObject := new(UpdatePasswordStruct)
	if err = c.Bind(requestObject); err != nil {
		return err
	}
	newPassword := requestObject.NewPassword
	oldPassword := requestObject.OldPassword

	user := model.User{}
	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.UserBranch).FindId(bson.ObjectIdHex(userID)).One(&user); err != nil {
		return err
	}

	if !logic.ComparePasswords(user.Password, oldPassword) {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "wrong old password"}
	}

	if !logic.IsValidPassword(newPassword) {
		return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "invalid new password"}
	}

	if err = db.DB(config.DBName).C(config.UserBranch).
		UpdateId(bson.ObjectIdHex(userID), bson.M{"$set": bson.M{"password": logic.HashPassword(newPassword), "updatedAt": int(time.Now().Unix())}}); err != nil {
		if err == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}

	return c.JSON(http.StatusOK, "ok")

}

func UserWithMostChopRank(c echo.Context) (err error) {
	eventID := c.QueryParam("event_id")

	rooms := []*model.Room{}
	arrRoomID := []string{}
	if eventID != "" {
		rooms, _ = model.GetRoomBasedOnEventID(eventID)
		for _, room := range rooms {
			arrRoomID = append(arrRoomID, room.ID.Hex())
		}
	}

	results := model.UserWithMostChopRank(arrRoomID)

	// results.Sort(results[:], func(i, j int) bool {
	// 	return results[i]["point"].(int) < results[j]["point"].(int)
	// })

	sort.Slice(results,
		func(i, j int) bool {
			return results[i]["point"].(int) > results[j]["point"].(int)
		})

	if c.QueryParam("excel") == "1" {
		var file *xlsx.File
		var sheet *xlsx.Sheet
		var row *xlsx.Row
		var cell *xlsx.Cell
		// var err error

		file = xlsx.NewFile()
		sheet, err = file.AddSheet("Sheet1")
		if err != nil {
			fmt.Printf(err.Error())
		}
		row = sheet.AddRow()

		cell = row.AddCell()
		cell.Value = "No"

		cell = row.AddCell()
		cell.Value = "Name"

		cell = row.AddCell()
		cell.Value = "Email"

		cell = row.AddCell()
		cell.Value = "Stamp"

		for index, user := range results {
			row = sheet.AddRow()

			cell = row.AddCell()
			cell.Value = strconv.Itoa(index + 1)

			cell = row.AddCell()
			cell.Value = user["user_name"].(string)

			cell = row.AddCell()
			cell.Value = user["user_email"].(string)

			cell = row.AddCell()
			cell.Value = strconv.Itoa(user["point"].(int))
		}

		filePath := "tmp/ranking.xlsx"
		err = file.Save(filePath)
		if err != nil {
			fmt.Printf(err.Error())
		}
		return c.File(filePath)
	}
	return c.JSON(http.StatusOK, results)
}

func userIDFromToken(c echo.Context) string {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	return claims["id"].(string)
}
