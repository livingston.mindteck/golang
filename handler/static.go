package handler

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
)

//SetStaticDataRequest is parameters while giving points. param point will be incremented with existing
type SetStaticDataRequest struct {
	Key   string `json:"key" form:"key" bson:"key"`
	Value string `json:"value" form:"value" bson:"value"`
}

//SetStaticData is to set value for all app user
func SetStaticData(c echo.Context) (err error) {
	request := new(SetStaticDataRequest)
	_ = c.Bind(request)
	// model.SetStaticData()

	return c.JSON(http.StatusOK, "success")
}

//GetStaticData is to set value for all app user
func GetStaticData(c echo.Context) (err error) {
	return c.JSON(http.StatusOK, "success")
}

//Timestamp is used to return timestamp and can also use to check whether system is running or not :P
func Timestamp(c echo.Context) (err error) {
	var results map[string]interface{}
	json.Unmarshal([]byte(`{ "human" : "`+time.Now().Format(time.RFC850)+`" , "miliseconds" : `+strconv.FormatInt(time.Now().UnixNano()/1000000, 10)+` }`), &results)
	return c.JSON(http.StatusOK, results)
}
