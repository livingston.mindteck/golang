package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"

	"bitbucket.org/nexlife/chopchop-backend/model"

	config "bitbucket.org/nexlife/chopchop-backend/config"
	logic "bitbucket.org/nexlife/chopchop-backend/logic"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"
)

//tak siap lagi ni
//SearchRoomsRequest is
type SearchRoomsRequest struct {
	Keyword string `json:"keyword" form:"keyword" bson:"keyword"`
}

func SearchRooms(c echo.Context) (err error) {
	rooms := []*model.Room{}
	request := new(SearchRoomsRequest)
	_ = c.Bind(request)
	rooms, err = model.SearchRoom(request.Keyword)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	return c.JSON(http.StatusOK, rooms)
}

//CreateRoom is to create a new room
func CreateRoom(c echo.Context) (err error) {
	// Bind
	userID := userIDFromToken(c)
	room := &model.Room{ID: bson.NewObjectId()}
	if err = c.Bind(room); err != nil {
		return err
	}

	roleForCreator := "admin"

	room.Users = bson.M{userID: bson.M{"role": roleForCreator}}
	room.IsPublic = true

	//handle if input = nil
	if room.IsRedeemable != true {
		room.IsRedeemable = false
	}

	if room.IsOneCustomerOneStamp != true {
		room.IsOneCustomerOneStamp = false
	}

	if err = model.CreateRoom(room); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Sorry, please try later"}
	}

	go model.AddRoomInUser(userID, room.ID.Hex(), roleForCreator)

	// path := "rooms." + room.ID.Hex()

	// if err = db.DB(config.DBName).C(config.UserBranch).Update(
	// 	bson.M{"_id": bson.ObjectIdHex(userID)},
	// 	bson.M{"$set": bson.M{path: bson.M{"role": roleForCreator}}},
	// ); err != nil {
	// 	return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Sorry, please try later"}
	// }

	user, _ := model.GetUser(userID)
	go model.AddUserInRoom(room.ID.Hex(), user, roleForCreator)

	return c.JSON(http.StatusOK, room)
}

//DeleteRoomRequest is struct for DeleteRoomRequest parameter
type DeleteRoomRequest struct {
	RoomID string `json:"room_id" form:"room_id" bson:"room_id"`
}

//DeleteRoom to delete room and delete all public files related to the room
func (h *Handler) DeleteRoom(c echo.Context) (err error) {
	request := new(DeleteRoomRequest)
	if err = c.Bind(request); err != nil {
		return err
	}

	fmt.Println(request.RoomID)
	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.RoomBranch).RemoveId(bson.ObjectIdHex(request.RoomID)); err != nil {
		fmt.Println(err)
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Sorry, please try later"}
	}

	_ = os.RemoveAll(config.RoomUploadDir + "/" + request.RoomID)

	return c.JSON(http.StatusOK, "success")
}

//RoomsList is to get summary of all rooms
func (h *Handler) RoomsList(c echo.Context) (err error) {
	rooms := []*model.Room{}
	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.RoomBranch).
		Find(nil).
		// Find(bson.M{"isPublic": true}).
		Select(bson.M{"screens": 0}).
		All(&rooms); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, rooms)
}

//EditRoomRequest is struct of EditRoom request
type EditRoomRequest struct {
	RoomID    string `json:"room_id" form:"room_id" bson:"room_id"`
	Key       string `json:"key" form:"key" bson:"key"`
	Value     string `json:"value" form:"value" bson:"value"`
	BoolValue bool   `json:"bool_value" form:"bool_value" bson:"bool_value"`
	InputType string `json:"input_type" form:"input_type" bson:"input_type"`
}

//EditRoom is to edit specific column of room. This function is not designed to receive a bundle of inputs. just one input per request as this fit the usage
func (h *Handler) EditRoom(c echo.Context) (err error) {
	request := new(EditRoomRequest)
	_ = c.Bind(request)
	db := h.DB.Clone()
	defer db.Close()

	fmt.Println(request)
	fmt.Println(request)
	fmt.Println(request)

	if request.InputType == "string" {
		if err = db.DB(config.DBName).C(config.RoomBranch).Update(
			bson.M{"_id": bson.ObjectIdHex(request.RoomID)},
			bson.M{"$set": bson.M{request.Key: request.Value}},
		); err != nil {
			return err
		}
	} else if request.InputType == "boolean" {
		if err = db.DB(config.DBName).C(config.RoomBranch).Update(
			bson.M{"_id": bson.ObjectIdHex(request.RoomID)},
			bson.M{"$set": bson.M{request.Key: request.BoolValue}},
		); err != nil {
			return err
		}
	}

	return c.JSON(http.StatusOK, "success")
}

//RoomDetail used to query all details about a specific room
func (h *Handler) RoomDetail(c echo.Context) (err error) {
	id := c.QueryParam("id")
	room := model.Room{}
	db := h.DB.Clone()
	defer db.Close()

	if err = db.DB(config.DBName).C(config.RoomBranch).FindId(bson.ObjectIdHex(id)).One(&room); err != nil {
		fmt.Println(err)
		return
	}

	return c.JSON(http.StatusOK, room)
}

//GetUsersObject is used to set or remove role (admin/staff) admin (can see sales and scan qr) or staff (can scan qr only) defined by role
func (h *Handler) GetUsersObject(c echo.Context) (err error) {
	db := h.DB.Clone()
	defer db.Close()
	room := model.Room{}

	roomID := c.QueryParam("room_id")

	users := []*model.User{}

	if err = db.DB(config.DBName).C(config.RoomBranch).FindId(bson.ObjectIdHex(roomID)).One(&room); err != nil {
		return err
	}

	uids := []bson.ObjectId{}
	for k, v := range room.Users {
		fmt.Printf("key[%s] value[%s]\n", k, v)
		uids = append(uids, bson.ObjectIdHex(k))
	}
	_ = db.DB(config.DBName).C(config.UserBranch).FindId(bson.M{"$in": uids}).
		Select(bson.M{"rooms": 0, "password": 0, "verifyKey": 0, "forgotPasswordToken": 0}).
		All(&users)

	return c.JSON(http.StatusOK, users)
}

//ManageUserRequest is params send while adding / removing user. to remove, just give role = "" or null
type ManageUserRequest struct {
	RoomID string `json:"room_id" form:"room_id" bson:"room_id"`
	UserID string `json:"user_id" form:"user_id" bson:"user_id"`
	Role   string `json:"role" form:"role" bson:"role"`
}

//ManageUser is used to set or remove role (admin/staff) admin (can see sales and scan qr) or staff (can scan qr only) defined by role
func (h *Handler) ManageUser(c echo.Context) (err error) {
	request := new(ManageUserRequest)
	if err = c.Bind(request); err != nil {
		return err
	}

	db := h.DB.Clone()
	defer db.Close()

	isDelete := request.Role == ""

	if isDelete {
		err = db.DB(config.DBName).C(config.UserBranch).Update(
			bson.M{"_id": bson.ObjectIdHex(request.UserID)},
			bson.M{"$unset": bson.M{config.UserRooms + "." + request.RoomID + ".role": 1}},
			// bson.M{"multi": true},
		)
		fmt.Println(err)
		go model.RemoveUserInRoom(request.RoomID, request.UserID)

	} else {
		user, err := model.GetUser(request.UserID)
		if err != nil {
			return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "Please try later"}
		}

		//set role under user branch
		go model.AddRoomInUser(request.UserID, request.RoomID, request.Role)

		// if err != nil {
		// 	if err = db.DB(config.DBName).C(config.UserBranch).Update(
		// 		bson.M{"_id": bson.ObjectIdHex(request.UserID)},
		// 		bson.M{"$set": bson.M{config.UserRooms + "." + request.RoomID + "." + config.UserRole: request.Role}},
		// 	); err != nil {
		// 		// fmt.Println(err)
		// 		return err
		// 	}
		// }

		//duplicate copy of user data under room branch
		go model.AddUserInRoom(request.RoomID, user, request.Role)
	}

	return c.JSON(http.StatusOK, "success")
}

func RoomInsight(c echo.Context) (err error) {
	//seriously this method needs lot of improvements. not sure if the customer is around thousand it will still standing or not
	transactions := model.GetTransaction(c.QueryParam("room_id"))

	// var usersHistory map[string]interface{}
	// json.Unmarshal([]byte(`{}`), &usersHistory)
	usersHistory, numOfRedeem, numOfCust := logic.GroupUserFromTransaction(transactions)
	// for _, t := range transactions {
	// 	if t.Point < 0 {
	// 		numOfRedeem++
	// 	}
	// 	if val, ok := usersHistory[t.UserID]; ok {
	// 		//do something here
	// 		usersHistory[t.UserID] = val.(int) + t.Point
	// 	} else {
	// 		usersHistory[t.UserID] = t.Point
	// 	}
	// }

	numOfUsers := numOfCust
	// numOfUsers := len(usersHistory)
	maxCardNum := 0
	var numOfCardNumOfUser map[string]interface{}
	json.Unmarshal([]byte(`{}`), &numOfCardNumOfUser)
	for _, v := range usersHistory {
		if v.(int) > maxCardNum {
			maxCardNum = v.(int)
		}
		if _, ok := numOfCardNumOfUser[strconv.Itoa(v.(int))]; ok {
			//do something here
			numOfCardNumOfUser[strconv.Itoa(v.(int))] = numOfCardNumOfUser[strconv.Itoa(v.(int))].(int) + 1
		} else {
			numOfCardNumOfUser[strconv.Itoa(v.(int))] = 1
		}
	}

	//to give nice format data
	var finalNumOfCardNumOfUser map[string]interface{}
	json.Unmarshal([]byte(`{}`), &finalNumOfCardNumOfUser)
	for i := 0; i <= 5; i++ {
		if numOfCardNumOfUser[strconv.Itoa(i)] != nil {
			finalNumOfCardNumOfUser[strconv.Itoa(i)] = numOfCardNumOfUser[strconv.Itoa(i)].(int)
		} else {
			finalNumOfCardNumOfUser[strconv.Itoa(i)] = 0
		}
	}
	var results map[string]interface{}
	json.Unmarshal([]byte(`{}`), &results)

	results["totalCustomers"] = numOfUsers
	// results["stamps"] = numOfCardNumOfUser
	results["stamps"] = finalNumOfCardNumOfUser
	results["numOfRedeem"] = numOfRedeem

	return c.JSON(http.StatusOK, results)
}
