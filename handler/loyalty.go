package handler

import (
	"net/http"
	"strconv"

	config "bitbucket.org/nexlife/chopchop-backend/config"
	utils "bitbucket.org/nexlife/chopchop-backend/utils"

	"bitbucket.org/nexlife/chopchop-backend/model"

	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"
)

//GivePointRequest is parameters while giving points. param point will be incremented with existing
type GivePointRequest struct {
	RoomID string `json:"room_id" form:"room_id" bson:"room_id"`
	UserID string `json:"user_id" form:"user_id" bson:"user_id"`
	Point  int    `json:"point" form:"point" bson:"point"`
}

//MAX_NUM_OF_CARD specify the number of card allowed
var MAX_NUM_OF_CARD = 1

//GivePoint is to give points to specific user in specific room
func (h *Handler) GivePoint(c echo.Context) (err error) {
	request := new(GivePointRequest)
	if err = c.Bind(request); err != nil {
		return err
	}
	db := h.DB.Clone()
	defer db.Close()

	user, err := model.GetUser(request.UserID)
	if err != nil {
		return err
	}

	isRedeem := request.Point < 0
	//this means redeem. need to check whether user has enough points to redeem
	if isRedeem {

		if user.Rooms != nil && user.Rooms[request.RoomID] != nil { //&& user.Rooms[request.RoomID][config.LoyaltyPoint] != nil && user.Rooms[request.RoomID][config.LoyaltyPoint] < request.Point {
			// key := fmt.Sprintf("%s.%s.%s", config.UserRooms, request.RoomID, config.LoyaltyPoint)
			// key := fmt.Sprintf("%s.%s", request.RoomID, "loyalty_point")
			// key := fmt.Sprintf("%s.%s", request.RoomID, config.LoyaltyPoint)
			// key := fmt.Sprintf("%s", request.RoomID)
			// fmt.Println(key)
			// erro := json.Unmarshal(user.Rooms[request.RoomID], &rooms)
			// md, ok := mdi.(map[string]interface{})

			room := user.Rooms[request.RoomID].(bson.M)
			if room[config.LoyaltyPoint] == nil || (room[config.LoyaltyPoint].(int)+request.Point < 0) {
				return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "Not enough points to redeem"}
			}
		} else {
			return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "Not enough points to redeem"}
		}
	}

	if isRedeem {
		_ = db.DB(config.DBName).C(config.UserBranch).Update(
			bson.M{"_id": bson.ObjectIdHex(request.UserID)},
			bson.M{
				"$inc": bson.M{
					config.UserRooms + "." + request.RoomID + "." + config.LoyaltyPoint:         request.Point,
					config.UserRooms + "." + request.RoomID + "." + config.LoyaltyPointRedeemed: (-1 * request.Point),
				},
			},
		)
	} else {

		roomObject, _ := model.GetRoom(request.RoomID)

		if roomObject.IsOneCustomerOneStamp {
			if model.GetTotalPointsForUser(request.UserID, request.RoomID) > 0 {
				return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "User already received a stamp from " + roomObject.Name}
			}

		}

		if user.Rooms[request.RoomID] != nil && !roomObject.IsUnlimitedStamp {
			room := user.Rooms[request.RoomID].(bson.M)
			if room[config.LoyaltyPoint] != nil && room[config.LoyaltyPoint] != "" && (room[config.LoyaltyPoint].(int)+request.Point > ((config.NumOfStampsPerCard) * MAX_NUM_OF_CARD)) {
				return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "Adding " + strconv.Itoa(request.Point) + " stamp(s) will make you exceed " + strconv.Itoa(MAX_NUM_OF_CARD) + " card. Please redeem first in order to receive more rewards"}
			}
		}

		_ = db.DB(config.DBName).C(config.UserBranch).Update(
			bson.M{"_id": bson.ObjectIdHex(request.UserID)},
			bson.M{"$inc": bson.M{config.UserRooms + "." + request.RoomID + "." + config.LoyaltyPoint: request.Point}},
		)
	}

	loyaltyTransaction := &model.LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: request.UserID,
		Point:  request.Point,
		ByID:   userIDFromToken(c),
	}

	room, _ := model.GetRoom(request.RoomID)

	go model.AddTransaction(request.RoomID, loyaltyTransaction)

	// if err = db.DB(config.DBName).C(config.TransactionBranch + "." + request.RoomID).Insert(loyaltyTransaction); err != nil {
	// 	return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Sorry, please try later"}
	// }
	message := "You've received " + strconv.Itoa(request.Point) + " stamp"
	if request.Point > 1 {
		message += "s"
	}
	data := `"reason":"reward"`
	data = `"reason":"redeem", "title":"` + room.Name + `", "body":"Yeay! You have successfully collected your stamp."`
	if isRedeem {
		message = `You redeemed your card(s)`
		data = `"reason":"redeem", "title":"` + room.Name + `", "body":"Yeay! You have successfully redeem your card."`
	}
	if request.Point != 0 {
		go utils.PushNotificationUsers(message, []string{request.UserID}, data, config.NewCustomerBundleID)
	}
	go NotifyPleaseRefreshLeaderBoard()
	return c.JSON(http.StatusOK, "success")
}

func ResetEvent(c echo.Context) (err error) {
	eventID := c.QueryParam("event_id")
	rooms := []*model.Room{}
	if eventID != "" {
		rooms, _ = model.GetRoomBasedOnEventID(eventID)
		for _, room := range rooms {
			transactions := model.GetTransaction(room.ID.Hex())
			for _, t := range transactions {
				model.EmptyRoomLoyaltyPointInUser(t.UserID, room.ID.Hex())
			}
			model.ClearAllTransactionInRoom(room.ID.Hex())
		}
		return c.JSON(http.StatusOK, "Event resetted")
	}
	return c.JSON(http.StatusOK, "Event ID is null")
}
