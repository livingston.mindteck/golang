package handler

import (
	"bitbucket.org/nexlife/chopchop-backend/model"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"
	"io/ioutil"
	"mime"
	"net/http"
	"path/filepath"
	"strconv"
	"time"
)

func UploadImage(c echo.Context) error {
	name := c.QueryParam("name")
	file, err := c.FormFile("image")
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	f, err := file.Open()
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	defer f.Close()

	data, err := ioutil.ReadAll(f)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}

	image := &model.Image{ID: bson.NewObjectId()}
	image.Data = data
	image.Name = name
	image.UpdatedAt = int(time.Now().Unix())

	if err := model.UploadImage(image); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Sorry, please try later"}
	}

	image.Data = nil
	return c.JSON(http.StatusCreated, image)
}

func DownloadImage(c echo.Context) error {
	id := c.QueryParam("imageid")
	image, err := model.DownloadImage(id)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}

	c.Response().Header().Set("Content-Length", strconv.Itoa(len(image.Data)))
	c.Response().Header().Set("Content-Disposition", "inline; filename=\""+image.Name+"\"")
	c.Response().Header().Set("Content-Type", mime.TypeByExtension(filepath.Ext(image.Name)))

	return c.Blob(http.StatusOK, mime.TypeByExtension(filepath.Ext(image.Name)), image.Data)
}
