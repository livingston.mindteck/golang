package handler

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"bitbucket.org/nexlife/chopchop-backend/model"
)

//SendMessageStruct is
type SendMessageStruct struct {
	ReceiverID string `json:"receiver_id" form:"receiver_id" bson:"receiver_id"`
	Message    string `json:"message" form:"message" bson:"message"`
}

func SendMessage(c echo.Context) (err error) {
	userID := userIDFromToken(c)
	request := new(SendMessageStruct)
	c.Bind(request)
	model.SendMessage(userID, request.ReceiverID, request.Message)
	return c.JSON(http.StatusOK, "success")
}

func GetMessages(c echo.Context) (err error) {
	results := model.GetMessages(userIDFromToken(c))
	return c.JSON(http.StatusOK, results)
}
