package handler

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/nexlife/chopchop-backend/config"
	merchant "bitbucket.org/nexlife/chopchop-backend/dbservices/merchant"
	olt "bitbucket.org/nexlife/chopchop-backend/dbservices/outlet"
	"bitbucket.org/nexlife/chopchop-backend/logic"
	"bitbucket.org/nexlife/chopchop-backend/model"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"
)

func createOutlet(outlet *model.Outlet) error {
	cnt, err := olt.Count(map[string]interface{}{"uid": outlet.UnifiID})
	if err != nil {
		fmt.Println("Outlet check completed")
		return errors.New("Sorry, please try later")

	}

	if cnt >= config.UNFID_LIMIT {
		return errors.New("Please use another Unifi ID" + strconv.Itoa(config.UNFID_LIMIT))
	}

	var acc *model.MerchantAccount

	fmt.Println("Test -" + outlet.MerchantId)
	if acc, err = merchant.GetById(outlet.MerchantId); err != nil {
		return errors.New("Merchant account not found")
	}

	if outlet.Staff != "" {
		if !logic.IsValidEmail(outlet.Staff) {
			return errors.New("Invalid Staff Email")
		} else if AssignedOutlet, err := olt.IsStaffAlreadyAsigned(outlet.Staff); err == nil {
			return errors.New(outlet.Staff + " is already assigned to " + AssignedOutlet.Name)
		}

		fmt.Println("Staff Check COmpleted")
		if !model.IsPortalUserAvailable(outlet.Staff) {
			model.CreatePortalUser(outlet.Staff, logic.HashPassword("dummy"), model.UserType.Staff)
		}

		fmt.Println("User Availablity Check COmpleted")
	}
	if acc.ApprovalStatus == model.StatusApproved {
		outlet.ApprovalStatus = model.StatusApproved
	} else {
		outlet.ApprovalStatus = model.StatusInProcess
	}
	outlet.Ispremium = false
	if acc.Isnew == true {
		outlet.Isfreemium = true
		outlet.Noofcards = config.CARD_LIMIT
	} else {
		outlet.Isfreemium = false
		outlet.Noofcards = 0
	}

	if _, err = olt.Create(outlet); err != nil {
		return errors.New("Sorry, please try later")
	}
	if acc.ApprovalReqSent == false && acc.ApprovalRequired == true {
		// send brand approval request to approver
		if err = merchant.Patch(acc.ID.Hex(), map[string]interface{}{
			"approvalReqSent": true,
		}); err != nil {
			if err == mgo.ErrNotFound {
				return echo.ErrNotFound
			}
		}
	} else if acc.Isnew == true {
		if err = merchant.Patch(acc.ID.Hex(), map[string]interface{}{
			"isnew": false,
		}); err != nil {
			if err == mgo.ErrNotFound {
				return echo.ErrNotFound
			}
		}
	}
	return nil
}

func CreateOutlet(c echo.Context) (err error) {
	outlet := &model.Outlet{ID: bson.NewObjectId()}
	if err = c.Bind(outlet); err != nil {
		return err
	}
	// if !logic.IsValidEmail(outlet.Staff) {
	// 	return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid Staff Email"}
	// }
	if err = createOutlet(outlet); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	return c.JSON(http.StatusCreated, outlet.ID)
}

func ListOutlets(c echo.Context) error {
	key := c.QueryParam("outletid")
	outlets, err := olt.Search(key)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	return c.JSON(http.StatusOK, outlets)
}

type updateOutletReq struct {
	*model.Outlet
	StatusId string `json:"statusid" bson:"statusid"`
	Title    string `json:"title" bson:"title"`
}

const (
	deactivate  = "Success Deactivate"
	pendingbill = "pending bill"
	successbill = "success bill"
)

type DeactivateID struct {
	Outletid string
}

func ActivateOutlet(c echo.Context) (err error) {
	req := new(DeactivateID)
	if err = c.Bind(req); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	db := model.DB.Clone()
	defer db.Close()
	status, err := model.QuerryOutletStatusByTitle(successbill)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}

	var o *model.Outlet
	if o, err = olt.GetById(req.Outletid); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	if len(status) == 0 && status[0].ID != o.Status {
		return errors.New("Billing status is not Updated")
	}

	if err = olt.Patch(o.ID.Hex(), map[string]interface{}{
		"noofcards":  "0",
		"ispremium":  "true",
		"isfreemium": "false",
	}); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}

	if err = olt.Patch(o.ID.Hex(), map[string]interface{}{
		"outletstatus": 1,
	}); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}

	return nil
}

func DeactivateOutlet(c echo.Context) (err error) {
	req := new(DeactivateID)
	if err = c.Bind(req); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}

	status, err := model.QuerryOutletStatusByTitle(deactivate)
	pbstatus, err := model.QuerryOutletStatusByTitle(pendingbill)

	if err == nil && len(status) == 0 {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Sorry, please Add deactivate status in outletdeactivate"}
	}

	var o *model.Outlet
	if o, err = olt.GetById(req.Outletid); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	if len(pbstatus) > 0 && pbstatus[0].ID == o.Status {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Sorry, You cannot deactivate while bill is pending"}
	}

	o.Status = status[0].ID
	if _, err = olt.Update(o); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}

	if OitemCount, err := olt.Count(map[string]interface{}{
		"outletstatus": bson.M{"$ne": o.Status},
		"merchantId":   o.MerchantId,
	}); OitemCount == 0 && err == nil {
		if err := merchant.Patch(o.MerchantId, map[string]interface{}{
			"accStatus": model.StatusDeactivated,
		}); err != nil {
			return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
		}
	}
	return c.JSON(http.StatusOK, req.Outletid)
}

func UpdateOutlet(c echo.Context) (err error) {
	req := new(updateOutletReq)
	if err = c.Bind(req); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}

	var o *model.Outlet
	if o, err = olt.GetById(req.ID.Hex()); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Outlet Not Found"}
	}
	if strings.Trim(req.Staff, " ") != "" {
		if AssignedOutlet, err := olt.IsStaffAlreadyAsigned(req.Staff); err == nil && AssignedOutlet.ID != req.Outlet.ID {
			return errors.New(AssignedOutlet.Staff + " is already assigned to " + AssignedOutlet.Name)
		}
		if !model.IsPortalUserAvailable(req.Outlet.Staff) {
			model.CreatePortalUser(req.Outlet.Staff, logic.HashPassword("dummy"), model.UserType.Staff)
		}
	}
	req.MerchantId = o.MerchantId
	req.ApprovalStatus = o.ApprovalStatus
	req.CreatedAt = o.CreatedAt

	if req.Status != o.Status {
		model.UpdateBillingStatus(req.Outlet)
	}

	req.Status = o.Status

	if _, err = olt.Update(req.Outlet); err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}

	if req.StatusId != "" && req.Title != "" {
		if err = model.UpdateOutletStatus(req.StatusId, "title", req.Title); err != nil {
			return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
		}
	}
	return c.JSON(http.StatusOK, req.ID)
}

func GetOutlets(c echo.Context) (err error) {
	key := c.QueryParam("key")
	value := c.QueryParam("value")
	if key == "" || value == "" {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid Query"}
	}
	outlets, err := olt.FindAll(map[string]interface{}{key: value})
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	return c.JSON(http.StatusOK, struct {
		Outlets []*model.Outlet `json:"outlets"`
	}{Outlets: outlets})
}
func (h *Handler) GetOutletApprover(c echo.Context) (err error) {
	e_ail := model.GetApprover()

	return c.JSON(http.StatusOK, e_ail)
}

func (h *Handler) SetOutletApprover(c echo.Context) (err error) {
	request := new(model.EmailApprover)
	_ = c.Bind(request)
	model.SetApprover(request.UserID)
	return c.JSON(http.StatusOK, request)
}
