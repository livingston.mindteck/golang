package handler

import (
	"errors"
	"strings"
	"testing"

	"bitbucket.org/nexlife/chopchop-backend/config"
	"bitbucket.org/nexlife/chopchop-backend/model"
	"github.com/globalsign/mgo/bson"
	"github.com/stretchr/testify/assert"
)

func TestCreateOutlet(t *testing.T) {
	id := bson.NewObjectId()
	merchantId := bson.NewObjectId().String()
	merchantId = merchantId[strings.Index(merchantId, "\"")+1 : strings.LastIndex(merchantId, "\"")]
	tests := []struct {
		outlet *model.Outlet
		expErr error
	}{
		{
			outlet: &model.Outlet{
				ID:         id,
				Name:       "outlet1",
				RegnNum:    "1234567",
				UnifiID:    "123xyz",
				Email:      "abc@xyz.com",
				MerchantId: createMerchanAcc(merchantId),
			},
			expErr: nil,
		},
		{
			outlet: &model.Outlet{
				ID:         id,
				Name:       "outlet2",
				MerchantId: merchantId,
			},
			expErr: errors.New("The outlet already exists"),
		},
		{
			outlet: &model.Outlet{
				ID:         bson.NewObjectId(),
				Name:       "outlet3",
				MerchantId: "5c8f42f1a10a6810988a3376",
			},
			expErr: errors.New("Merchant account not found"),
		},
	}

	for i, _ := range tests {
		err := createOutlet(tests[i].outlet)
		assert.Equal(t, tests[i].expErr, err)
	}
}

func createMerchanAcc(id string) string {
	db := model.DB.Clone()
	defer db.Close()

	acc := &model.MerchantAccount{ID: bson.ObjectIdHex(id), Email: "test@test.com"}
	_ = db.DB(config.DBName).C(config.MerchantBranch).Insert(acc)

	return acc.ID.String()[strings.Index(acc.ID.String(), "\"")+1 : strings.LastIndex(acc.ID.String(), "\"")]
}
