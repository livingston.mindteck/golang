package config

import (
	"log"
	"os"
	"strconv"
)

const (
	// Key (Should come from somewhere else).
	Key = "secret"
)

//DBName set the MongoDB database to be used
var DBName = os.Getenv("DB_NAME")

//DBHost set the MongoDB database server address
var DBHost = os.Getenv("DB_HOST")

//SparkPostAPIKey set the key needed for SparkPost API
var SparkPostAPIKey = os.Getenv("SPARKPOST_API_KEY")

//Domain set the domain the application is deployed on
var Domain = os.Getenv("DOMAIN")

//set the email domain to be used
var EmailDomain = os.Getenv("EMAIL_DOMAIN") // "gmail.com"

//EmailSender set the sender email used in deployment
var EmailSender = /*"chopchoptesting@gmail.com" */ os.Getenv("EMAIL_SENDER")

//set the sender email password used in deployment
var EmailPassword = /*"webechop" */ os.Getenv("EMAIL_PASSWORD")

//set the smtp used in deployment
var EmailHost = /*"smtp.gmail.com" */ os.Getenv("EMAIL_HOST")

//set the port used in deployment
var EmailPort = /*"465" */ os.Getenv("EMAIL_PORT")

//set paginationSize
var PaginationSize = 10 //strconv.Atoi(os.Getenv("PAGINATION_SIZE"))

//BcryptCost is the encryption scheme used by the app
var BcryptCost, bcryptErr = strconv.Atoi(os.Getenv("BCRYPT_COSR"))

//SSOServer sets the SSO Service endpoint URL to connect to
var SSOServer = os.Getenv("SSO_SERVER")

//UNFID_LIMIT is the limit of outlets for the creation of Outlets
// var UNFID_LIMIT, err = strconv.Atoi(os.Getenv("UNFID_LIMIT"))
var UNFID_LIMIT = 5

//CARD_LIMIT is the limit of outlets for the creation of Outlets
// var CARD_LIMIT, _ = strconv.Atoi(os.Getenv("CARD_LIMIT"))
var CARD_LIMIT = 100

//NumOfStampsPerCard sets the number of stamps allowed
var NumOfStampsPerCard, numOfStampsPerCardErr = strconv.Atoi(os.Getenv("NUM_OF_STAMPS_PER_CARD"))

//CustomerBundleID sets the bundle id for customer android app
var CustomerBundleID = "my.com.tm.loyalty.app"

//NewCustomerBundleID sets the bundle id for new customer
var NewCustomerBundleID = "nexlife.app"

//MerchantBundleID sets the bundle if for merchants andrid app
var MerchantBundleID = "nexlife.business"

//MerchantBundleIDIOS sets the merchant app bundle id for IOS
var MerchantBundleIDIOS = "nexlife.merchant.second"

//OnesignalURL sets the URL of notification service of Onesignal
var OnesignalURL = "https://onesignal.com/api/v1/notifications"

//RoomUploadDir sets the directory for uploading banner images
var RoomUploadDir = os.Getenv("ROOM_UPLOAD_DIR")

//Env is environment of project. can be local / staging / production
var Env = os.Getenv("ENV")

//PortNumber sets the listening port for this server
var PortNumber = os.Getenv("PORT_NUMBER")

func init() {
	if bcryptErr != nil {
		log.Fatal("BCRYPT_COSR not exist or wrong format in env")
	}

	if numOfStampsPerCardErr != nil {
		log.Fatal("NUM_OF_STAMPS_PER_CARD not exist or wrong format in env")
	}

	if DBName == "" {
		log.Fatal("cannot find DB_NAME from env")
	}

	if DBHost == "" {
		log.Fatal("cannot find DB_HOST from env")
	}

	if RoomUploadDir == "" {
		log.Fatal("cannot find ROOM_UPLOAD_DIR from env")
	}
	//will be created during fileupload, therefore no need now
	// if _, err := os.Stat(RoomUploadDir); os.IsNotExist(err) {
	// 	os.MkdirAll(RoomUploadDir, os.ModePerm)
	// }
	if SparkPostAPIKey == "" {
		log.Fatal("cannot find SPARKPOST_API_KEY from env")
	}
	if Domain == "" {
		log.Fatal("cannot find DOMAIN from env")
	}
	if EmailDomain == "" {
		log.Fatal("cannot find EMAIL_DOMAIN from env")
	}
	if EmailSender == "" {
		log.Fatal("cannot find EMAIL_SENDER from env")
	}

	if Env == "" {
		log.Fatal("cannot find ENV from env")
	}
	if PortNumber == "" {
		log.Fatal("cannot find PORT_NUMBER from env")
	}
}

//IsProduction to check whether environment is production
func IsProduction() bool {
	return Env == "production"
}
