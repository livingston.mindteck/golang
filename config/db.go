package config

// this is list of branches / tables in database
var UserBranch = "users"
var PortalUser = "portalusers"
var IDFAUserBranch = "idfa_user"
var OnesignalUserBranch = "onesignal_user"
var StaticBranch = "static"
var RoomBranch = "room"
var TokenUserBranch = "token_user"
var RoomDetailBranch = "room_detail"
var MessageBranch = "message"

var AccountVerification = "verifyaccount"
var EmailTemplate = "emailtemplate"

// var LoyaltyTransactionBranch = "loyalty_transaction"
var TransactionBranch = "transaction"

var OutletBranch = "outlet"
var OutletStatusBranch = "outletstatus"
var BillingLog = "billinglog"

var UserRewardDetails = "usermerchantmap"

//users sub branch
//-------------------------

//UserRooms is a one user to many room relationship to indicate the user's role in the room.
var UserRooms = "rooms"

//UserRole is to define role of a user in the room
var UserRole = "role"

//end for users sub branch
//-----------------------------------------------------------------------------------------------------------------------------

//branches for loyalty
//-------------------------

var MerchantBranch = "merchantaccounts"
var ImageBranch = "images"

//LoyaltyPoint is branch to set loyalty points of the customer
var LoyaltyPoint = "loyalty_point"
var LoyaltyPointRedeemed = "loyalty_point_redeemed"

//end for loyalty
//-----------------------------------------------------------------------------------------------------------------------------
