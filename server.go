package main // import "bitbucket.org/nexlife/chopchop-backend"

import (
	"net/http"
	"time"

	//"log"
	// m "bitbucket.org/nexlife/chopchop-backend/middleware"

	"bitbucket.org/nexlife/chopchop-backend/handler"

	config "bitbucket.org/nexlife/chopchop-backend/config"

	"github.com/globalsign/mgo"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	l "github.com/labstack/gommon/log"

	//"github.com/carlescere/scheduler"
	//email "bitbucket.org/nexlife/chopchop-backend/service"
	auth "bitbucket.org/nexlife/chopchop-backend/service"
)

// @title IDP
// @version 1.0
// @description This is API spec for IDP.
// @termsOfService ht`tps://tm.com.my/

// @contact.name Abdulfattah
// @contact.url https://tm.com.my/
// @contact.email fattahmuhyiddeen@gmail.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host https://tm.com.my/
// @BasePath /url
func main_() {
	handler.RestoreFromScreenshot()
}
func main() {

	e := echo.New()

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{"*"},
	}))
	// sso := m.NewSSO()

	e.Logger.SetLevel(l.ERROR)
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	// e.Use(sso.HandleSSO())

	// Database connection
	db, err := mgo.Dial(config.DBHost)
	if err != nil {
		e.Logger.Fatal(err.Error())
	}

	// Initialize handler
	h := &handler.Handler{DB: db}

	//Cron Job
	//scheduler.Every().Day().At("08:30").Run(func() {
	/*scheduler.Every(15).Seconds().Run(func() {
		log.Println("Testing AAAAAAAA " );
		var toIds = []string{"raviteja.karri6@gmail.com"}
		email.SendEmail("Testing", toIds, "Its working")
	}) */

	secure := e.Group("/secure") //, merchanrGroup())
	secure.Use(auth.AuthorizeMerchantAPI)
	secure.GET("/jwt-timestamp-test", handler.Timestamp)
	secure.POST("/portal/logout", h.PortalLogout)
	secure.POST("/portal/setpassword", h.PortalSetPassword)
	secure.POST("/portal/dotransaction", h.DoTransaction)
	secure.GET("/portal/getcustomer", h.PortalGetCustomer)
	secure.GET("/outlet/home", handler.GetOutlets)
	secure.POST("/updateoutlet", handler.UpdateOutlet)
	secure.POST("/outlet", handler.CreateOutlet)
	secure.GET("/merchant/outlet", handler.ListOutlets)
	secure.POST("/merchant/outlet", handler.CreateOutlet)
	secure.GET("/merchant/transactions", h.GetTransactionsByMerchant)
	secure.GET("/portal/gettransactions", h.GetTransactions)
	secure.GET("/merchant/stampedredeemed", h.StampedRedeemed)
	secure.GET("/merchant/stampsbyuser", h.StampsByUser)
	secure.POST("/portal/deactivateoutlet", handler.DeactivateOutlet)
	secure.POST("/portal/activateoutlet", handler.ActivateOutlet)

	e.GET("/merchant/getbilllog", h.GetBillLog)
	e.GET("/merchant/getbilllogcount", h.GetBillLogCount)
	e.GET("/merchant/gettransactionscount", h.GetTransactionsCount)

	e.POST("/merchants", h.CreateMerchantAccount)
	e.GET("/portal/verify", h.PortalVerify)
	e.POST("/portal/forgot", h.PortalForgot)
	e.POST("/portal/login", h.PortalLogin)
	e.POST("/portal/signup", h.PortalSignup)
	e.POST("/portal/resetpassword", h.PortalResetPassword)
	e.GET("/resetPasswordPage", handler.ResetPasswordPage)
	e.POST("/merchants", h.CreateMerchantAccount)

	e.GET("/timestamp", handler.Timestamp)

	e.GET("/public/faq", func(c echo.Context) error {
		return c.File("public/faq.pdf")
	})

	e.GET("/public/tnc", func(c echo.Context) error {
		return c.File("public/tnc.pdf")
	})

	e.GET("/public/privacy", func(c echo.Context) error {
		return c.File("public/privacy.pdf")
	})

	e.Static("/public/html/reset-password", "public/html/reset-password")

	// Routes
	// Users
	e.POST("/signup", h.Signup)
	e.POST("/login", h.Login)
	e.GET("/verify", h.Verify)
	e.GET("/profile", h.GetProfile)
	e.POST("/profile", h.UpdateProfile)
	e.POST("/password", h.UpdatePassword)
	e.GET("/forget", h.RequestChangePassword)
	e.POST("/saveuser", h.SaveUser)

	e.Static("/public", "public")

	e.POST("/forget", h.ResetPassword)
	e.GET("/idfa", h.ProfileIDFA)
	e.POST("/logout", handler.Logout)

	e.GET("/transaction", h.History)
	e.GET("/user_with_most_chop", handler.UserWithMostChopRank)

	//room
	e.POST("/room/create", handler.CreateRoom)
	e.POST("/room/delete", h.DeleteRoom)
	e.POST("/room/edit", h.EditRoom)
	e.GET("/room/all", h.RoomsList)
	e.GET("/room/detail", h.RoomDetail)
	e.GET("/room/getUsersObject", h.GetUsersObject)
	e.POST("/room/manageUser", h.ManageUser)
	e.POST("/room/uploadImage", h.UploadImageRoom)
	e.GET("/room/insight", handler.RoomInsight)
	e.GET("/room/transaction_log", handler.TransactionLog)

	//static data
	e.GET("/get_static_data", handler.GetStaticData)
	e.POST("/set_static_data", handler.SetStaticData)

	//user
	e.GET("/user/search", handler.SearchUsers)
	e.GET("/user/biodata", h.GetBiodata)

	e.GET("/test", handler.TestFunc)

	//room files
	e.GET("/room/search", handler.SearchRooms)
	e.GET("/"+config.RoomUploadDir, h.RoomPublicFile)
	e.POST(config.RoomUploadDir+"/delete", h.DeleteRoomPublicFile)

	//For loyalty
	loyalty := "loyalty"
	e.POST("/"+loyalty+"/givePoint", h.GivePoint)

	e.GET("/bu", handler.BackupToSQLite)

	//Routes for specs
	// e.GET("/swagger/*", echoSwagger.WrapHandler)

	//message
	e.POST("/sendMessage", handler.SendMessage)
	e.GET("/getMessages", handler.GetMessages)

	//outlet

	//outlet status
	e.POST("/outletstatus", handler.CreateOutletStatus)
	e.GET("/outletstatus", handler.GetOutletStatus)
	e.DELETE("/outletstatus", h.DeleteOutletStatus)

	//websocket
	e.GET("/listen", handler.ListenWebsocket)

	//merchant app

	e.GET("/merchants", handler.ListMerchantAccounts)
	e.GET("/merchants/approval", h.BrandApproval)
	e.GET("/merchants/verification", h.MerchantAccVerification)
	e.GET("/portal/userverification", h.UserVefification)
	e.POST("/merchants/login", handler.MerchantLogIn)
	e.POST("/merchants/logout", handler.MerchantLogOut)
	e.POST("/merchants/password", handler.UpdateMerchantPassword)

	e.GET("/portal/sendtestemail", h.SendTestEmail)

	e.POST("/image", handler.UploadImage)
	e.GET("/image", handler.DownloadImage)

	e.POST("/sendMail", h.SendEmail)
	e.GET("/portal/getApprover", h.GetOutletApprover)
	e.POST("/portal/setApprover", h.SetOutletApprover)
	e.POST("/portal/updateemail", h.UpdateEmail)

	// Start server
	s := &http.Server{
		Addr:         ":" + config.PortNumber,
		ReadTimeout:  20 * time.Minute,
		WriteTimeout: 20 * time.Minute,
	}
	e.Logger.Fatal(e.Start(s.Addr))
}
