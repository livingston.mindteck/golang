package middleware

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"time"

	"testing"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	l "github.com/labstack/gommon/log"
	"github.com/stretchr/testify/assert"
)

//TestSSOCheackBearerToken  will test the scenario when the request header
// is checked for authorization token
func TestSSOCheckBearerToken(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(echo.GET, "/", nil)
	req.Header.Set("authorization", "bearer abcdefghijklmnop")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	sso := NewSSO()
	token, err := sso.CheckBearerToken(c)
	if err != nil {
		l.Error(err.Error())
	}
	assert.Equal(t, token, "abcdefghijklmnop")
}

//TestSSOQueryUserInfo is a fixture that setup a minimal mock identity server
func TestSSOQueryUserInfo(t *testing.T) {
	go fakeIdentityServer()
	time.Sleep(10 * time.Second)
	sso := NewSSO()
	token := "lTgXzaluOeSptnaFy8g4tVF3Buc6EQYwm1pUee088NA.LLebnBuE8M-oEWdhZHr0ChVQV_86aXY59UVaRY0oH8s"
	userInfo, err := sso.QueryUserInfo(token, "localhost:7777/userinfo")
	if err != nil {
		fmt.Println(err.Error())
	}
	assert.NotEmpty(t, userInfo.Email, "azzuwan@gmail.com")
}

//TestSSOHandlerFailure is a fixture to test failure scenario when request are made without
//authorization token
func TestSSOHandlerFailure(t *testing.T) {
	sso := NewSSO()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	res := httptest.NewRecorder()
	e := echo.New()
	c := e.NewContext(req, res)
	h := sso.HandleSSO()(func(c echo.Context) error {
		c.String(http.StatusOK, "test")
		return nil
	})
	h(c)
	fmt.Println(res.Code)
	assert.Equal(t, http.StatusUnauthorized, res.Code)
}

//fakeIdentityServer starts a minimal Echo server and return userInfo
func fakeIdentityServer() {
	// Echo instance
	e := echo.New()
	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	// Routes
	e.GET("/userinfo", userInfoHandler)
	// Start server
	// e.Logger.Fatal(e.StartTLS(":443", "./certs/cert.pem", "./certs/private.key"))
	e.Logger.Fatal(e.Start(":7777"))
}

// Handler
func userInfoHandler(c echo.Context) error {
	info := NewUserInfo()
	info.Name = "Azzuwan"
	info.Email = "azzuwan@gmail.com"
	return c.JSON(http.StatusOK, info)

}
