package middleware

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	config "bitbucket.org/nexlife/chopchop-backend/config"
	"github.com/labstack/echo/v4"
	l "github.com/labstack/gommon/log"
)

//UserInfo is a struct that store user info data from the SSO service /userinfo request
type UserInfo struct {
	Sub                 string `json:"sub"`
	CreatedAt           string `json:"created_at"`
	Name                string `json:"name"`
	Nickname            string `json:"nickname"`
	UpdatedAt           string `json:"updated_at"`
	Email               string `json:"email"`
	EmailVerified       bool   `json:"email_verified"`
	NationalID          string `json:"national_id"`
	NationalIDType      string `json:"national_id_type"`
	NationalIDVerified  bool   `json:"national_id_verified"`
	PhoneNumber         string `json:"phone_number"`
	PhoneNumberVerified bool   `json:"phone_number_verified"`
	PassportVerified    bool   `json:"passport_verified"`
}

//SSO is a struct that groups all the functionality of a struct
type SSO struct {
	UserInfo *UserInfo
}

//NewSSO is the constructor for the SSO object
func NewSSO() SSO {
	info := NewUserInfo()
	return SSO{
		UserInfo: info,
	}
}

//NewUserInfo is the constructor of UserInfo struct
func NewUserInfo() *UserInfo {
	return &UserInfo{
		Sub:                 "",
		CreatedAt:           "",
		Name:                "",
		Nickname:            "",
		UpdatedAt:           "",
		Email:               "",
		EmailVerified:       false,
		NationalID:          "",
		NationalIDType:      "",
		NationalIDVerified:  false,
		PhoneNumber:         "",
		PhoneNumberVerified: false,
		PassportVerified:    false,
	}
}

//RedirectMsg is the message that will be sent to user for redirect information
type RedirectMsg struct {
	Reason      string `json:"reason"`
	RedirectURL string `json:"redirect_url"`
}

//SSOConfig holds SSO configurations for future expansion
type SSOConfig struct {
	Type string
}

//NewSSOConfig creates a new SSOConfig object
func NewSSOConfig() SSOConfig {
	defaultConfig := SSOConfig{
		Type: "Authorization",
	}
	return defaultConfig
}

//HandleSSO is the main handler for the SSO middleware
func (sso *SSO) HandleSSO() echo.MiddlewareFunc {
	defaultConfig := NewSSOConfig()
	return sso.HandleSSOWithConfig(defaultConfig)
}

//HandleSSOWithConfig allows params to be passed to HandleSSO
func (sso *SSO) HandleSSOWithConfig(ssoConfig SSOConfig) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) (err error) {
			//Response message for reuquest without bearer token
			msg := &RedirectMsg{
				Reason:      "No bearer token",
				RedirectURL: config.SSOServer,
			}

			//Get the token
			token, err := sso.CheckBearerToken(c)
			if err != nil {
				//c.Error(err)
				l.Error(err)
				return c.JSON(http.StatusUnauthorized, msg)
			}
			userInfo, err := sso.QueryUserInfo(token, os.Getenv("IDENTITY_SERVER"))
			if err != nil {
				c.Error(err)
				l.Error("Error querying user info")
				return c.JSON(http.StatusUnauthorized, msg)
			}
			fmt.Println(userInfo)
			//This is not a good mechanism of verification. In case of DNS poisoning where the identity server url
			//is spoofed, the return value can be set to anything and this API server will just accept it.
			//Please consider using SAML or public key infrastructure in order to statelessly verify access.
			if userInfo.Name != "" && userInfo.Email != "" {
				//Pass to the next middleware
				if err = next(c); err != nil {
					c.Error(err)
				}
			}

			return nil
		}
	}
}

//CheckBearerToken perform check on request header for authorzation token
func (sso *SSO) CheckBearerToken(c echo.Context) (string, error) {
	//Get the authorization header
	header := c.Request().Header.Get("authorization")
	//Check if header exists
	if header != "" {
		//Get just the token part
		arr := strings.Split(header, " ")
		token := strings.TrimSpace(arr[1])
		fmt.Println("token:" + token)
		return token, nil
	}
	return "", errors.New("No valid bearer token found")
}

//QueryUserInfo sends the access token to /userinfo at the SSO service endpoint
func (sso *SSO) QueryUserInfo(token string, ssoEndpoint string) (*UserInfo, error) {
	//By default the stock http client does not specify a timeout.
	//We do not want to burden the SSO server with long live requests
	//That stay alive even after completion or error
	var client = &http.Client{
		Timeout: time.Second * 10,
	}
	url := "https://" + ssoEndpoint
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("authorization", "bearer "+token)
	res, err := client.Do(req)
	if err != nil {
		log.Println("Unable to request sso user info")
		log.Println(err.Error())
		return nil, err
	}
	fmt.Println(res.Status)
	//Re-initialize user info object so that the values come from current query
	sso.UserInfo = NewUserInfo()
	decoder := json.NewDecoder(res.Body)
	err = decoder.Decode(&sso.UserInfo)
	if err != nil {
		log.Println("Unable to decode userinfo response")
		log.Println(err.Error())
		return nil, err
	}
	return sso.UserInfo, nil
}
