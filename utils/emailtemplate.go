package utils

import (
	"bitbucket.org/nexlife/chopchop-backend/model"
)

const (
	// Assign access to outlet staff / Manager
	OutletAssignAcces = "outletAssignAccess"
	// Thank you e-mail when merchant successfully deactivated Freemium account
	FrremiumDeactivation = "freemiumDeactivation"
	// Freemium account - 80% card utilization
	FreemiumUtilization80 = "freemiumUtilization80"
	// Freemium account - 90% card utilization
	FreemiumUtilization90 = "freemiumUtilization90"
	// Freemium account - 5 days before expiry
	FreemiumDaysRemaining5 = "freemiumDaysRemaining5"
	// Freemium account - 2 days before expiry
	FreemiumDaysRemaining2 = "freemiumDaysRemaining2"
	// Freemium account - 100 card utilization / 2months usage and Notify premium charge
	FreemiumExpired = "freemiumExpired"
	// Notify "Success Bill"
	NotifySuccessBill = "notifySuccessBill"
	// Notify "Fail Bill" to business owner
	NotifyFailBillUser = "notifyFailBillUser"
	// Notification for "Fail Bill" SME sales
	NotifyFailBill = "notifyFailBill"
	// Final notification "success deactivate"
	OutletDeactivateSuccessFinal = "outletDeactivateSuccessFinal"
	// Notify error because have added more than 5 outlets for same Unifi login ID
	OutletAddLimitNotificationEmail = "outletAddLimitNotificationUser"
	// Fraud alert
	FraudAlert = "fraudAlert"
	// New account creation email verification
	VerifyEmailAccountCreation = "verifyEmailAccountCreation"
	// New portal account creation email verification
	VerifyEmailAccountCreationPortal = "verifyEmailAccountCreationPortal"
	// Forget password
	ForgetPassword = "forgetPassword"
	// Forget password customer
	ForgetPasswordCustomer = "forgetPasswordCustomer"
	// Forget password portal
	ForgetPasswordPortal = "forgetPasswordPortal"
	// Batch report (80% card utilization)
	BatchReportCardUsage80 = "batchReportCardUsage80"
	// Batch report (5 days before expiry)
	BatchReportDaysRemaining5 = "batchReportDaysRemaining5"
	// Process billing order for charging (batch)
	ProcessBillingOrderChargeBatch = "processBillingOrderChargeBatch"
	// Process billing reminder (3 business days)
	ProcessBillingReminderDays3 = "processBillingReminderDays3"
	// Process billing reminder (6 business days)
	ProcessBillingReminderDays6 = "processBillingReminderDays6"
	// Process deactivation order (batch)
	ProcessDeactivationOrderBatch = "processDeactivationOrderBatch"
	// Process deactivation reminder (3 business days)
	ProcessDeactivationReminderDays3 = "processDeactivationReminderDays3"
	// Process deactivation reminder (6 business days)
	ProcessDeactivationReminderDays6 = "processDeactivationReminderDays6"

	// Notifications
	// Notification "Success deactivate"
	OutletDeactivateSuccess = "outletDeactivateSuccess"
	// Notify success reactivation of outlet
	OutletReactivateSuccess = "outletReactivateSuccess"
	// Notify error because have added more than 5 outlets for same Unifi login ID - SME Sales
	OutletAddLimitNotification = "outletAddLimitNotification"
	// To key in unifi login ID
	KeyInUnifiLoginId = "keyInUnifiLoginId"
	// New account creation email verification
	AccountVerificationNotification = "accountVerificationNotification"
)

var Templates = []*model.EmailTemplate{
	{
		Title:   OutletAssignAcces,
		Subject: "Digital Stamp Card - App access",
		Body: `Hi,

You have been assigned an access to the Digital Stamp Card App.
Kindly click on the link below to access the page and change your password accordingly.
<%s>

Thank you.`,
	},
	{
		Title:   FrremiumDeactivation,
		Subject: " Sorry to see you go :(",
		Body: `Dear Customer, 

You have deactivated your Freemium package for Digital Stamp Card.
Do visit www.unifi.com.my/business when you consider to expand your business with our great solutions!

Thank you.`,
	},
	{
		Title:   FreemiumUtilization80,
		Subject: "Card utilization reminder",
		Body: `Dear Customer,

Thank you for using Digital Stamp Card.

This is a reminder that you have used up to 80% of the cards subscribed. 

If you wish to continue with the Premium package of Digital Stamp Card for your business, kindly ensure that you are a unifi Biz subscriber and proceed here (%s).

If you wish to deactivate this service, please deactivate here (%s)`,
	},
	{
		Title:   FreemiumUtilization90,
		Subject: "Card utilization reminder",
		Body: `Dear Customer,

Thank you for using Digital Stamp Card.

This is a reminder that you have utilized 90% cards in the Freemium package.
Once you have utilized your 100 cards, you will be automatically billed RM50 / month / outlet with unlimited cards. 

If you wish to deactivate this service, please deactivate here (%s)

Thank you.`,
	},
	{
		Title:   FreemiumDaysRemaining5,
		Subject: "Freemium access will expire",
		Body: `Dear Customer,

Thank you for using Digital Stamp Card.

This is a reminder that the Freemium package will expire in 5 days (%s). 
Once you have reached your 60days free access, you will be automatically billed RM50 / month / outlet. 

If you wish to deactivate this service, please deactivate here (%s).

Thank you.`,
	},
	{
		Title:   FreemiumDaysRemaining2,
		Subject: "Freemium access will expire",
		Body: `Dear Customer,

Thank you for using Digital Stamp Card.

This is a reminder that the Freemium package will expire in 2 days (%s). 
Once you have reached your 60days free access, you will be automatically billed RM50 / month / outlet. 

If you wish to deactivate this service, please deactivate here (%s).

Thank you.`,
	},
	{
		Title:   FreemiumExpired,
		Subject: "Premium Package",
		Body: `Dear Customer,

You have used up to 100% of the cards in Freemium package.

Your Freemium package has ended and you are now upgraded to the Premium package.

Thank you for your continuous support to this Digital Stamp Card service.
If you wish to deactivate this service, please deactivate here (%s).

Thank you.`,
	},
	{
		Title:   NotifySuccessBill,
		Subject: "Welcome to Premium Access",
		Body: `Dear Customer,

Please be informed that the Digital Stamp Card service of RM50/month will be charged to your monthly unifi Biz bill in the next billing cycle.

Thank you for subscribing to the Premium package of Digital Stamp Card.`,
	},
	{
		Title:   NotifyFailBillUser,
		Subject: "Unable to proceed with registration",
		Body: `Dear Customer,

We are unable to proceed with your request due to invalid unifi login ID.

Kindly re-register using a valid unifi login ID (example: john@unifi) 
OR
Kindly go to Live Chat (%s) or call 100 for assistance.

Thank you.`,
	},
	{
		Title:   FraudAlert,
		Subject: "Fraud alert",
		Body: `Hi,

We noticed irregularities in your outlet transactions. Please see attached for your info and further action.`,
	},
	{
		Title:   OutletDeactivateSuccessFinal,
		Subject: "Successfully deactivate outlet",
		Body: `Hi, 

We are sorry to see you go. 

Should you wish to continue using the Digital Stamp Card, just login to the app and click on your Outlet to reactivate it.

Hope to see you back soon.`,
	},
	{
		Title:   VerifyEmailAccountCreation,
		Subject: "Email Verification",
		Body: `You're verified!

Nice one. We were able to verify that %s is your valid e-mail address. 

Thank you for your interest to subscribe Digital Stamp Card with us. Please click "Verify E-mail Address" button below to continue with your registration. <a href="%s">Verify E-mail Address</a>  `,
	},
	{
		Title:   VerifyEmailAccountCreationPortal,
		Subject: "Digital Stamp Card - Portal access",
		Body: `Hi,

You have been assigned an access to the Digital Stamp Card Portal. Kindly click on the link below to access the page and change your password accordingly.

%s

Thank you.`,
	},
	{
		Title:   ForgetPassword,
		Subject: "Forgot password",
		Body: `Dear Customer,
There is a request to change password for the account registered to this e-mail address:

Username: %s

If you have requested for the password change, please <a href = "%s">clink here</a> to reset your password.

Thank you.`,
	},
	{
		Title:   ForgetPasswordCustomer,
		Subject: "Forgot password",
		Body: `Dear Customer,
There is a request to change password for the account registered to this e-mail address:

Username: %s

If you have requested for the password change, please clink here (%s) to reset your password.

Thank you.`,
	},
	{
		Title:   ForgetPasswordPortal,
		Subject: "Forgot password",
		Body: `Forgot yout password? 

No worries. Click on the link below to set your new password.

<%s>

Thank you.`,
	},
	{
		Title:   BatchReportCardUsage80,
		Subject: "Digital Stamp Card - Freemium Access (80% utilization)",
		Body: `Hi,

Kindly refer to the attached file on businesses that have utilized 80% of their 100 card.

Thank You.`,
	},
	{
		Title:   BatchReportDaysRemaining5,
		Subject: "Digital Stamp Card - Freemium access (5 days before expiry)",
		Body: `Hi,

Kindly refer to the attached file for businesses that will be upgraded to Premium in 5 days.

Thank You.`,
	},
	{
		Title:     NotifyFailBill,
		Receivers: []string{"jesher.lieow@webe.com.my"},
		Subject:   "Digital Stamp Card - Fail billing",
		Body: `Hi,

Kindly find attached list of outlets that were not able to be billed.

Thank You.`,
	},
	{
		Title:     OutletAddLimitNotificationEmail,
		Receivers: []string{"jesher.lieow@webe.com.my"},
		Subject:   "Digital Stamp Card - Exceed VAS allocation",
		Body: `Hi,

Kindly find attached list of Unifi ID that has exceeded the allocated 5 VAS. They have reached the maximum number of outlets per unifi login ID.`,
	},
	{
		Title: ProcessBillingOrderChargeBatch,
		Receivers: []string{"sahiful@tm.com.my",
			"norzarini.misrau@tm.com.my",
			"nurraihana@tm.com.my",
			"nofaizah@tm.com.my",
			"norawati.abdullah@tm.com.my",
			"dzaratil@tm.com.my",
			"norulnadiah@tsssb.com",
		},
		Subject: "Digital Stamp Card - Billing request",
		Body: `Dear unifi Operations Team,

Kindly refer to the attached batch file for the list of merchants, who would like to subscribe to Digital Stamp Card.

Please proceed with order creation in Siebel and Digital Stamp Card portal soonest possible.

Thank You.`,
	},
	{
		Title: ProcessBillingReminderDays3,
		Receivers: []string{"sahiful@tm.com.my",
			"norzarini.misrau@tm.com.my",
			"nurraihana@tm.com.my",
			"nofaizah@tm.com.my",
			"norawati.abdullah@tm.com.my",
			"dzaratil@tm.com.my",
			"norulnadiah@tsssb.com",
		},
		Subject: "Digital Stamp Card - Billing request (Reminder)",
		Body: `Dear unifi Operations Team,

You have yet to update the Digital Stamp Card portal to bill the following customer: <Outlet Id: %s>.

Your prompt action to update the portal is highly required and appreciated to avoid any service disruption to our customers.

Thank you.`,
	},
	{
		Title: ProcessBillingReminderDays6,
		Receivers: []string{"sahiful@tm.com.my",
			"norzarini.misrau@tm.com.my",
			"nurraihana@tm.com.my",
			"nofaizah@tm.com.my",
			"norawati.abdullah@tm.com.my",
			"dzaratil@tm.com.my",
			"norulnadiah@tsssb.com",
		},
		Subject: "Digital Stamp Card - Billing request (Second Reminder)",
		Body: `Dear unifi Operations Team,

You have yet to update the Digital Stamp Card portal to bill the following customer: <Outlet ID: %s>.

Your prompt action to update the portal is highly required and appreciated to avoid any service disruption to our customers.

Thank you.`,
	},
	{
		Title: ProcessDeactivationOrderBatch,
		Receivers: []string{"sahiful@tm.com.my",
			"norzarini.misrau@tm.com.my",
			"nurraihana@tm.com.my",
			"nofaizah@tm.com.my",
			"norawati.abdullah@tm.com.my",
			"dzaratil@tm.com.my",
			"norulnadiah@tsssb.com",
		},
		Subject: "Digital Stamp Card - Outlet Deactivation Request",
		Body: `Hi,

Kindly refer to the attached list for for the list of outlets to be deactivated.

Thank You.`,
	},
	{
		Title: ProcessDeactivationReminderDays3,
		Receivers: []string{"sahiful@tm.com.my",
			"norzarini.misrau@tm.com.my",
			"nurraihana@tm.com.my",
			"nofaizah@tm.com.my",
			"norawati.abdullah@tm.com.my",
			"dzaratil@tm.com.my",
			"norulnadiah@tsssb.com",
		},
		Subject: "Digital Stamp Card - Deactivation request (Second Reminder)",
		Body: `Hi, 

We have yet to receive update on the attached.

Please assist to process the outlet deactivation request.

Thank You.`,
	},
	{
		Title: ProcessDeactivationReminderDays6,
		Receivers: []string{"sahiful@tm.com.my",
			"norzarini.misrau@tm.com.my",
			"nurraihana@tm.com.my",
			"nofaizah@tm.com.my",
			"norawati.abdullah@tm.com.my",
			"dzaratil@tm.com.my",
			"norulnadiah@tsssb.com",
		},
		Subject: "Digital Stamp Card - Deactivation request (Reminder)",
		Body: `Hi,

We have yet to receive update on the attached.

Please assist to process the outlet deactivation request.

Thank You.`,
	},
	{
		Title: OutletDeactivateSuccess,
		Body:  `Your request to deactivate the outlet is being processed. You may continue to use it for the next 30 days until <%s>. Do visit www.unifi.com.my/business when you consider to expand your business with our great solutions!`,
	},
	{
		Title: OutletReactivateSuccess,
		Body:  `Your request to reactivate this outlet will be processed and any charges will be included in the next billing cycle.`,
	},
	{
		Title: OutletAddLimitNotification,
		Body: `You have already registered the maximum of 5 outlets to this unifi login ID for Digital Stamp Card.

To ensure more outlets can enjoy the Premium package for Digital Stamp Card, please use another unifi login ID. You can also register for another unifi Biz account here. (%s)`,
	},
	{
		Title: KeyInUnifiLoginId,
		Body: `Hi, you have not entered your unifi Login ID yet. If you wish to subscribe to unifi Biz, please visit unifi.com.my/business for our incredible business packages.

Thank you.`,
	},
	{
		Title: AccountVerificationNotification,
		Body:  `You have successfully completed the registration. We will process the registration and send you the confirmation via email soon`,
	},
}
