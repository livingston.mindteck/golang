package utils

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	config "bitbucket.org/nexlife/chopchop-backend/config"
	"bitbucket.org/nexlife/chopchop-backend/model"
)

func rescue() {
	r := recover() // We stopped here the failing process!!!
	if r != nil {
		fmt.Println("Panic has been recover")
	}
}

// PushNotificationUsers to
func PushNotificationUsers(message string, userIDs []string, data string, targetBundleID string) {
	users, _ := model.GetUsersBasedOnUsersID(userIDs)
	//index 0 = customer, index 1 = business, index 2 = new bundle id for customer
	playerIDs := []string{"", "", ""}
	onesignalIDs := []string{"437dcc0d-b922-4055-bad2-08fd3d67f540", "4e297971-95db-4e72-8131-3c4b4d545581", "b94df65d-b96c-4c88-aa86-61e9c7fd971c"}

	for _, ou := range users {
		oidIndex := -1
		if targetBundleID == "" {
			if ou.BundleID == config.MerchantBundleID || ou.BundleID == config.MerchantBundleIDIOS {
				oidIndex = 1
			} else if ou.BundleID == config.NewCustomerBundleID {
				oidIndex = 2
			} else {
				oidIndex = 0
			}
		} else {
			if targetBundleID == config.CustomerBundleID && ou.BundleID == config.CustomerBundleID {
				oidIndex = 0
			} else if (targetBundleID == config.MerchantBundleID && ou.BundleID == config.MerchantBundleID) || (targetBundleID == config.MerchantBundleIDIOS && ou.BundleID == config.MerchantBundleIDIOS) {
				oidIndex = 1
			} else if targetBundleID == config.NewCustomerBundleID && ou.BundleID == config.NewCustomerBundleID {
				oidIndex = 2
			}
		}
		if oidIndex >= 0 {
			playerIDs[oidIndex] += `"` + ou.OnesignalID + `",`
		}
	}

	for index, osid := range onesignalIDs {
		if playerIDs[index] != "" {
			// remove last character (trailing comma) in slice
			playerIDs[index] = playerIDs[index][:len(playerIDs[index])-1]
		} else {
			continue
		}

		payload := strings.NewReader(`{
		"app_id":"` + osid + `",
		"include_player_ids":[` + playerIDs[index] + `],
		"data":{` + data + `},
		"ios_badgeType":"Increase",
		"ios_badgeCount":1,
		"contents": {"en": "` + message + `"}
		}`)

		// fmt.Println(payload)

		// defer rescue()

		req, newRequestError := http.NewRequest("POST", config.OnesignalURL, payload)
		if newRequestError != nil {
			fmt.Println(newRequestError)
		} else {
			req.Header.Add("content-type", "application/json; charset=utf-8")
			req.Header.Add("cache-control", "no-cache")

			res, defaultClientError := http.DefaultClient.Do(req)
			if defaultClientError != nil {
				fmt.Println(defaultClientError)
			} else {
				body, err := ioutil.ReadAll(res.Body)
				fmt.Println(err)
				fmt.Println(string(body))
			}
			defer res.Body.Close()
		}
	}
}
