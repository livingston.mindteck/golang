package logic

import (
	"encoding/json"

	"bitbucket.org/nexlife/chopchop-backend/model"
)

func StructuredInsightsFromTransaction(data []model.LoyaltyTransaction) (results map[string]interface{}, numOfRedeem int) {
	_, numOfRedeem, _ = GroupUserFromTransaction(data)
	return results, numOfRedeem
}

func GroupUserFromTransaction(data []model.LoyaltyTransaction) (results map[string]interface{}, numOfRedeem int, numOfCust int) {
	json.Unmarshal([]byte(`{}`), &results)

	for _, t := range data {
		if t.Point < 0 {
			numOfRedeem++
		}
		if val, ok := results[t.UserID]; ok {
			//do something here
			// results[t.UserID] = val.(int) + t.Point
			if results[t.UserID] == 5 && t.Point > 0 {
				if val, ok := results[t.UserID+"_2"]; ok {
					results[t.UserID+"_2"] = val.(int) + t.Point
				} else {
					results[t.UserID+"_2"] = t.Point
				}
			} else {
				results[t.UserID] = val.(int) + t.Point
			}
		} else {
			results[t.UserID] = t.Point
			numOfCust++
		}
	}
	return results, numOfRedeem, numOfCust
}
