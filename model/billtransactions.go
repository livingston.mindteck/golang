package model

import (
	"time"

	config "bitbucket.org/nexlife/chopchop-backend/config"
	"github.com/globalsign/mgo/bson"
)

type Billing struct {
	ID         bson.ObjectId `json:"billingid" form:"billingid" bson:"_id,omitempty"`
	MerchantId string        `json:"merchantId" bson:"merchantId"`
	OutletId   string        `json:"outletId" bson:"outletId"`
	Remark     string        `json:"remark" bson:"remark"`
	StaffId    string        `json:"staffId" bson:"staffId"`
	Status     string        `json:"status" bson:"status"`
	StaffName  string        `json:"staffName" bson:"staffName"`
	CreatedAt  int           `json:"createdAt" bson:"createdAt"`
	UpdatedAt  int           `json:"updatedAt" bson:"updatedAt"`
}

func UpdateBillingStatus(outlet *Outlet) error {
	billing := Billing{ID: bson.NewObjectId()}

	db := DB.Clone()
	defer db.Close()
	billing.CreatedAt = int(time.Now().Unix())
	billing.UpdatedAt = billing.CreatedAt
	billing.MerchantId = outlet.MerchantId
	billing.OutletId = outlet.ID.Hex()
	billing.Remark = outlet.Remark
	billing.StaffId = outlet.Staff
	billing.StaffName = outlet.StaffName
	billing.Status = outlet.Status.Hex()

	if err := db.DB(config.DBName).C(config.BillingLog).Insert(billing); err != nil {
		return err
	}
	return nil
}
