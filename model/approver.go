package model

import (
	config "bitbucket.org/nexlife/chopchop-backend/config"
	"github.com/globalsign/mgo/bson"
)

type EmailApprover struct {
	ID     bson.ObjectId `json:"id" form:"id" bson:"_id,omitempty"`
	UserID string        `json:"userid" bson:"userid"` // Email Address
}

func GetApprover() (results *EmailApprover) {
	db := DB.Clone()
	defer db.Close()
	if err := db.DB(config.DBName).C("emailapprover").Find(nil).One(&results); err != nil {
		return new(EmailApprover)
	}
	return results
}

func SetApprover(email string) {
	o := GetApprover()
	db := DB.Clone()
	defer db.Close()
	o.UserID = email
	if o.ID == "" {
		_ = db.DB(config.DBName).C("emailapprover").Insert(o)
	} else {
		_ = db.DB(config.DBName).C("emailapprover").Update(nil, o)
	}
}
