package model

import (
	"github.com/globalsign/mgo/bson"
	config "bitbucket.org/nexlife/chopchop-backend/config"
)

//Static branch saved in db
type Static struct {
}

func staticInit() {
	// c := DB.Copy().DB(config.DBName).C(config.StaticBranch)
	// err := c.EnsureIndex(mgo.Index{
	// 	Key:    []string{"name"},
	// 	Unique: false,
	// })
	// if err != nil {
	// 	log.Fatal(err)
	// }
}

func SetStaticData(key string, value string) (err error) {
	db := DB.Clone()
	defer db.Close()

	// err = c.Find(bson.M{"name": regex, "isPublic": true}).All(&results)
	// if err != nil {
	// 	return err
	// }

	db.DB(config.DBName).C(config.StaticBranch).Update(
		nil,
		bson.M{"$set": bson.M{key: value}},
	)

	return err
}

func GetStaticData() (err error) {
	db := DB.Clone()
	defer db.Close()
	// c := db.DB(config.DBName).C(config.StaticBranch)

	// err = c.Find(bson.M{"name": regex, "isPublic": true}).All(&results)
	// if err != nil {
	// 	return err
	// }

	return err
}
