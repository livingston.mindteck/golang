package model

import (
	"encoding/json"
	"log"
	"strconv"

	config "bitbucket.org/nexlife/chopchop-backend/config"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

//User is model of user branch
type User struct {
	ID                  bson.ObjectId `json:"id" form:"id" bson:"_id,omitempty"`
	Email               string        `json:"email" form:"email" bson:"email"`
	Name                string        `json:"name" form:"name" bson:"name"`
	VerifyKey           string        `json:"verifyKey" bson:"verifyKey"`
	ForgotPasswordToken string        `json:"forgotPasswordToken" bson:"forgotPasswordToken"`
	Status              string        `json:"status" bson:"status"`
	Password            string        `json:"password,omitempty" bson:"password"`
	Token               string        `json:"token,omitempty" bson:"-"`
	Rooms               bson.M        `json:"rooms,omitempty" bson:"rooms,omitempty"` //this is user (single) to rooms (many) relationship indicating role and properties of the user in the room
	UpdatedAt           int           `json:"updatedAt" bson:"updatedAt"`
	// Rooms               interface{}   `json:"rooms,omitempty" bson:"rooms,omitempty"` //this is user (single) to rooms (many) relationship indicating role and properties of the user in the room
	// RoomActivity        interface{}   `json:"roomActivity,omitempty" bson:"roomActivity,omitempty"` // properties of user in specific room
	// Rooms               bson.M        `json:"rooms,omitempty" bson:"rooms,omitempty,inline"` //this is user (single) to rooms (many) relationship indicating role and properties of the user in the room
}

func userInit() {
	c := DB.Copy().DB(config.DBName).C(config.UserBranch)

	err := c.EnsureIndex(mgo.Index{
		Key:    []string{"email"},
		Unique: true,
	})
	if err != nil {
		log.Fatal(err)
	}

	err = c.EnsureIndex(mgo.Index{
		Key:    []string{"name"},
		Unique: false,
	})
	if err != nil {
		log.Fatal(err)
	}
}

func SearchUser(keyword string) (users []User) {
	db := DB.Clone()
	defer db.Close()

	regex := bson.RegEx{"(?i)^.*" + keyword + ".*$", ""}
	condition := bson.M{"name": regex}
	if keyword == "" {
		condition = nil
	}
	if err := db.DB(config.DBName).C(config.UserBranch).Find(condition).
		Select(bson.M{"rooms": 0, "token": 0, "password": 0, "forgotPasswordToken": 0, "verifyKey": 0}).
		All(&users); err != nil {
		if err == mgo.ErrNotFound {
			return []User{}
		}
	}
	return users
}
func GetUser(userID string) (user User, err error) {
	db := DB.Clone()
	defer db.Close()
	err = db.DB(config.DBName).C(config.UserBranch).FindId(bson.ObjectIdHex(userID)).One(&user)
	return user, err
}

// SavePointForUser is only used during chaos 17 Jan
func SavePointForUser(userID string, roomID string, point int) {
	db := DB.Clone()
	defer db.Close()

	user, _ := GetUser(userID)
	if user.Rooms[roomID] != nil && user.Rooms[roomID].(bson.M)["loyalty_point"] != nil {
		log.Println(user.Email + " - " + strconv.Itoa(user.Rooms[roomID].(bson.M)["loyalty_point"].(int)) + " - " + strconv.Itoa(point))
	}
	_ = db.DB(config.DBName).C(config.UserBranch).Update(
		bson.M{"_id": bson.ObjectIdHex(userID)},
		bson.M{"$inc": bson.M{config.UserRooms + "." + roomID + "." + config.LoyaltyPoint: point}},
		// bson.M{"$set": bson.M{config.UserRooms + "." + roomID + "." + config.LoyaltyPoint: point}},
	)
}

func CreateUser(email string, password string, name string) (id string) {
	db := DB.Clone()
	defer db.Close()
	// password = logic.HashPassword(u.Password)

	user := &User{ID: bson.NewObjectId()}
	user.Password = password
	user.Name = name
	user.Email = email
	db.DB(config.DBName).C(config.UserBranch).Insert(user)

	// fmt.Println(err)
	// fmt.Println(err)
	// fmt.Println(err)
	// fmt.Println(err)
	// fmt.Println(err)

	n := user.ID.Hex()
	// fmt.Println(err)
	// fmt.Println(err)
	// fmt.Println(err)
	// fmt.Println(err)
	// fmt.Println(err)
	return n
}

func GetUserFromEmail(email string) (user User, err error) {
	db := DB.Clone()
	defer db.Close()
	err = db.DB(config.DBName).C(config.UserBranch).Find(bson.M{"email": email}).One(&user)
	return user, err
}

func AddRoomInUser(userID string, roomID string, role string) {
	db := DB.Clone()
	defer db.Close()

	path := "rooms." + roomID

	db.DB(config.DBName).C(config.UserBranch).Update(
		bson.M{"_id": bson.ObjectIdHex(userID)},
		bson.M{"$set": bson.M{path: bson.M{"role": role}}},
	)
}

func EmptyRoomLoyaltyPointInUser(userID string, roomID string) {
	db := DB.Clone()
	defer db.Close()

	path := "rooms." + roomID

	db.DB(config.DBName).C(config.UserBranch).Update(
		bson.M{"_id": bson.ObjectIdHex(userID)},
		bson.M{"$unset": bson.M{
			path: bson.M{"loyalty_point": 1},
			path: bson.M{"loyalty_point_redeemed": 1},
		}},
	)
}

func AllUesrs() (users []User) {
	db := DB.Clone()
	defer db.Close()
	db.DB(config.DBName).C(config.UserBranch).Find(nil).All(&users)
	return users
}

func UserWithMostChopRank(arrRoomID []string) (results []map[string]interface{}) {
	db := DB.Clone()
	defer db.Close()

	users := []User{}
	db.DB(config.DBName).C(config.UserBranch).Find(nil).All(&users)
	if users == nil {
		return make([]map[string]interface{}, 0, 0)
	}
	for _, user := range users {
		var result map[string]interface{}
		json.Unmarshal([]byte(`{}`), &result)
		result["user_id"] = user.ID
		result["user_name"] = user.Name
		result["user_email"] = user.Email
		result["point"] = 0
		for key, room := range user.Rooms {
			isCondition := len(arrRoomID) == 0
			if !isCondition {
				for _, id := range arrRoomID {
					if key == id {
						isCondition = true
						break
					}
				}
			}
			if room.(bson.M)["loyalty_point"] != nil && isCondition {
				result["point"] = result["point"].(int) + room.(bson.M)["loyalty_point"].(int)
			}
		}
		results = append(results, result)
	}

	return results
}
