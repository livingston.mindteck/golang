package model

import (
	"github.com/globalsign/mgo/bson"
)

type EmailTemplate struct {
	Id        bson.ObjectId `json:"id" bson:"_id"`
	Title     string        `json:"title" bson:"title"`
	Receivers []string      `json:"receivers" bson:"receivers"`
	Subject   string        `json:"subject" bson:"subject"`
	Body      string        `json:"body" bson:"body"`
	CreatedAt int           `json:"createdAt" bson:"createdAt"`
	UpdatedAt int           `json:"updatedAt" bson:"updatedAt"`
}
