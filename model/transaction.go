package model

import (
	"errors"
	"fmt"
	"time"

	config "bitbucket.org/nexlife/chopchop-backend/config"
	"github.com/globalsign/mgo/bson"
)

type LoyaltyTransaction struct {
	ID     bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UserID string        `json:"user_id" bson:"user_id"`
	ByID   string        `json:"by_id" bson:"by_id"`
	Point  int           `json:"point" bson:"point"`
}

type LoyaltyTransactionLog struct {
	ID       bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UserID   string        `json:"user_id" bson:"user_id"`
	ByID     string        `json:"by_id" bson:"by_id"`
	Point    int           `json:"point" bson:"point"`
	Username string        `json:"username" bson:"username"`
}

type TransactionDetails struct {
	ID           bson.ObjectId `json:"id" form:"id" bson:"_id,omitempty"`
	OutletID     bson.ObjectId `json:"outletid" bson:"outletid,omitempty"`
	UserID       string        `json:"userid" bson:"userid"`   // Email Address
	StaffID      string        `json:"staffid" bson:"staffid"` // Email Address
	Name         string        `json:"name" bson:"Name"`       // Name of the customer
	Type         string        `json:"type" bson:"type"`       // stamp or reward
	NoofSr       int           `json:"noofsr" bson:"noofsr"`   // No of Stamp (or) Reward
	Modified     int           `json:"modified" bson:"modified"`
	Created      int           `json:"created" bson:"created"`
	StampedCount int           `json:"stampedcount" bson:"stampedcount"`
}
type UserStampDetails struct {
	ID               bson.ObjectId `json:"id" form:"id" bson:"_id,omitempty"`
	UserID           string        `json:"userid" bson:"userid"`
	Stamp            int           `json:"stamp" bson:"stamp"`
	Reward           int           `json:"reward" bson:"reward"`
	WaitingForReward int           `json:"waitingforreward" bson:"waitingforreward"`
	MerchantID       string        `json:"merchantid" bson:"merchantid"`
	ModifiedAt       int           `json:"modifiedat" bson:"modifiedat"`
	CreatedAt        int           `json:"createdat" bson:"createdat"`
}

func ChopTransaction(MerchantID string, tran *TransactionDetails) error {
	db := DB.Clone()
	defer db.Close()
	tran.Created = int(time.Now().Unix())
	tran.Modified = int(time.Now().Unix())

	if err := db.DB(config.DBName).C("mer" + MerchantID).Insert(tran); err != nil {
		return errors.New("Sorry, please try later")
	}
	return nil
}

func GetUserStamp(merchantid string, userid string) (*UserStampDetails, error) {
	db := DB.Clone()
	defer db.Close()
	us := &UserStampDetails{}
	if err := db.DB(config.DBName).C(config.UserRewardDetails).Find(bson.M{"merchantid": merchantid, "userid": userid}).One(us); err != nil {
		return us, err
	}
	fmt.Println(us)
	return us, nil
}

func GetTransactionByOutlet(Outlet_d string) (results []TransactionDetails, err error) {
	db := DB.Clone()
	defer db.Close()
	o := new(Outlet)
	if err = db.DB(config.DBName).C(config.OutletBranch).FindId(bson.ObjectIdHex(Outlet_d)).One(&o); err != nil {
		return nil, errors.New("Sorry please try later")
	}
	db.DB(config.DBName).C("mer" + o.MerchantId).Find(bson.M{"outletid": o.ID}).Sort("-$natural").All(&results)
	return results, nil
}

func GetTransactionsByMerchant(merchantId string, offset int, size int) (results []TransactionDetails, err error) {
	db := DB.Clone()
	defer db.Close()
	db.DB(config.DBName).C("mer" + merchantId).Find(nil).Sort("-$natural").Skip(offset * size).Limit(size).All(&results)
	return results, nil
}

// func GetUserTransactionsByStamps(merchantId string) (results []bson.M, err error) {
// 	db := DB.Clone()
// 	defer db.Close()

// 	pipe := db.DB(config.DBName).C("mer" + merchantId).
// 		Pipe([]bson.M{bson.M{"$match": bson.M{"type": "stamp"}},
// 			bson.M{"$group": bson.M{"_id": "$userid",
// 				"count": bson.M{"$sum": "$noofsr"}}}})
// 	// pipe := db.DB(config.DBName).C("mer" + merchantId).Pipe([]bson.M{{"$match": bson.M{"type": "stamp"}}})
// 	resp := []bson.M{}
// 	iter := pipe.Iter()
// 	err = iter.All(&resp)
// 	// err = pipe.All(&resp)
// 	// fmt.Println(len(resp))
// 	return resp, nil
// }
func GetRewards(merchantId string) ([]bson.M, error) {
	db := DB.Clone()
	defer db.Close()
	pipe := db.DB(config.DBName).C(config.UserRewardDetails).
		Pipe([]bson.M{bson.M{"$match": bson.M{"merchantid": merchantId}},
			bson.M{"$group": bson.M{"_id": "null",
				"count": bson.M{"$sum": "$reward"}}}})
	resp := []bson.M{}
	iter := pipe.Iter()
	err := iter.All(&resp)
	// cnt, err :=
	return resp, err
}

func GetStampedRedeemed(merchantid string) (int, int, error) {
	db := DB.Clone()
	defer db.Close()
	stamps, err1 := db.DB(config.DBName).C(config.UserRewardDetails).Find(bson.M{"merchantid": merchantid,
		"stamp": bson.M{"$gt": 0}}).Count()

	redeemed, err2 := db.DB(config.DBName).C(config.UserRewardDetails).Find(bson.M{"merchantid": merchantid,
		"reward": bson.M{"$gt": 0}}).Count()
	if err1 != nil || err2 != nil {
		return 0, 0, errors.New("Something Went Wrong!!!")
	}
	return stamps, redeemed, nil

}

//({$match:{merchantid:"5ca9c033502bfea87d8de36b"}},{$group:{_id:null,count:{$sum:"$reward"}}})
func GetUserTransactionsByStamps(merchantId string) (results []bson.M, err error) {
	db := DB.Clone()
	defer db.Close()

	pipe := db.DB(config.DBName).C(config.UserRewardDetails).
		Pipe([]bson.M{bson.M{"$match": bson.M{"merchantid": merchantId}},
			bson.M{"$group": bson.M{"_id": "$stamp",
				"count": bson.M{"$sum": 1}}}})

	/// db.usermerchantmap.aggregate([{$match:{merchantid:"5ca9c033502bfea87d8de36b"}},
	//{$group:{_id:"$stamp",count:{$sum:1}}}])
	// pipe := db.DB(config.DBName).C("mer" + merchantId).Pipe([]bson.M{{"$match": bson.M{"type": "stamp"}}})
	resp := []bson.M{}
	iter := pipe.Iter()
	err = iter.All(&resp)
	// err = pipe.All(&resp)
	// fmt.Println(len(resp))
	return resp, nil
}

func InsertUserStamp(uc *UserStampDetails) (string, error) {
	db := DB.Clone()
	defer db.Close()

	uc.CreatedAt = int(time.Now().Unix())
	uc.ModifiedAt = int(time.Now().Unix())
	if err := db.DB(config.DBName).C(config.UserRewardDetails).Insert(uc); err != nil {
		return "", err
	}
	return uc.ID.Hex(), nil
}

func UpdateUserStamp(uc *UserStampDetails) (string, error) {
	db := DB.Clone()
	defer db.Close()
	uc.ModifiedAt = int(time.Now().Unix())
	err := db.DB(config.DBName).C(config.UserRewardDetails).Update(bson.M{"_id": uc.ID}, uc)
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	return uc.ID.Hex(), nil
}

func GetTransactionByStaff(MerchantID string, Semail string) (results []TransactionDetails, err error) {
	db := DB.Clone()
	defer db.Close()

	db.DB(config.DBName).C("mer" + MerchantID).Find(bson.M{"staffid": Semail}).Sort("-$natural").All(&results)
	return results, nil
}

func GetTransaction(roomID string) (results []LoyaltyTransaction) {
	db := DB.Clone()
	defer db.Close()

	db.DB(config.DBName).C(config.TransactionBranch + "." + roomID).Find(nil).Sort("-$natural").All(&results)
	return results
}

func ClearAllTransactionInRoom(roomID string) {
	db := DB.Clone()
	defer db.Close()

	db.DB(config.DBName).C(config.TransactionBranch + "." + roomID).RemoveAll(nil)
}

func GetTotalPointsForUser(userID string, roomID string) (points int) {
	db := DB.Clone()
	defer db.Close()
	filter := bson.M{"user_id": userID}
	transactions := []LoyaltyTransaction{}

	if roomID != "" {

		db.DB(config.DBName).C(config.TransactionBranch + "." + roomID).Find(filter).All(&transactions)
		if transactions == nil {
			return 0
		}
		totalPoints := 0
		for _, t := range transactions {
			totalPoints += t.Point
		}

		return totalPoints
	}

	return 0
}

func TransactionLog(roomID string, userID string) (results []LoyaltyTransactionLog) {
	db := DB.Clone()
	defer db.Close()

	filter := bson.M{"by_id": userID}
	if userID == "" {
		filter = nil
	}

	c := db.DB(config.DBName).C("transaction." + roomID)

	query := []bson.M{
		{"$match": filter},
		{"$match": bson.M{"point": bson.M{"$ne": 0}}},
		{"$addFields": bson.M{"userId": bson.M{"$toObjectId": "$user_id"}}},
		{"$lookup": bson.M{"from": "users", "localField": "userId", "foreignField": "_id", "as": "user_detail"}},
		bson.M{"$unwind": "$user_detail"},
		{"$project": bson.M{"_id": 1, "user_id": 1, "by_id": 1, "point": 1, "username": "$user_detail.name"}},
		{"$sort": bson.M{"_id": -1}},
		{"$limit": 100},
	}

	pipe := c.Pipe(query)

	err := pipe.All(&results)
	if err != nil {
		//handle error
	}

	// db.DB(config.DBName).C("transaction." + roomID).Find(filter).Sort("-$natural").Limit(100).All(&results)

	if results == nil {
		return []LoyaltyTransactionLog{}
	}
	return results
}

func AddTransaction(roomID string, loyaltyTransaction *LoyaltyTransaction) {
	db := DB.Clone()
	defer db.Close()
	db.DB(config.DBName).C(config.TransactionBranch + "." + roomID).Insert(loyaltyTransaction)
}

func TransactionFakeData() (results []LoyaltyTransaction, numOfUniqueCustomer int, numOfRedeem int) {

	//user 1 add 5 stamps
	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user1",
		Point:  1,
		ByID:   "staff1",
	})

	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user1",
		Point:  1,
		ByID:   "staff1",
	})

	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user1",
		Point:  1,
		ByID:   "staff1",
	})

	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user1",
		Point:  1,
		ByID:   "staff1",
	})

	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user1",
		Point:  1,
		ByID:   "staff1",
	})

	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user2",
		Point:  1,
		ByID:   "staff1",
	})

	//user 1 redeem
	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user1",
		Point:  -5,
		ByID:   "staff2",
	})

	//user 1 add 7 stamps
	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user1",
		Point:  1,
		ByID:   "staff1",
	})
	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user1",
		Point:  1,
		ByID:   "staff1",
	})
	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user1",
		Point:  1,
		ByID:   "staff1",
	})
	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user1",
		Point:  1,
		ByID:   "staff1",
	})
	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user1",
		Point:  1,
		ByID:   "staff1",
	})
	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user1",
		Point:  1,
		ByID:   "staff1",
	})
	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user1",
		Point:  1,
		ByID:   "staff1",
	})

	//user 2 add 2 stamps
	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user2",
		Point:  1,
		ByID:   "staff2",
	})

	//user 3 add 1 stamps
	results = append(results, LoyaltyTransaction{
		ID:     bson.NewObjectId(),
		UserID: "user3",
		Point:  0,
		ByID:   "staff2",
	})
	return results, 3, 1
}
