package model

import (
	"github.com/globalsign/mgo/bson"
)

type Outlet struct {
	ID             bson.ObjectId `json:"outletid" form:"outletid" bson:"_id,omitempty"`
	MerchantId     string        `json:"merchantId" bson:"merchantId"`
	Name           string        `json:"name" bson:"name"`
	UnifiID        string        `json:"uid" bson:"uid"`
	SalesCode      string        `json:"salesCode" bson:"salesCode"`
	Remark         string        `json:"remark" bson:"remark"`
	RegnNum        string        `json:"brn" bson:"brn"`
	Address        string        `json:"address" bson:"address"`
	Contact        string        `json:"contact" bson:"contact"`
	Status         bson.ObjectId `json:"outletstatus" bson:"outletstatus,omitempty"`
	OwnerBrand     string        `json:"brand" bson:"brand"`
	OwnerName      string        `json:"ownername" bson:"ownername"`
	Email          string        `json:"email" bson:"email"`
	Mobile         string        `json:"mobile" bson:"mobile"`
	ApprovalStatus string        `json:"approvalStatus" bson:"approvalStatus"`
	CreatedAt      int           `json:"createdat" bson:"createdat"`
	UpdatedAt      int           `json:"updatedAt" bson:"updatedAt"`
	CreatedBy      string        `json:"createdby" bson:"createdby"`
	ModifiedBy     string        `json:"modifiedby" bson:"modifiedby"`
	Ispremium      bool          `json:"ispremium" bson:"ispremium"`
	Noofcards      int           `json:"noofcards" bson:"noofcards"`
	Isfreemium     bool          `json:"isfreemium" bson:"isfreemium"`
	Manager        string        `json:"manager" bson:"manager"`
	Staff          string        `json:"staff" bson:"staff"`
	ManagerName    string        `json:"managerName" bson:"managerName"`
	StaffName      string        `json:"staffName" bson:"staffName"`
	Image          string        `json:"image" bson:"image"`
	Redeem         bool          `json:"redeem" bson:"redeem"`
	OneStamp       bool          `json:"onestamp" bson:"onestamp"`
	LimitStamp     bool          `json:"limitstamp" bson:"limitstamp"`
	EventID        string        `json:"eventid" bson:"eventid"`
}
