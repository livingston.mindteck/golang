package model

import (
	"time"

	"bitbucket.org/nexlife/chopchop-backend/config"
	"github.com/globalsign/mgo/bson"
)

type OutletStatus struct {
	ID         bson.ObjectId `json:"statusid" bson:"_id,omitempty"`
	Title      string        `json:"title" bson:"title"`
	CreatedAt  int           `json:"createdat" bson:"createdat,omitempty"`
	ModifiedAt int           `json:"modifiedat" bson:"modifiedat,omitempty"`
	CreatedBy  string        `json:"createdby" bson:"createdby,omitempty"`
	ModifiedBy string        `json:"modifiedby" bson:"modifiedby,omitempty"`
}

func CreateOutletStatus(o *OutletStatus) error {
	db := DB.Clone()
	defer db.Close()
	return db.DB(config.DBName).C(config.OutletStatusBranch).Insert(o)
}

func QuerryOutletStatus(key string) (result []*OutletStatus, err error) {
	db := DB.Clone()
	defer db.Close()

	o := db.DB(config.DBName).C(config.OutletStatusBranch)
	err = o.Find(bson.M{"$where": "this._id.toString().match(/.*" + key + ".*/)"}).All(&result)
	return result, err
}
func QuerryOutletStatusByTitle(key string) (result []*OutletStatus, err error) {
	db := DB.Clone()
	defer db.Close()

	o := db.DB(config.DBName).C(config.OutletStatusBranch)
	err = o.Find(bson.M{"title": key}).All(&result)
	return result, err
}
func GetOutletStatus(id string) (o OutletStatus, err error) {
	db := DB.Clone()
	defer db.Close()

	err = db.DB(config.DBName).C(config.OutletStatusBranch).FindId(bson.ObjectIdHex(id)).One(&o)
	return o, err
}

func UpdateOutletStatus(id, key, value string) error {
	db := DB.Clone()
	defer db.Close()

	if err := db.DB(config.DBName).C(config.OutletStatusBranch).Update(
		bson.M{"_id": bson.ObjectIdHex(id)},
		bson.M{"$set": bson.M{key: value, "modifiedat": int(time.Now().Unix())}},
	); err != nil {
		return err
	}

	return nil
}
