package model

import (
	"log"

	"github.com/globalsign/mgo/bson"
	config "bitbucket.org/nexlife/chopchop-backend/config"

	"github.com/globalsign/mgo"
)

//Room branch saved in db
type Room struct {
	ID                    bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Username              string        `json:"username" bson:"username"`
	Name                  string        `json:"name" bson:"name"`
	IconPath              string        `json:"icon_path" bson:"icon_path"`
	Type                  string        `json:"type" bson:"type"`
	Description           string        `json:"description" bson:"description"`
	IsPublic              bool          `json:"isPublic,omitempty" bson:"isPublic"`
	IsRedeemable          bool          `json:"isRedeemable,omitempty" bson:"isRedeemable"` //for loyalty niche
	IsOneCustomerOneStamp bool          `json:"isOneCustomerOneStamp,omitempty" bson:"isOneCustomerOneStamp"`
	IsUnlimitedStamp      bool          `json:"isUnlimitedStamp,omitempty" bson:"isUnlimitedStamp"`
	EventID               string        `json:"event_id,omitempty" bson:"event_id"`
	Screens               string        `json:"screens,omitempty" bson:"screens,omitempty"` //screens is actually is a stringified json
	Users                 bson.M        `json:"users,omitempty" bson:"users,omitempty"`
	UpdatedAt             int           `json:"updatedAt" bson:"updatedAt"`
}

func roomInit() {
	c := DB.Copy().DB(config.DBName).C(config.RoomBranch)
	err := c.EnsureIndex(mgo.Index{
		Key:    []string{"name"},
		Unique: false,
	})
	if err != nil {
		log.Fatal(err)
	}
}

func CreateRoom(room *Room) (err error) {
	db := DB.Clone()
	defer db.Close()
	return db.DB(config.DBName).C(config.RoomBranch).Insert(room)
}

func SearchRoom(keyword string) (results []*Room, err error) {
	db := DB.Clone()
	defer db.Close()
	c := db.DB(config.DBName).C(config.RoomBranch)

	regex := bson.RegEx{"(?i)^.*" + keyword + ".*$", ""}

	err = c.Find(bson.M{"name": regex, "isPublic": true}).All(&results)

	return results, err
}

func GetRoom(roomID string) (room Room, err error) {
	db := DB.Clone()
	defer db.Close()
	err = db.DB(config.DBName).C(config.RoomBranch).FindId(bson.ObjectIdHex(roomID)).One(&room)
	return room, err
}

func GetRoomBasedOnEventID(eventID string) (results []*Room, err error) {
	db := DB.Clone()
	defer db.Close()
	c := db.DB(config.DBName).C(config.RoomBranch)

	err = c.Find(bson.M{"event_id": eventID}).All(&results)

	return results, err
}

//AddUserInRoom is to add (duplicate) user object under room when user is added a role
func AddUserInRoom(roomID string, user User, role string) {
	db := DB.Clone()
	defer db.Close()

	db.DB(config.DBName).C(config.RoomBranch).Update(
		bson.M{"_id": bson.ObjectIdHex(roomID)},
		bson.M{"$set": bson.M{"users." + user.ID.Hex(): bson.M{"role": role, "name": user.Name, "email": user.Email}}},
	)
}

func RemoveUserInRoom(roomID string, userID string) {
	db := DB.Clone()
	defer db.Close()
	db.DB(config.DBName).C(config.RoomBranch).Update(
		bson.M{"_id": bson.ObjectIdHex(roomID)},
		bson.M{"$unset": bson.M{"users." + userID: 1}},
	)
}
