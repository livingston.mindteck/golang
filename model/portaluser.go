package model

import (
	"time"

	config "bitbucket.org/nexlife/chopchop-backend/config"

	"github.com/globalsign/mgo/bson"
)

type PortalUser struct {
	ID                  bson.ObjectId `json:"id" form:"id" bson:"_id,omitempty"`
	Email               string        `json:"email" form:"email" bson:"email"`
	Name                string        `json:"name" form:"name" bson:"name"`
	Password            string        `json:"password,omitempty" bson:"password"`
	NewLogin            string        `default:"yes" json:"newlogin,omitempty" bson:"newlogin"`
	VerifyKey           string        `json:"verifyKey" bson:"verifyKey"`
	ForgotPasswordToken string        `json:"forgotPasswordToken" form:"forgotPasswordToken" bson:"forgotPasswordToken"`
	UserType            string        `json:"userType" form:"userType" bson:"userType"`
	UpdatedAt           int           `json:"updatedAt" bson:"updatedAt"`
	Token               string        `json:"token" form:"token" bson:"token"`
	LastLogin           uint64        `json:"lastLogin" bson:"lastLogin"`
	LoggedIn            bool          `json:"loggedIn" bson:"loggedIn"`
}

var UserType = struct {
	Portal   string
	Admin    string
	Merchant string
	Staff    string
}{"portal", "admin", "merchant", "staff"}

func IsPortalUserAvailable(email string) bool {
	db := DB.Clone()
	defer db.Close()
	if cnt, err := db.DB(config.DBName).C(config.PortalUser).Find(bson.M{"email": email}).Count(); err == nil && cnt > 0 {
		return true
	}
	return false
}

func CreatePortalUser(email string, password string, name string) (id string) {
	db := DB.Clone()
	defer db.Close()
	user := &PortalUser{ID: bson.NewObjectId()}
	user.Password = password
	user.Name = name
	user.Email = email
	if name == "staff" {
		user.UserType = UserType.Staff
	}
	db.DB(config.DBName).C(config.PortalUser).Insert(user)
	n := user.ID.Hex()
	return n
}

func AccountVerified(email string) {
	db := DB.Clone()
	defer db.Close()
	u := new(User)
	if err := db.DB(config.DBName).C(config.PortalUser).Find(bson.M{"email": email}).One(u); err != nil {
		return
	}

	if err := db.DB(config.DBName).C(config.PortalUser).
		UpdateId(u.ID, bson.M{"$set": bson.M{"verifyKey": "", "updatedAt": int(time.Now().Unix())}}); err != nil {
		return
	}
	return
}

func ClearUserToken(usr PortalUser) (err error) {
	db := DB.Clone()
	defer db.Close()
	return
}
