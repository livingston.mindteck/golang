package model

import (
	"github.com/globalsign/mgo/bson"
)

const (
	StatusConfirmed   = "confirmed"
	StatusUnconfirmed = "unconfirmed"
	StatusApproved    = "approved"
	StatusRejected    = "rejected"
	StatusInProcess   = "processing"
	StatusDeactivated = "deactivated"
)

type MerchantAccount struct {
	ID               bson.ObjectId `json:"id" form:"id" bson:"_id,omitempty"`
	ContactName      string        `json:"contactName" bson:"contactName"`
	Email            string        `json:"email" bson:"email"`
	Address          string        `json:"address" bson:"address"`
	Phone            string        `json:"phone" bson:"phone"`
	Password         string        `json:"password,omitempty" bson:"password"`
	BrandName        string        `json:"brandName" bson:"brandName"`
	BrandDesc        string        `json:"brandDesc,omitempty" bson:"brandDesc"`
	PromoInfo        string        `json:"promo,omitempty" bson:"promo"`
	Image            string        `json:"image,omitempty" bson:"image"`
	NumberOfStamp    int           `json:"stamps" bson:"stamps"`
	AccStatus        string        `json:"accStatus,omitempty" bson:"accStatus"`
	ApprovalRequired bool          `json:"approvalRequired" bson:"approvalRequired"`
	Isnew            bool          `json:"isnew" bson:"isnew"`
	ApprovalStatus   string        `json:"approvalStatus" bson:"approvalStatus"`
	ApprovalReqSent  bool          `json:"approvalReqSent" bson:"approvalReqSent"`
	VerifyKey        string        `json:"verifyKey" bson:"verifyKey"`
	LoggedIn         bool          `json:"loggedIn" bson:"loggedIn"`
	LastLogin        uint64          `json:"lastLogin" bson:"lastLogin"`
	CreatedAt        int           `json:"createdAt" bson:"createdAt"`
	UpdatedAt        int           `json:"updatedAt" bson:"updatedAt"`
}