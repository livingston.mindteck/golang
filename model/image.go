package model

import (
	"bitbucket.org/nexlife/chopchop-backend/config"
	"github.com/globalsign/mgo/bson"
)

type Image struct {
	ID        bson.ObjectId `json:"imageid" bson:"_id"`
	Name      string        `json:"name" bson:"name"`
	Data      []byte        `json:"data,omitempty" bson:"data"`
	UpdatedAt int           `json:"updatedAt" bson:"updatedAt"`
}

func UploadImage(image *Image) error {
	db := DB.Clone()
	defer db.Close()

	if err := db.DB(config.DBName).C(config.ImageBranch).Insert(image); err != nil {
		return err
	}

	return nil
}

func DownloadImage(id string) (*Image, error) {
	db := DB.Clone()
	defer db.Close()

	image := &Image{}
	if err := db.DB(config.DBName).C(config.ImageBranch).FindId(bson.ObjectIdHex(id)).One(image); err != nil {
		return nil, err
	}

	return image, nil
}
