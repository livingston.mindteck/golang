package model

import (
	"log"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	config "bitbucket.org/nexlife/chopchop-backend/config"
)

type Message struct {
	ID         bson.ObjectId `json:"id" bson:"_id,omitempty"`
	SenderID   string        `json:"sender_id" bson:"sender_id"`
	ReceiverID string        `json:"receiver_id" bson:"receiver_id"`
	Message    string        `json:"message" bson:"message"`
}

func GetMessages(userID string) (results []Message) {
	results = []Message{}
	db := DB.Clone()
	defer db.Close()

	err := db.DB(config.DBName).C(config.MessageBranch).Find(
		bson.M{"$or": []bson.M{
			bson.M{"sender_id": userID},
			bson.M{"receiver_id": userID},
		}}).All(&results)
	if err != nil {
		return []Message{}
	}
	return results
}

func SendMessage(senderID string, receiverID string, message string) (error error) {
	db := DB.Clone()
	defer db.Close()

	m := &Message{
		ID:         bson.NewObjectId(),
		SenderID:   senderID,
		ReceiverID: receiverID,
		Message:    message,
	}

	return db.DB(config.DBName).C(config.MessageBranch).Insert(m)
}

func messageInit() {
	c := DB.Copy().DB(config.DBName).C(config.MessageBranch)

	err := c.EnsureIndex(mgo.Index{
		Key:    []string{"sender_id"},
		Unique: false,
	})
	if err != nil {
		log.Fatal(err)
	}

	err = c.EnsureIndex(mgo.Index{
		Key:    []string{"receiver_id"},
		Unique: false,
	})
	if err != nil {
		log.Fatal(err)
	}
}
