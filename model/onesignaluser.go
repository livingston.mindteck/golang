package model

import (
	"fmt"
	"log"

	"github.com/globalsign/mgo/bson"

	config "bitbucket.org/nexlife/chopchop-backend/config"

	"github.com/globalsign/mgo"
)

//OnesignalUser is model of OnesignalUser branch
type OnesignalUser struct {
	ID          bson.ObjectId `json:"id" form:"id" bson:"_id,omitempty"`
	UserID      string        `json:"user_id" form:"user_id" bson:"user_id"`
	OnesignalID string        `json:"onesignal_id" form:"onesignal_id" bson:"onesignal_id"`
	BundleID    string        `json:"bundle_id" form:"bundle_id" bson:"bundle_id"`
	Platform    string        `json:"platform" form:"platform" bson:"platform"`
	UpdatedAt   int           `json:"updatedAt" bson:"updatedAt"`
}

func onesignalUserInit() {
	c := DB.Copy().DB(config.DBName).C(config.OnesignalUserBranch)

	err := c.EnsureIndex(mgo.Index{
		Key:    []string{"onesignal_id"},
		Unique: true,
	})
	if err != nil {
		log.Fatal(err)
	}

	err = c.EnsureIndex(mgo.Index{
		Key:    []string{"user_id", "bundle_id"},
		Unique: false,
	})
	if err != nil {
		log.Fatal(err)
	}
}

func ClearUserOnesignal(onesignalID string) (err error) {
	db := DB.Clone()
	defer db.Close()
	c := db.DB(config.DBName).C(config.OnesignalUserBranch)

	err = c.Remove(bson.M{"onesignal_id": onesignalID})
	if err != nil {
		fmt.Printf("remove fail %v\n", err)
		return err
	}

	return err
}

func GetUsersBasedOnUsersID(usersID []string) (results []*OnesignalUser, err error) {
	db := DB.Clone()
	defer db.Close()
	c := db.DB(config.DBName).C(config.OnesignalUserBranch)

	err = c.Find(bson.M{"user_id": bson.M{"$in": usersID}}).All(&results)

	if err != nil {
		return nil, err
	}

	return results, err
}
