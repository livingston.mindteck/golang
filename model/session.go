package model

import (
	"log"

	"bitbucket.org/nexlife/chopchop-backend/config"
	"github.com/globalsign/mgo"
)

// DB global session
var DB *mgo.Session

func init() {
	var err error
	DB, err = mgo.Dial(config.DBHost)
	if err != nil {
		log.Fatal(err)
	}

	roomInit()
	userInit()
	onesignalUserInit()
	staticInit()
	messageInit()
}
