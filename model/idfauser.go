package model

import (
	"fmt"
	"log"

	"github.com/globalsign/mgo/bson"

	config "bitbucket.org/nexlife/chopchop-backend/config"

	"github.com/globalsign/mgo"
)

type IDFAUser struct {
	ID        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	IDFA      string        `json:"idfa" bson:"idfa"`
	UserID    string        `json:"user_id" bson:"user_id"`
	UpdatedAt int           `json:"updatedAt" bson:"updatedAt"`
}

func idfaUserInit() {
	c := DB.Copy().DB(config.DBName).C(config.IDFAUserBranch)

	err := c.EnsureIndex(mgo.Index{
		Key:    []string{"idfa"},
		Unique: true,
	})
	if err != nil {
		log.Fatal(err)
	}

	err = c.EnsureIndex(mgo.Index{
		Key:    []string{"user_id"},
		Unique: false,
	})
	if err != nil {
		log.Fatal(err)
	}
}

//ClearUserIDFA is not sure if usefull. the only reason why this function is here is i mistakenly coded for it
func ClearUserIDFA(idfa string) (err error) {
	db := DB.Clone()
	defer db.Close()
	c := db.DB(config.DBName).C(config.IDFAUserBranch)

	err = c.Remove(bson.M{"idfa": idfa})
	if err != nil {
		fmt.Printf("remove fail %v\n", err)
		return err
	}

	return err
}
