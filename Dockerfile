FROM golang:latest as builder

#allow module env inside GOPATH
ENV GO111MODULE=on

ARG SSH_KEY_FILE
COPY ${SSH_KEY_FILE} /root/.ssh/id_rsa
RUN chmod 0600 /root/.ssh/id_rsa
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

RUN mkdir -p /go/src/bitbucket.org/nexlife/chopchop-backend
WORKDIR /go/src/bitbucket.org/nexlife/chopchop-backend
#copy go.mod & go.sum to workspace
COPY go.mod . 
COPY go.sum .

# Get dependancies - will also be cached if we won't change mod/sum
RUN go mod download
# COPY the source code as the last step
COPY . .

#Build binary
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o app .

FROM scratch

WORKDIR /opt
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/src/bitbucket.org/nexlife/chopchop-backend/app .
CMD ["./app"]