package service

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/smtp"

	config "bitbucket.org/nexlife/chopchop-backend/config"
	model "bitbucket.org/nexlife/chopchop-backend/model"

	sp "github.com/SparkPost/gosparkpost"
)

type SmtpServer struct {
	host string
	port string
}

//send emails using smtp
func SendEmail(subject string, receivers []string, content string) error {

	smtpServer := SmtpServer{host: config.EmailHost, port: config.EmailPort}

	fmt.Println(smtpServer.host)
	//build an auth
	auth := smtp.PlainAuth("", config.EmailSender, config.EmailPassword, smtpServer.host)

	// Gmail will reject connection if it's not secure
	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         smtpServer.host,
	}

	conn, err := tls.Dial("tcp", smtpServer.host+":"+smtpServer.port, tlsconfig)
	if err != nil {
		fmt.Println(err)

	}

	client, err := smtp.NewClient(conn, smtpServer.host)
	if err != nil {
		fmt.Println(err)
	}

	// step 1: Use Auth
	if err = client.Auth(auth); err != nil {
		fmt.Println(err)
	}

	// step 2: add all from and to
	if err = client.Mail(config.EmailSender); err != nil {
		fmt.Println(err)
	}
	for _, k := range receivers {
		if err = client.Rcpt(k); err != nil {
			fmt.Println(err)
		}
	}

	// Data
	w, err := client.Data()
	if err != nil {
		fmt.Println(err)
	}

	_, err = w.Write([]byte(content))
	if err != nil {
		fmt.Println(err)
	}

	err = w.Close()
	if err != nil {
		fmt.Println(err)
	}

	client.Quit()

	fmt.Println("Mail sent successfully")

	return err

}

func SendMail(subject string, sender string, receivers []string, content string) error {

	var sparky sp.Client
	err := sparky.Init(&sp.Config{ApiKey: config.SparkPostAPIKey})
	if err != nil {
		return err
	}

	tx := &sp.Transmission{
		Recipients: receivers,
		Content: sp.Content{
			HTML:    content,
			From:    sender,
			Subject: subject,
		},
	}
	id, _, err := sparky.Send(tx)
	if err != nil {
		return err
	}

	log.Printf("Transmission sent with id [%s]\n", id)
	return nil
}

func SendVerificationEmail(receiver string, verificationKey string) {
	verificationLink := fmt.Sprintf("%sverify?e=%s&k=%s", config.Domain, receiver, verificationKey)
	content := fmt.Sprintf("<html><body><p>Welcome to Auth. Click this link to verify your account <a href='%s'>VERIFY</a></p></body></html>", verificationLink)
	go SendMail("Auth Verification", config.EmailSender, []string{receiver}, content)
}

func SendRequestResetPasswordEmail(receiver string, user model.User) {
	verificationLink := config.Domain + "public/html/reset-password?e=" + receiver + "&k=" + user.ForgotPasswordToken
	// verificationLink := fmt.Sprintf("%sresetPasswordPage?e=%s&k=%s", config.Domain, receiver, user.ForgotPasswordToken)
	body := `<p>Dear ` + user.Name + `</p>`
	body += `<p>Thank you for using nexlife</p>`
	body += `<p>You requested a link to change your passsword. Click on the link below to change your password. If you did not request for this password reset, please feel free to ignore this email. This link will expire in 24 hours.</p>`
	body += `<p><a href='` + verificationLink + `'>` + verificationLink + `</a></p>`
	body += `<p>Best Regards<br/>Nexlife Team</p>`
	body = "<html><body>" + body + "</body></html>"
	go SendMail("Reset Password - nex.life", config.EmailSender, []string{receiver}, body)
}
func SendPasswordReset(user model.PortalUser) {
	body := `<p>Dear ` + user.Name + `</p>`
	body += `<p>Thank you for using nexlife</p>`
	body += `<p>You requested a link to change your passsword. Click on the link below to change your password. If you did not request for this password reset, please feel free to ignore this email. This link will expire in 24 hours.</p>`
	body += `<p>Verification Token:` + user.ForgotPasswordToken + `</p>`
	body += `<p>Best Regards<br/>Nexlife Team</p>`
	body = "<html><body>" + body + "</body></html>"
	go SendMail("Reset Password - nex.life", config.EmailSender, []string{user.Email}, body)
}

func SendApprovalRequest(approverEmail string, merchantID string) {
	verificationLink := fmt.Sprintf("%smerchants/approval?merchant=%s", config.Domain, merchantID)
	content := fmt.Sprintf("<html><body><p>Please click the following link to approve Merchant <a href='%s'>VERIFY</a></p></body></html>", verificationLink)
	go SendMail("Please Approve ", config.EmailSender, []string{approverEmail}, content)
}

func SendVerificationEmail_Merchant(UserEmail string, verificationKey string, url string) {
	verificationLink := fmt.Sprintf("%s%s?e=%s&k=%s", config.Domain, url, UserEmail, verificationKey)
	content := fmt.Sprintf("<html><body><p>Welcome to Auth. Click this link to verify your account <a href='%s'>VERIFY</a></p></body></html>", verificationLink)
	go SendMail("Auth Verification", config.EmailSender, []string{UserEmail}, content)
}

///public/html/merchant/verify.html

func SendMailByEmailTemplate(template *model.EmailTemplate) {
	go SendMail(template.Subject, config.EmailSender, template.Receivers, template.Body)
}

func PortalSendPasswordReset(email string, token string) {
	verificationLink := fmt.Sprintf("%spublic/html/merchant/forgot.html?e=%s&t=%s", config.Domain, email, token)
	body := `<p>Dear ` + email + `</p>`
	body += `<p>Thank you for using nexlife</p>`
	body += `<p>There is a request to change password for the account registered to this e-mail address:\n\nUsername: ` + email + `\n\nIf you have requested for the password change, please clink here <a href="` + verificationLink + `"></a> to reset your password.\n\nThank you.</p>`
	body += `<p>Best Regards<br/>Nexlife Team</p>`
	body = "<html><body>" + body + "</body></html>"
	go SendMail("Forgot password - nex.life", config.EmailSender, []string{email}, body)
}
