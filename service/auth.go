package service

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	//	"encoding/base64"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"

	config "bitbucket.org/nexlife/chopchop-backend/config"
	. "bitbucket.org/nexlife/chopchop-backend/model"
	"github.com/globalsign/mgo/bson"
	//	"bitbucket.org/nexlife/chopchop-backend/dbservices/merchant"
	//	"bitbucket.org/nexlife/chopchop-backend/model"
)

var mySigningKey = []byte("webesecretpassword")

func AuthorizeMerchantAPI(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		apiAuthToken := c.Request().Header.Get("token")

		validToken, verifyMsg := verifyToken(apiAuthToken)
		if validToken == false {
			return c.JSON(http.StatusUnauthorized, map[string]interface{}{"error": "unauthorized - " + verifyMsg,
				"errorCode": 401})
		}

		userId, userType, lastLogin, validToken := ExtractClaims(apiAuthToken)

		if validToken == false {
			return c.JSON(http.StatusUnauthorized, map[string]interface{}{"error": "unauthorized - Invalid token",
				"errorCode": 401})
		}

		if userType != "merchant" {
			return c.JSON(http.StatusUnauthorized, map[string]interface{}{"error": "missing user type",
				"errorCode": 401})
		}

		//	acc, err := merchant.GetById(userId)
		//	if err != nil {
		//		return c.JSON(http.StatusUnauthorized, map[string]interface{}{"error": err})
		//	}

		db := DB.Clone()
		defer db.Close()

		acc := &PortalUser{}
		if err := db.DB(config.DBName).C(config.PortalUser).FindId(bson.ObjectIdHex(userId)).One(acc); err != nil {
			return c.JSON(http.StatusUnauthorized, map[string]interface{}{"error": err,
				"errorCode": 401})
		}

		if !acc.LoggedIn {
			return c.JSON(http.StatusUnauthorized, map[string]interface{}{"error": "session expired",
				"errorCode": 401})
		}

		if acc.LastLogin != lastLogin {
			return c.JSON(http.StatusUnauthorized, map[string]interface{}{"error": "session expired",
				"errorCode": 401})
		}

		return next(c)
	}
}

func verifyToken(jwtoken string) (bool, string) {
	token, err := jwt.Parse(jwtoken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error")
		}
		return mySigningKey, nil
	})

	if err != nil {
		return false, "Invalid token"
	}

	if token.Valid {
		return true, "Valid token"
	}

	return false, "Invalid token"
}

func ExtractClaims(tokenStr string) (string, string, uint64, bool) {
	hmacSecret := []byte(mySigningKey)
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		return hmacSecret, nil
	})

	if err != nil {
		return "", "", 0, false
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		userId := fmt.Sprintf("%v", claims["userId"])
		userType := fmt.Sprintf("%v", claims["userType"])
		lastLogin, err := strconv.ParseUint(fmt.Sprintf("%v", claims["lastLogin"]), 10, 64)
		if err != nil {
			lastLogin = 0
		}
		return userId, userType, lastLogin, true
	} else {
		return "", "", 0, false
	}
}

func GenerateJWT(userName string, userId string, userType string, lastLogin uint64) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["userName"] = userName
	claims["userId"] = userId
	claims["lastLogin"] = strconv.FormatUint(lastLogin, 10)
	claims["userType"] = userType
	claims["exp"] = time.Now().Add(time.Minute * 60 * 24 * 365 * 10).Unix()

	tokenString, err := token.SignedString(mySigningKey)

	if err != nil {
		return "", err
	}

	return tokenString, nil
}
