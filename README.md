# Golang API + Mongo database


## Configuration
Set all variable in `.env.example` in your path. This application will read the value of the variable from OS environment


## Running
Run `go run server.go`  

## Signup
User signup  

Retrieve user credentials from the body and validate against database.
For invalid email or password, `send 400 - Bad Request` response.
For valid email and password, save user in database and send `201 - Created` response.  

Request  

```sh
curl \
  -X POST \
  http://localhost:1323/signup \
  -H "Content-Type: application/json" \
  -d '{"email":"jon@doe.com","password":"secret"}'
```
Response  

`201 - Created`  

```json
{
  "id": "58465b4ea6fe886d3215c6df",
  "email": "jon@doe.com"
}
```


## Login
User login  

Retrieve user credentials from the body and validate against database.
For invalid credentials, send 401 - Unauthorized response.
For valid credentials, send 200 - OK response:
Generate JWT for the user and send it as response.
Each subsequent request must include JWT in the Authorization header.
Method: `POST`  
Path: `/login`  

Request  

```sh
curl \
  -X POST \
  http://localhost:1323/login \
  -H "Content-Type: application/json" \
  -d '{"email":"jon@doe.com","password":"secret"}'
```
Response  

`200 - OK`

```json
{
  "id": "58465b4ea6fe886d3215c6df",
  "email": "jon@doe.com",
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0ODEyNjUxMjgsImlkIjoiNTg0NjViNGVhNmZlODg2ZDMyMTVjNmRmIn0.1IsGGxko1qMCsKkJDQ1NfmrZ945XVC9uZpcvDnKwpL0"
}
```
Client should store the token, for browsers, you may use local storage.  


## Room Image Upload
This api is designed to accept multiple images upload in single api, but it always adviced to send single image per api

/room/uploadImage

files : this is the file itself. to upload mulitple images, just add more ```files``` so there will be more than 1 ```files``` parameter
file_category = icon / pic

room_id

file_category : one of these
```icon``` will be icon of the room

Response:
array of imaegs path