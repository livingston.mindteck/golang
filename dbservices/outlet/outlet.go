package outlet

import (
	"errors"
	"fmt"
	"time"

	"bitbucket.org/nexlife/chopchop-backend/config"
	. "bitbucket.org/nexlife/chopchop-backend/model"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"
)

func Create(outlet *Outlet) (string, error) {
	db := DB.Clone()
	defer db.Close()

	outlet.CreatedAt = int(time.Now().Unix())
	outlet.UpdatedAt = outlet.CreatedAt
	if err := db.DB(config.DBName).C(config.OutletBranch).Insert(outlet); err != nil {
		return "", err
	}

	return outlet.ID.Hex(), nil
}

func GetAll(limit int) ([]*Outlet, error) {
	db := DB.Clone()
	defer db.Close()

	o := []*Outlet{}
	if limit > 0 {
		if err := db.DB(config.DBName).C(config.OutletBranch).Find(nil).Limit(limit).All(&o); err != nil {
			return nil, err
		}
		return o, nil
	}

	if err := db.DB(config.DBName).C(config.OutletBranch).Find(nil).All(&o); err != nil {
		return nil, err
	}

	return o, nil
}

func GetByEmail(email string) (*Outlet, error) {
	db := DB.Clone()
	defer db.Close()

	outlet := &Outlet{}
	if err := db.DB(config.DBName).C(config.OutletBranch).Find(bson.M{"email": email}).One(outlet); err != nil {
		return nil, err
	}
	return outlet, nil
}

func GetById(id string) (*Outlet, error) {
	db := DB.Clone()
	defer db.Close()

	o := &Outlet{}
	if err := db.DB(config.DBName).C(config.OutletBranch).FindId(bson.ObjectIdHex(id)).One(o); err != nil {
		return nil, err
	}
	// fmt.Println(o.Noofcards)
	return o, nil
}

func Find(m map[string]interface{}) (*Outlet, error) {
	db := DB.Clone()
	defer db.Close()

	o := new(Outlet)
	if err := db.DB(config.DBName).C(config.OutletBranch).Find(bson.M(m)).One(o); err != nil {
		return nil, err
	}

	return o, nil
}

func FindAll(m map[string]interface{}) ([]*Outlet, error) {
	db := DB.Clone()
	defer db.Close()

	outlets := []*Outlet{}
	err := db.DB(config.DBName).C(config.OutletBranch).Find(bson.M(m)).All(&outlets)
	if err != nil {
		return nil, err
	}

	return outlets, err
}

func Search(keyword string) ([]*Outlet, error) {
	db := DB.Clone()
	defer db.Close()

	regex := bson.M{"$where": "this._id.toString().match(/.*" + keyword + ".*/)"}
	o := []*Outlet{}
	if err := db.DB(config.DBName).C(config.OutletBranch).Find(regex).All(&o); err != nil {
		return nil, err
	}
	return o, nil
}

func Count(m map[string]interface{}) (cnt int, err error) {
	db := DB.Clone()
	defer db.Close()
	cnt, err = db.DB(config.DBName).C(config.OutletBranch).Find(bson.M(m)).Count()
	return
}

func Update(update *Outlet) (*Outlet, error) {
	db := DB.Clone()
	defer db.Close()
	// o, err := GetById(id)
	// if err != nil {
	// 	return nil, err
	// }

	// update.CreatedAt = o.CreatedAt
	update.UpdatedAt = int(time.Now().Unix())
	err := db.DB(config.DBName).C(config.OutletBranch).Update(bson.M{"_id": update.ID}, update)
	if err != nil {
		return nil, err
	}

	return update, nil
}

func Patch(id string, m map[string]interface{}) error {
	db := DB.Clone()
	defer db.Close()

	m["updatedAt"] = int(time.Now().Unix())
	if err := db.DB(config.DBName).C(config.OutletBranch).Update(
		bson.M{"_id": bson.ObjectIdHex(id)},
		bson.M{"$set": bson.M(m)},
	); err != nil {
		return err
	}

	return nil
}

func IsStaffAlreadyAsigned(StaffEmail string) (o *Outlet, err error) {
	db := DB.Clone()
	defer db.Close()
	if o, err = Find(map[string]interface{}{"staff": StaffEmail}); err != nil {
		fmt.Println(StaffEmail)
		return nil, errors.New("Staff is not assigned to any outlet")
	}
	return o, nil
}

func UpdateChop(StaffEmail string, UpdateOutlet bool, NoOfCards int, o *Outlet) (err error) {
	db := DB.Clone()
	defer db.Close()

	// if o, err = IsStaffAlreadyAsigned(StaffEmail); err != nil {
	// 	return nil, errors.New("Unable to Find the Staff")
	// }

	fmt.Println(fmt.Sprintf("premi =%s, %s - NoOfCards = %s - %s", o.Ispremium, o.Isfreemium, o.Noofcards, NoOfCards))
	if !((o.Isfreemium && o.Noofcards >= NoOfCards) || o.Ispremium) {
		return errors.New("You are not eligible to Chop")
	}

	o.ModifiedBy = StaffEmail
	if UpdateOutlet {
		o.Noofcards -= NoOfCards
		if _, err := Update(o); err != nil {
			return errors.New("Unable to Update")
		}
	}
	return nil
}

func ApproveOutlet(id string) error {
	if err := Patch(id, map[string]interface{}{
		"approvalStatus": StatusApproved,
	}); err != nil {
		if err == mgo.ErrNotFound {
			return echo.ErrNotFound
		}
	}
	return nil
}
