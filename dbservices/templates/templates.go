package templates

import (
	"time"

	"bitbucket.org/nexlife/chopchop-backend/config"
	. "bitbucket.org/nexlife/chopchop-backend/model"
	utils "bitbucket.org/nexlife/chopchop-backend/utils"
	"github.com/globalsign/mgo/bson"
)

func CreateTemplate(template *EmailTemplate) (*EmailTemplate, error) {
	db := DB.Clone()
	defer db.Close()

	template.CreatedAt = int(time.Now().Unix())
	template.UpdatedAt = template.CreatedAt
	if err := db.DB(config.DBName).C(config.EmailTemplate).Insert(template); err != nil {
		return nil, err
	}

	return template, nil
}

func GetTemplateByTitle(title string) (*EmailTemplate, error) {
	db := DB.Clone()
	defer db.Close()

	t := new(EmailTemplate)
	if err := db.DB(config.DBName).C(config.EmailTemplate).Find(bson.M{"title": title}).One(t); err != nil {
		return nil, err
	}

	return t, nil
}

func GetTemplateByKey(key, value string) (*EmailTemplate, error) {
	db := DB.Clone()
	defer db.Close()

	t := new(EmailTemplate)
	if err := db.DB(config.DBName).C(config.EmailTemplate).Find(bson.M{key: value}).One(t); err != nil {
		return nil, err
	}

	return t, nil
}

func GetTemplateById(id string) (*EmailTemplate, error) {
	db := DB.Clone()
	defer db.Close()

	t := new(EmailTemplate)
	if err := db.DB(config.DBName).C(config.EmailTemplate).Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(t); err != nil {
		return nil, err
	}

	return t, nil
}

func UpdateTemplate(id string, update *EmailTemplate) (err error) {
	db := DB.Clone()
	defer db.Close()

	var t *EmailTemplate
	if t, err = GetTemplateById(id); err != nil {
		return err
	}

	update.CreatedAt = t.CreatedAt
	update.UpdatedAt = int(time.Now().Unix())
	err = db.DB(config.DBName).C(config.EmailTemplate).Update(bson.M{"_id": bson.ObjectIdHex(id)}, update)
	if err != nil {
		return err
	}
	return nil
}

func DeleteTemplate(id string) error {
	db := DB.Clone()
	defer db.Close()

	err := db.DB(config.DBName).C(config.EmailTemplate).Remove(bson.M{"_id": bson.ObjectIdHex(id)})
	return err
}

func AddDefaultTemplates() error {
	for _, t := range utils.Templates {
		t.Id = bson.NewObjectId()
		CreateTemplate(t)
	}

	return nil
}
