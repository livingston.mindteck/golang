package merchant

import (
	"time"

	"bitbucket.org/nexlife/chopchop-backend/config"
	. "bitbucket.org/nexlife/chopchop-backend/model"
	"github.com/globalsign/mgo/bson"
)

func Create(merchant *MerchantAccount) (string, error) {
	db := DB.Clone()
	defer db.Close()

	merchant.CreatedAt = int(time.Now().Unix())
	merchant.UpdatedAt = merchant.CreatedAt
	if err := db.DB(config.DBName).C(config.MerchantBranch).Insert(merchant); err != nil {
		return "", err
	}

	return merchant.ID.Hex(), nil
}

func GetAll(limit int) ([]*MerchantAccount, error) {
	db := DB.Clone()
	defer db.Close()

	acc := []*MerchantAccount{}
	if limit > 0 {
		if err := db.DB(config.DBName).C(config.MerchantBranch).Find(nil).Limit(limit).All(&acc); err != nil {
			return nil, err
		}
		return acc, nil
	}

	if err := db.DB(config.DBName).C(config.MerchantBranch).Find(nil).All(&acc); err != nil {
		return nil, err
	}

	return acc, nil
}

func GetByEmail(email string) (*MerchantAccount, error) {
	db := DB.Clone()
	defer db.Close()

	acc := &MerchantAccount{}
	if err := db.DB(config.DBName).C(config.MerchantBranch).Find(bson.M{"email": email}).One(acc); err != nil {
		return nil, err
	}
	return acc, nil
}

func GetById(id string) (*MerchantAccount, error) {
	db := DB.Clone()
	defer db.Close()

	acc := &MerchantAccount{}
	if err := db.DB(config.DBName).C(config.MerchantBranch).FindId(bson.ObjectIdHex(id)).One(acc); err != nil {
		return nil, err
	}
	return acc, nil
}

func Find(m map[string]interface{}) (*MerchantAccount, error) {
	db := DB.Clone()
	defer db.Close()

	acc := new(MerchantAccount)
	if err := db.DB(config.DBName).C(config.MerchantBranch).Find(bson.M(m)).One(acc); err != nil {
		return nil, err
	}

	return acc, nil
}

func Search(keyword string) ([]*MerchantAccount, error) {
	db := DB.Clone()
	defer db.Close()
	regex := bson.RegEx{"(?i)^.*" + keyword + ".*$", ""}
	cond := bson.M{"name": regex}
	if keyword == "" {
		cond = nil
	}

	acc := []*MerchantAccount{}
	if err := db.DB(config.DBName).C(config.MerchantBranch).Find(cond).All(&acc); err != nil {
		return nil, err
	}

	return acc, nil

}

func Update(id string, update *MerchantAccount) (*MerchantAccount, error) {
	db := DB.Clone()
	defer db.Close()
	merchant, err := GetById(id)
	if err != nil {
		return nil, err
	}

	update.CreatedAt = merchant.CreatedAt
	update.UpdatedAt = int(time.Now().Unix())
	err = db.DB(config.DBName).C(config.MerchantBranch).Update(bson.M{"_id": merchant.ID}, update)
	if err != nil {
		return nil, err
	}

	return update, nil
}

func Patch(id string, m map[string]interface{}) error {
	db := DB.Clone()
	defer db.Close()

	m["updatedAt"] = int(time.Now().Unix())
	if err := db.DB(config.DBName).C(config.MerchantBranch).Update(
		bson.M{"_id": bson.ObjectIdHex(id)},
		bson.M{"$set": bson.M(m)},
	); err != nil {
		return err
	}

	return nil
}
