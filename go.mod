module bitbucket.org/nexlife/chopchop-backend

require (
	github.com/SparkPost/gosparkpost v0.0.0-20181030195550-4ca4ce177bb9
	github.com/asaskevich/govalidator v0.0.0-20180720115003-f9ffefc3facf
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/gorilla/websocket v1.4.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.0.0
	github.com/labstack/gommon v0.2.8
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.3.0
	github.com/tealeg/xlsx v1.0.3
	golang.org/x/crypto v0.0.0-20190313024323-a1f597ede03a
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
